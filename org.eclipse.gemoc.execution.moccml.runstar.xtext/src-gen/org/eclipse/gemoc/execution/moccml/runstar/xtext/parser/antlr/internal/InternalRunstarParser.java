package org.eclipse.gemoc.execution.moccml.runstar.xtext.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.eclipse.gemoc.execution.moccml.runstar.xtext.services.RunstarGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalRunstarParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Runtime'", "'State'", "'Representation'", "'RunStaR'", "'let'", "';'", "'Image'", "'('", "')'", "'be'", "'Variable'", "','", "'ConstText'", "'Chart'", "'x'", "'='", "'y'", "'+'", "'path'", "'with'", "'{'", "'title'", "'xLabel'", "'yLabel'", "'nbValues'", "'lineStyle'", "'}'", "'DecoratedText'", "'prefix'", "'suffix'", "'font'", "'fontSize'", "'fontStyle'", "'color'", "'errorText'", "'errorColor'", "'for'", "'a'", "'step'", "'between'", "'and'", "'import'", "'as'", "'importClass'", "'importPackage'", "'.'", "'*'", "'create'", "':'", "'Paint'", "'clear'", "'type'", "'persistent'", "'Mark'", "'Hover'", "'image'", "'fromStart'", "'Comment'", "'Label'", "'size'", "'depth'", "'label'", "'Stack'", "'length'", "'vertical'", "'shape'", "'strategy'", "'NONE'", "'BOLD'", "'ITALIC'", "'BACKGROUND'", "'FOREGROUND'", "'INFORMATION'", "'WARNING'", "'ERROR'", "'AFTER'", "'BEFORE'", "'RECTANGLE'", "'ROUNDED_RECTANGLE'", "'ELLIPSE'", "'LASTS'", "'FIRSTS'", "'DIGITAL'", "'ANALOG'", "'FALSE'", "'TRUE'"
    };
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=5;
    public static final int RULE_INT=6;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__94=94;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__88=88;
    public static final int T__89=89;
    public static final int T__84=84;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int T__87=87;

    // delegates
    // delegators


        public InternalRunstarParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalRunstarParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalRunstarParser.tokenNames; }
    public String getGrammarFileName() { return "InternalRunstar.g"; }



     	private RunstarGrammarAccess grammarAccess;
     	
        public InternalRunstarParser(TokenStream input, RunstarGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "RuntimeStateRepresentation";	
       	}
       	
       	@Override
       	protected RunstarGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleRuntimeStateRepresentation"
    // InternalRunstar.g:68:1: entryRuleRuntimeStateRepresentation returns [EObject current=null] : iv_ruleRuntimeStateRepresentation= ruleRuntimeStateRepresentation EOF ;
    public final EObject entryRuleRuntimeStateRepresentation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRuntimeStateRepresentation = null;


        try {
            // InternalRunstar.g:69:2: (iv_ruleRuntimeStateRepresentation= ruleRuntimeStateRepresentation EOF )
            // InternalRunstar.g:70:2: iv_ruleRuntimeStateRepresentation= ruleRuntimeStateRepresentation EOF
            {
             newCompositeNode(grammarAccess.getRuntimeStateRepresentationRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRuntimeStateRepresentation=ruleRuntimeStateRepresentation();

            state._fsp--;

             current =iv_ruleRuntimeStateRepresentation; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRuntimeStateRepresentation"


    // $ANTLR start "ruleRuntimeStateRepresentation"
    // InternalRunstar.g:77:1: ruleRuntimeStateRepresentation returns [EObject current=null] : ( () ( (otherlv_1= 'Runtime' otherlv_2= 'State' otherlv_3= 'Representation' ( (lv_name_4_0= ruleEString ) ) ) | (otherlv_5= 'RunStaR' ( (lv_name_6_0= ruleEString ) ) ) ) ( ( ( (lv_modelImports_7_0= ruleImportStatement ) ) | ( (lv_classImports_8_0= ruleImportJavaStatement ) ) ) ( ( (lv_modelImports_9_0= ruleImportStatement ) ) | ( (lv_classImports_10_0= ruleImportJavaStatement ) ) )* )? ( ( (lv_variables_11_0= ruleVariable ) ) | ( (lv_representations_12_0= ruleRepresentation ) ) )* ) ;
    public final EObject ruleRuntimeStateRepresentation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_4_0 = null;

        AntlrDatatypeRuleToken lv_name_6_0 = null;

        EObject lv_modelImports_7_0 = null;

        EObject lv_classImports_8_0 = null;

        EObject lv_modelImports_9_0 = null;

        EObject lv_classImports_10_0 = null;

        EObject lv_variables_11_0 = null;

        EObject lv_representations_12_0 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:80:28: ( ( () ( (otherlv_1= 'Runtime' otherlv_2= 'State' otherlv_3= 'Representation' ( (lv_name_4_0= ruleEString ) ) ) | (otherlv_5= 'RunStaR' ( (lv_name_6_0= ruleEString ) ) ) ) ( ( ( (lv_modelImports_7_0= ruleImportStatement ) ) | ( (lv_classImports_8_0= ruleImportJavaStatement ) ) ) ( ( (lv_modelImports_9_0= ruleImportStatement ) ) | ( (lv_classImports_10_0= ruleImportJavaStatement ) ) )* )? ( ( (lv_variables_11_0= ruleVariable ) ) | ( (lv_representations_12_0= ruleRepresentation ) ) )* ) )
            // InternalRunstar.g:81:1: ( () ( (otherlv_1= 'Runtime' otherlv_2= 'State' otherlv_3= 'Representation' ( (lv_name_4_0= ruleEString ) ) ) | (otherlv_5= 'RunStaR' ( (lv_name_6_0= ruleEString ) ) ) ) ( ( ( (lv_modelImports_7_0= ruleImportStatement ) ) | ( (lv_classImports_8_0= ruleImportJavaStatement ) ) ) ( ( (lv_modelImports_9_0= ruleImportStatement ) ) | ( (lv_classImports_10_0= ruleImportJavaStatement ) ) )* )? ( ( (lv_variables_11_0= ruleVariable ) ) | ( (lv_representations_12_0= ruleRepresentation ) ) )* )
            {
            // InternalRunstar.g:81:1: ( () ( (otherlv_1= 'Runtime' otherlv_2= 'State' otherlv_3= 'Representation' ( (lv_name_4_0= ruleEString ) ) ) | (otherlv_5= 'RunStaR' ( (lv_name_6_0= ruleEString ) ) ) ) ( ( ( (lv_modelImports_7_0= ruleImportStatement ) ) | ( (lv_classImports_8_0= ruleImportJavaStatement ) ) ) ( ( (lv_modelImports_9_0= ruleImportStatement ) ) | ( (lv_classImports_10_0= ruleImportJavaStatement ) ) )* )? ( ( (lv_variables_11_0= ruleVariable ) ) | ( (lv_representations_12_0= ruleRepresentation ) ) )* )
            // InternalRunstar.g:81:2: () ( (otherlv_1= 'Runtime' otherlv_2= 'State' otherlv_3= 'Representation' ( (lv_name_4_0= ruleEString ) ) ) | (otherlv_5= 'RunStaR' ( (lv_name_6_0= ruleEString ) ) ) ) ( ( ( (lv_modelImports_7_0= ruleImportStatement ) ) | ( (lv_classImports_8_0= ruleImportJavaStatement ) ) ) ( ( (lv_modelImports_9_0= ruleImportStatement ) ) | ( (lv_classImports_10_0= ruleImportJavaStatement ) ) )* )? ( ( (lv_variables_11_0= ruleVariable ) ) | ( (lv_representations_12_0= ruleRepresentation ) ) )*
            {
            // InternalRunstar.g:81:2: ()
            // InternalRunstar.g:82:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getRuntimeStateRepresentationAccess().getRuntimeStateRepresentationAction_0(),
                        current);
                

            }

            // InternalRunstar.g:87:2: ( (otherlv_1= 'Runtime' otherlv_2= 'State' otherlv_3= 'Representation' ( (lv_name_4_0= ruleEString ) ) ) | (otherlv_5= 'RunStaR' ( (lv_name_6_0= ruleEString ) ) ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==11) ) {
                alt1=1;
            }
            else if ( (LA1_0==14) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalRunstar.g:87:3: (otherlv_1= 'Runtime' otherlv_2= 'State' otherlv_3= 'Representation' ( (lv_name_4_0= ruleEString ) ) )
                    {
                    // InternalRunstar.g:87:3: (otherlv_1= 'Runtime' otherlv_2= 'State' otherlv_3= 'Representation' ( (lv_name_4_0= ruleEString ) ) )
                    // InternalRunstar.g:87:5: otherlv_1= 'Runtime' otherlv_2= 'State' otherlv_3= 'Representation' ( (lv_name_4_0= ruleEString ) )
                    {
                    otherlv_1=(Token)match(input,11,FollowSets000.FOLLOW_3); 

                        	newLeafNode(otherlv_1, grammarAccess.getRuntimeStateRepresentationAccess().getRuntimeKeyword_1_0_0());
                        
                    otherlv_2=(Token)match(input,12,FollowSets000.FOLLOW_4); 

                        	newLeafNode(otherlv_2, grammarAccess.getRuntimeStateRepresentationAccess().getStateKeyword_1_0_1());
                        
                    otherlv_3=(Token)match(input,13,FollowSets000.FOLLOW_5); 

                        	newLeafNode(otherlv_3, grammarAccess.getRuntimeStateRepresentationAccess().getRepresentationKeyword_1_0_2());
                        
                    // InternalRunstar.g:99:1: ( (lv_name_4_0= ruleEString ) )
                    // InternalRunstar.g:100:1: (lv_name_4_0= ruleEString )
                    {
                    // InternalRunstar.g:100:1: (lv_name_4_0= ruleEString )
                    // InternalRunstar.g:101:3: lv_name_4_0= ruleEString
                    {
                     
                    	        newCompositeNode(grammarAccess.getRuntimeStateRepresentationAccess().getNameEStringParserRuleCall_1_0_3_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_6);
                    lv_name_4_0=ruleEString();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getRuntimeStateRepresentationRule());
                    	        }
                           		set(
                           			current, 
                           			"name",
                            		lv_name_4_0, 
                            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.EString");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalRunstar.g:118:6: (otherlv_5= 'RunStaR' ( (lv_name_6_0= ruleEString ) ) )
                    {
                    // InternalRunstar.g:118:6: (otherlv_5= 'RunStaR' ( (lv_name_6_0= ruleEString ) ) )
                    // InternalRunstar.g:118:8: otherlv_5= 'RunStaR' ( (lv_name_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,14,FollowSets000.FOLLOW_5); 

                        	newLeafNode(otherlv_5, grammarAccess.getRuntimeStateRepresentationAccess().getRunStaRKeyword_1_1_0());
                        
                    // InternalRunstar.g:122:1: ( (lv_name_6_0= ruleEString ) )
                    // InternalRunstar.g:123:1: (lv_name_6_0= ruleEString )
                    {
                    // InternalRunstar.g:123:1: (lv_name_6_0= ruleEString )
                    // InternalRunstar.g:124:3: lv_name_6_0= ruleEString
                    {
                     
                    	        newCompositeNode(grammarAccess.getRuntimeStateRepresentationAccess().getNameEStringParserRuleCall_1_1_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_6);
                    lv_name_6_0=ruleEString();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getRuntimeStateRepresentationRule());
                    	        }
                           		set(
                           			current, 
                           			"name",
                            		lv_name_6_0, 
                            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.EString");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }

            // InternalRunstar.g:140:4: ( ( ( (lv_modelImports_7_0= ruleImportStatement ) ) | ( (lv_classImports_8_0= ruleImportJavaStatement ) ) ) ( ( (lv_modelImports_9_0= ruleImportStatement ) ) | ( (lv_classImports_10_0= ruleImportJavaStatement ) ) )* )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==52||(LA4_0>=54 && LA4_0<=55)) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalRunstar.g:140:5: ( ( (lv_modelImports_7_0= ruleImportStatement ) ) | ( (lv_classImports_8_0= ruleImportJavaStatement ) ) ) ( ( (lv_modelImports_9_0= ruleImportStatement ) ) | ( (lv_classImports_10_0= ruleImportJavaStatement ) ) )*
                    {
                    // InternalRunstar.g:140:5: ( ( (lv_modelImports_7_0= ruleImportStatement ) ) | ( (lv_classImports_8_0= ruleImportJavaStatement ) ) )
                    int alt2=2;
                    int LA2_0 = input.LA(1);

                    if ( (LA2_0==52) ) {
                        alt2=1;
                    }
                    else if ( ((LA2_0>=54 && LA2_0<=55)) ) {
                        alt2=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 0, input);

                        throw nvae;
                    }
                    switch (alt2) {
                        case 1 :
                            // InternalRunstar.g:140:6: ( (lv_modelImports_7_0= ruleImportStatement ) )
                            {
                            // InternalRunstar.g:140:6: ( (lv_modelImports_7_0= ruleImportStatement ) )
                            // InternalRunstar.g:141:1: (lv_modelImports_7_0= ruleImportStatement )
                            {
                            // InternalRunstar.g:141:1: (lv_modelImports_7_0= ruleImportStatement )
                            // InternalRunstar.g:142:3: lv_modelImports_7_0= ruleImportStatement
                            {
                             
                            	        newCompositeNode(grammarAccess.getRuntimeStateRepresentationAccess().getModelImportsImportStatementParserRuleCall_2_0_0_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_6);
                            lv_modelImports_7_0=ruleImportStatement();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getRuntimeStateRepresentationRule());
                            	        }
                                   		add(
                                   			current, 
                                   			"modelImports",
                                    		lv_modelImports_7_0, 
                                    		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.ImportStatement");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalRunstar.g:159:6: ( (lv_classImports_8_0= ruleImportJavaStatement ) )
                            {
                            // InternalRunstar.g:159:6: ( (lv_classImports_8_0= ruleImportJavaStatement ) )
                            // InternalRunstar.g:160:1: (lv_classImports_8_0= ruleImportJavaStatement )
                            {
                            // InternalRunstar.g:160:1: (lv_classImports_8_0= ruleImportJavaStatement )
                            // InternalRunstar.g:161:3: lv_classImports_8_0= ruleImportJavaStatement
                            {
                             
                            	        newCompositeNode(grammarAccess.getRuntimeStateRepresentationAccess().getClassImportsImportJavaStatementParserRuleCall_2_0_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_6);
                            lv_classImports_8_0=ruleImportJavaStatement();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getRuntimeStateRepresentationRule());
                            	        }
                                   		add(
                                   			current, 
                                   			"classImports",
                                    		lv_classImports_8_0, 
                                    		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.ImportJavaStatement");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // InternalRunstar.g:177:3: ( ( (lv_modelImports_9_0= ruleImportStatement ) ) | ( (lv_classImports_10_0= ruleImportJavaStatement ) ) )*
                    loop3:
                    do {
                        int alt3=3;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==52) ) {
                            alt3=1;
                        }
                        else if ( ((LA3_0>=54 && LA3_0<=55)) ) {
                            alt3=2;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // InternalRunstar.g:177:4: ( (lv_modelImports_9_0= ruleImportStatement ) )
                    	    {
                    	    // InternalRunstar.g:177:4: ( (lv_modelImports_9_0= ruleImportStatement ) )
                    	    // InternalRunstar.g:178:1: (lv_modelImports_9_0= ruleImportStatement )
                    	    {
                    	    // InternalRunstar.g:178:1: (lv_modelImports_9_0= ruleImportStatement )
                    	    // InternalRunstar.g:179:3: lv_modelImports_9_0= ruleImportStatement
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getRuntimeStateRepresentationAccess().getModelImportsImportStatementParserRuleCall_2_1_0_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_6);
                    	    lv_modelImports_9_0=ruleImportStatement();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getRuntimeStateRepresentationRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"modelImports",
                    	            		lv_modelImports_9_0, 
                    	            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.ImportStatement");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalRunstar.g:196:6: ( (lv_classImports_10_0= ruleImportJavaStatement ) )
                    	    {
                    	    // InternalRunstar.g:196:6: ( (lv_classImports_10_0= ruleImportJavaStatement ) )
                    	    // InternalRunstar.g:197:1: (lv_classImports_10_0= ruleImportJavaStatement )
                    	    {
                    	    // InternalRunstar.g:197:1: (lv_classImports_10_0= ruleImportJavaStatement )
                    	    // InternalRunstar.g:198:3: lv_classImports_10_0= ruleImportJavaStatement
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getRuntimeStateRepresentationAccess().getClassImportsImportJavaStatementParserRuleCall_2_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_6);
                    	    lv_classImports_10_0=ruleImportJavaStatement();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getRuntimeStateRepresentationRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"classImports",
                    	            		lv_classImports_10_0, 
                    	            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.ImportJavaStatement");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);


                    }
                    break;

            }

            // InternalRunstar.g:214:6: ( ( (lv_variables_11_0= ruleVariable ) ) | ( (lv_representations_12_0= ruleRepresentation ) ) )*
            loop5:
            do {
                int alt5=3;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==15) ) {
                    alt5=1;
                }
                else if ( (LA5_0==58) ) {
                    alt5=2;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalRunstar.g:214:7: ( (lv_variables_11_0= ruleVariable ) )
            	    {
            	    // InternalRunstar.g:214:7: ( (lv_variables_11_0= ruleVariable ) )
            	    // InternalRunstar.g:215:1: (lv_variables_11_0= ruleVariable )
            	    {
            	    // InternalRunstar.g:215:1: (lv_variables_11_0= ruleVariable )
            	    // InternalRunstar.g:216:3: lv_variables_11_0= ruleVariable
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getRuntimeStateRepresentationAccess().getVariablesVariableParserRuleCall_3_0_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_7);
            	    lv_variables_11_0=ruleVariable();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getRuntimeStateRepresentationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"variables",
            	            		lv_variables_11_0, 
            	            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.Variable");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalRunstar.g:233:6: ( (lv_representations_12_0= ruleRepresentation ) )
            	    {
            	    // InternalRunstar.g:233:6: ( (lv_representations_12_0= ruleRepresentation ) )
            	    // InternalRunstar.g:234:1: (lv_representations_12_0= ruleRepresentation )
            	    {
            	    // InternalRunstar.g:234:1: (lv_representations_12_0= ruleRepresentation )
            	    // InternalRunstar.g:235:3: lv_representations_12_0= ruleRepresentation
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getRuntimeStateRepresentationAccess().getRepresentationsRepresentationParserRuleCall_3_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_7);
            	    lv_representations_12_0=ruleRepresentation();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getRuntimeStateRepresentationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"representations",
            	            		lv_representations_12_0, 
            	            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.Representation");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRuntimeStateRepresentation"


    // $ANTLR start "entryRuleVariable"
    // InternalRunstar.g:259:1: entryRuleVariable returns [EObject current=null] : iv_ruleVariable= ruleVariable EOF ;
    public final EObject entryRuleVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariable = null;


        try {
            // InternalRunstar.g:260:2: (iv_ruleVariable= ruleVariable EOF )
            // InternalRunstar.g:261:2: iv_ruleVariable= ruleVariable EOF
            {
             newCompositeNode(grammarAccess.getVariableRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariable=ruleVariable();

            state._fsp--;

             current =iv_ruleVariable; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalRunstar.g:268:1: ruleVariable returns [EObject current=null] : (otherlv_0= 'let' (this_ImageVar_1= ruleImageVar | this_TextVar_2= ruleTextVar | this_ChartVar_3= ruleChartVar ) otherlv_4= ';' ) ;
    public final EObject ruleVariable() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_4=null;
        EObject this_ImageVar_1 = null;

        EObject this_TextVar_2 = null;

        EObject this_ChartVar_3 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:271:28: ( (otherlv_0= 'let' (this_ImageVar_1= ruleImageVar | this_TextVar_2= ruleTextVar | this_ChartVar_3= ruleChartVar ) otherlv_4= ';' ) )
            // InternalRunstar.g:272:1: (otherlv_0= 'let' (this_ImageVar_1= ruleImageVar | this_TextVar_2= ruleTextVar | this_ChartVar_3= ruleChartVar ) otherlv_4= ';' )
            {
            // InternalRunstar.g:272:1: (otherlv_0= 'let' (this_ImageVar_1= ruleImageVar | this_TextVar_2= ruleTextVar | this_ChartVar_3= ruleChartVar ) otherlv_4= ';' )
            // InternalRunstar.g:272:3: otherlv_0= 'let' (this_ImageVar_1= ruleImageVar | this_TextVar_2= ruleTextVar | this_ChartVar_3= ruleChartVar ) otherlv_4= ';'
            {
            otherlv_0=(Token)match(input,15,FollowSets000.FOLLOW_8); 

                	newLeafNode(otherlv_0, grammarAccess.getVariableAccess().getLetKeyword_0());
                
            // InternalRunstar.g:276:1: (this_ImageVar_1= ruleImageVar | this_TextVar_2= ruleTextVar | this_ChartVar_3= ruleChartVar )
            int alt6=3;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt6=1;
                }
                break;
            case 21:
            case 23:
            case 38:
                {
                alt6=2;
                }
                break;
            case 20:
            case 24:
                {
                alt6=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalRunstar.g:277:5: this_ImageVar_1= ruleImageVar
                    {
                     
                            newCompositeNode(grammarAccess.getVariableAccess().getImageVarParserRuleCall_1_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_9);
                    this_ImageVar_1=ruleImageVar();

                    state._fsp--;

                     
                            current = this_ImageVar_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalRunstar.g:287:5: this_TextVar_2= ruleTextVar
                    {
                     
                            newCompositeNode(grammarAccess.getVariableAccess().getTextVarParserRuleCall_1_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_9);
                    this_TextVar_2=ruleTextVar();

                    state._fsp--;

                     
                            current = this_TextVar_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalRunstar.g:297:5: this_ChartVar_3= ruleChartVar
                    {
                     
                            newCompositeNode(grammarAccess.getVariableAccess().getChartVarParserRuleCall_1_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_9);
                    this_ChartVar_3=ruleChartVar();

                    state._fsp--;

                     
                            current = this_ChartVar_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }

            otherlv_4=(Token)match(input,16,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_4, grammarAccess.getVariableAccess().getSemicolonKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleTextVar"
    // InternalRunstar.g:317:1: entryRuleTextVar returns [EObject current=null] : iv_ruleTextVar= ruleTextVar EOF ;
    public final EObject entryRuleTextVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTextVar = null;


        try {
            // InternalRunstar.g:318:2: (iv_ruleTextVar= ruleTextVar EOF )
            // InternalRunstar.g:319:2: iv_ruleTextVar= ruleTextVar EOF
            {
             newCompositeNode(grammarAccess.getTextVarRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTextVar=ruleTextVar();

            state._fsp--;

             current =iv_ruleTextVar; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTextVar"


    // $ANTLR start "ruleTextVar"
    // InternalRunstar.g:326:1: ruleTextVar returns [EObject current=null] : (this_DecoratedTextVar_0= ruleDecoratedTextVar | this_ConstTextVar_1= ruleConstTextVar | this_RTDVar_2= ruleRTDVar ) ;
    public final EObject ruleTextVar() throws RecognitionException {
        EObject current = null;

        EObject this_DecoratedTextVar_0 = null;

        EObject this_ConstTextVar_1 = null;

        EObject this_RTDVar_2 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:329:28: ( (this_DecoratedTextVar_0= ruleDecoratedTextVar | this_ConstTextVar_1= ruleConstTextVar | this_RTDVar_2= ruleRTDVar ) )
            // InternalRunstar.g:330:1: (this_DecoratedTextVar_0= ruleDecoratedTextVar | this_ConstTextVar_1= ruleConstTextVar | this_RTDVar_2= ruleRTDVar )
            {
            // InternalRunstar.g:330:1: (this_DecoratedTextVar_0= ruleDecoratedTextVar | this_ConstTextVar_1= ruleConstTextVar | this_RTDVar_2= ruleRTDVar )
            int alt7=3;
            switch ( input.LA(1) ) {
            case 38:
                {
                alt7=1;
                }
                break;
            case 23:
                {
                alt7=2;
                }
                break;
            case 21:
                {
                alt7=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // InternalRunstar.g:331:5: this_DecoratedTextVar_0= ruleDecoratedTextVar
                    {
                     
                            newCompositeNode(grammarAccess.getTextVarAccess().getDecoratedTextVarParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_DecoratedTextVar_0=ruleDecoratedTextVar();

                    state._fsp--;

                     
                            current = this_DecoratedTextVar_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalRunstar.g:341:5: this_ConstTextVar_1= ruleConstTextVar
                    {
                     
                            newCompositeNode(grammarAccess.getTextVarAccess().getConstTextVarParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ConstTextVar_1=ruleConstTextVar();

                    state._fsp--;

                     
                            current = this_ConstTextVar_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalRunstar.g:351:5: this_RTDVar_2= ruleRTDVar
                    {
                     
                            newCompositeNode(grammarAccess.getTextVarAccess().getRTDVarParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RTDVar_2=ruleRTDVar();

                    state._fsp--;

                     
                            current = this_RTDVar_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTextVar"


    // $ANTLR start "entryRuleImageVar"
    // InternalRunstar.g:367:1: entryRuleImageVar returns [EObject current=null] : iv_ruleImageVar= ruleImageVar EOF ;
    public final EObject entryRuleImageVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImageVar = null;


        try {
            // InternalRunstar.g:368:2: (iv_ruleImageVar= ruleImageVar EOF )
            // InternalRunstar.g:369:2: iv_ruleImageVar= ruleImageVar EOF
            {
             newCompositeNode(grammarAccess.getImageVarRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleImageVar=ruleImageVar();

            state._fsp--;

             current =iv_ruleImageVar; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImageVar"


    // $ANTLR start "ruleImageVar"
    // InternalRunstar.g:376:1: ruleImageVar returns [EObject current=null] : (otherlv_0= 'Image' otherlv_1= '(' ( (lv_path_2_0= RULE_STRING ) ) ( (lv_strategy_3_0= ruleRStrategy ) ) otherlv_4= ')' otherlv_5= 'be' ( (lv_name_6_0= RULE_ID ) ) ) ;
    public final EObject ruleImageVar() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_path_2_0=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token lv_name_6_0=null;
        EObject lv_strategy_3_0 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:379:28: ( (otherlv_0= 'Image' otherlv_1= '(' ( (lv_path_2_0= RULE_STRING ) ) ( (lv_strategy_3_0= ruleRStrategy ) ) otherlv_4= ')' otherlv_5= 'be' ( (lv_name_6_0= RULE_ID ) ) ) )
            // InternalRunstar.g:380:1: (otherlv_0= 'Image' otherlv_1= '(' ( (lv_path_2_0= RULE_STRING ) ) ( (lv_strategy_3_0= ruleRStrategy ) ) otherlv_4= ')' otherlv_5= 'be' ( (lv_name_6_0= RULE_ID ) ) )
            {
            // InternalRunstar.g:380:1: (otherlv_0= 'Image' otherlv_1= '(' ( (lv_path_2_0= RULE_STRING ) ) ( (lv_strategy_3_0= ruleRStrategy ) ) otherlv_4= ')' otherlv_5= 'be' ( (lv_name_6_0= RULE_ID ) ) )
            // InternalRunstar.g:380:3: otherlv_0= 'Image' otherlv_1= '(' ( (lv_path_2_0= RULE_STRING ) ) ( (lv_strategy_3_0= ruleRStrategy ) ) otherlv_4= ')' otherlv_5= 'be' ( (lv_name_6_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,17,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_0, grammarAccess.getImageVarAccess().getImageKeyword_0());
                
            otherlv_1=(Token)match(input,18,FollowSets000.FOLLOW_11); 

                	newLeafNode(otherlv_1, grammarAccess.getImageVarAccess().getLeftParenthesisKeyword_1());
                
            // InternalRunstar.g:388:1: ( (lv_path_2_0= RULE_STRING ) )
            // InternalRunstar.g:389:1: (lv_path_2_0= RULE_STRING )
            {
            // InternalRunstar.g:389:1: (lv_path_2_0= RULE_STRING )
            // InternalRunstar.g:390:3: lv_path_2_0= RULE_STRING
            {
            lv_path_2_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_12); 

            			newLeafNode(lv_path_2_0, grammarAccess.getImageVarAccess().getPathSTRINGTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getImageVarRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"path",
                    		lv_path_2_0, 
                    		"org.eclipse.xtext.common.Terminals.STRING");
            	    

            }


            }

            // InternalRunstar.g:406:2: ( (lv_strategy_3_0= ruleRStrategy ) )
            // InternalRunstar.g:407:1: (lv_strategy_3_0= ruleRStrategy )
            {
            // InternalRunstar.g:407:1: (lv_strategy_3_0= ruleRStrategy )
            // InternalRunstar.g:408:3: lv_strategy_3_0= ruleRStrategy
            {
             
            	        newCompositeNode(grammarAccess.getImageVarAccess().getStrategyRStrategyParserRuleCall_3_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_13);
            lv_strategy_3_0=ruleRStrategy();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getImageVarRule());
            	        }
                   		set(
                   			current, 
                   			"strategy",
                    		lv_strategy_3_0, 
                    		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.RStrategy");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,19,FollowSets000.FOLLOW_14); 

                	newLeafNode(otherlv_4, grammarAccess.getImageVarAccess().getRightParenthesisKeyword_4());
                
            otherlv_5=(Token)match(input,20,FollowSets000.FOLLOW_15); 

                	newLeafNode(otherlv_5, grammarAccess.getImageVarAccess().getBeKeyword_5());
                
            // InternalRunstar.g:432:1: ( (lv_name_6_0= RULE_ID ) )
            // InternalRunstar.g:433:1: (lv_name_6_0= RULE_ID )
            {
            // InternalRunstar.g:433:1: (lv_name_6_0= RULE_ID )
            // InternalRunstar.g:434:3: lv_name_6_0= RULE_ID
            {
            lv_name_6_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

            			newLeafNode(lv_name_6_0, grammarAccess.getImageVarAccess().getNameIDTerminalRuleCall_6_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getImageVarRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_6_0, 
                    		"org.eclipse.xtext.common.Terminals.ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImageVar"


    // $ANTLR start "entryRuleRTDVar"
    // InternalRunstar.g:458:1: entryRuleRTDVar returns [EObject current=null] : iv_ruleRTDVar= ruleRTDVar EOF ;
    public final EObject entryRuleRTDVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRTDVar = null;


        try {
            // InternalRunstar.g:459:2: (iv_ruleRTDVar= ruleRTDVar EOF )
            // InternalRunstar.g:460:2: iv_ruleRTDVar= ruleRTDVar EOF
            {
             newCompositeNode(grammarAccess.getRTDVarRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRTDVar=ruleRTDVar();

            state._fsp--;

             current =iv_ruleRTDVar; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRTDVar"


    // $ANTLR start "ruleRTDVar"
    // InternalRunstar.g:467:1: ruleRTDVar returns [EObject current=null] : (otherlv_0= 'Variable' otherlv_1= '(' ( (lv_attributes_2_0= ruleEString ) ) (otherlv_3= ',' ( (lv_attributes_4_0= ruleEString ) ) )* ( (lv_strategy_5_0= ruleRStrategy ) ) otherlv_6= ')' otherlv_7= 'be' ( (lv_name_8_0= RULE_ID ) ) ) ;
    public final EObject ruleRTDVar() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token lv_name_8_0=null;
        AntlrDatatypeRuleToken lv_attributes_2_0 = null;

        AntlrDatatypeRuleToken lv_attributes_4_0 = null;

        EObject lv_strategy_5_0 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:470:28: ( (otherlv_0= 'Variable' otherlv_1= '(' ( (lv_attributes_2_0= ruleEString ) ) (otherlv_3= ',' ( (lv_attributes_4_0= ruleEString ) ) )* ( (lv_strategy_5_0= ruleRStrategy ) ) otherlv_6= ')' otherlv_7= 'be' ( (lv_name_8_0= RULE_ID ) ) ) )
            // InternalRunstar.g:471:1: (otherlv_0= 'Variable' otherlv_1= '(' ( (lv_attributes_2_0= ruleEString ) ) (otherlv_3= ',' ( (lv_attributes_4_0= ruleEString ) ) )* ( (lv_strategy_5_0= ruleRStrategy ) ) otherlv_6= ')' otherlv_7= 'be' ( (lv_name_8_0= RULE_ID ) ) )
            {
            // InternalRunstar.g:471:1: (otherlv_0= 'Variable' otherlv_1= '(' ( (lv_attributes_2_0= ruleEString ) ) (otherlv_3= ',' ( (lv_attributes_4_0= ruleEString ) ) )* ( (lv_strategy_5_0= ruleRStrategy ) ) otherlv_6= ')' otherlv_7= 'be' ( (lv_name_8_0= RULE_ID ) ) )
            // InternalRunstar.g:471:3: otherlv_0= 'Variable' otherlv_1= '(' ( (lv_attributes_2_0= ruleEString ) ) (otherlv_3= ',' ( (lv_attributes_4_0= ruleEString ) ) )* ( (lv_strategy_5_0= ruleRStrategy ) ) otherlv_6= ')' otherlv_7= 'be' ( (lv_name_8_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,21,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_0, grammarAccess.getRTDVarAccess().getVariableKeyword_0());
                
            otherlv_1=(Token)match(input,18,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_1, grammarAccess.getRTDVarAccess().getLeftParenthesisKeyword_1());
                
            // InternalRunstar.g:479:1: ( (lv_attributes_2_0= ruleEString ) )
            // InternalRunstar.g:480:1: (lv_attributes_2_0= ruleEString )
            {
            // InternalRunstar.g:480:1: (lv_attributes_2_0= ruleEString )
            // InternalRunstar.g:481:3: lv_attributes_2_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getRTDVarAccess().getAttributesEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_16);
            lv_attributes_2_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRTDVarRule());
            	        }
                   		add(
                   			current, 
                   			"attributes",
                    		lv_attributes_2_0, 
                    		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalRunstar.g:497:2: (otherlv_3= ',' ( (lv_attributes_4_0= ruleEString ) ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==22) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalRunstar.g:497:4: otherlv_3= ',' ( (lv_attributes_4_0= ruleEString ) )
            	    {
            	    otherlv_3=(Token)match(input,22,FollowSets000.FOLLOW_5); 

            	        	newLeafNode(otherlv_3, grammarAccess.getRTDVarAccess().getCommaKeyword_3_0());
            	        
            	    // InternalRunstar.g:501:1: ( (lv_attributes_4_0= ruleEString ) )
            	    // InternalRunstar.g:502:1: (lv_attributes_4_0= ruleEString )
            	    {
            	    // InternalRunstar.g:502:1: (lv_attributes_4_0= ruleEString )
            	    // InternalRunstar.g:503:3: lv_attributes_4_0= ruleEString
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getRTDVarAccess().getAttributesEStringParserRuleCall_3_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_16);
            	    lv_attributes_4_0=ruleEString();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getRTDVarRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"attributes",
            	            		lv_attributes_4_0, 
            	            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.EString");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            // InternalRunstar.g:519:4: ( (lv_strategy_5_0= ruleRStrategy ) )
            // InternalRunstar.g:520:1: (lv_strategy_5_0= ruleRStrategy )
            {
            // InternalRunstar.g:520:1: (lv_strategy_5_0= ruleRStrategy )
            // InternalRunstar.g:521:3: lv_strategy_5_0= ruleRStrategy
            {
             
            	        newCompositeNode(grammarAccess.getRTDVarAccess().getStrategyRStrategyParserRuleCall_4_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_13);
            lv_strategy_5_0=ruleRStrategy();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRTDVarRule());
            	        }
                   		set(
                   			current, 
                   			"strategy",
                    		lv_strategy_5_0, 
                    		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.RStrategy");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_6=(Token)match(input,19,FollowSets000.FOLLOW_14); 

                	newLeafNode(otherlv_6, grammarAccess.getRTDVarAccess().getRightParenthesisKeyword_5());
                
            otherlv_7=(Token)match(input,20,FollowSets000.FOLLOW_15); 

                	newLeafNode(otherlv_7, grammarAccess.getRTDVarAccess().getBeKeyword_6());
                
            // InternalRunstar.g:545:1: ( (lv_name_8_0= RULE_ID ) )
            // InternalRunstar.g:546:1: (lv_name_8_0= RULE_ID )
            {
            // InternalRunstar.g:546:1: (lv_name_8_0= RULE_ID )
            // InternalRunstar.g:547:3: lv_name_8_0= RULE_ID
            {
            lv_name_8_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

            			newLeafNode(lv_name_8_0, grammarAccess.getRTDVarAccess().getNameIDTerminalRuleCall_7_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRTDVarRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_8_0, 
                    		"org.eclipse.xtext.common.Terminals.ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRTDVar"


    // $ANTLR start "entryRuleConstTextVar"
    // InternalRunstar.g:571:1: entryRuleConstTextVar returns [EObject current=null] : iv_ruleConstTextVar= ruleConstTextVar EOF ;
    public final EObject entryRuleConstTextVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstTextVar = null;


        try {
            // InternalRunstar.g:572:2: (iv_ruleConstTextVar= ruleConstTextVar EOF )
            // InternalRunstar.g:573:2: iv_ruleConstTextVar= ruleConstTextVar EOF
            {
             newCompositeNode(grammarAccess.getConstTextVarRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConstTextVar=ruleConstTextVar();

            state._fsp--;

             current =iv_ruleConstTextVar; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstTextVar"


    // $ANTLR start "ruleConstTextVar"
    // InternalRunstar.g:580:1: ruleConstTextVar returns [EObject current=null] : (otherlv_0= 'ConstText' otherlv_1= '(' ( (lv_value_2_0= RULE_STRING ) ) ( (lv_strategy_3_0= ruleRStrategy ) ) otherlv_4= ')' otherlv_5= 'be' ( (lv_name_6_0= RULE_ID ) ) ) ;
    public final EObject ruleConstTextVar() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_value_2_0=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token lv_name_6_0=null;
        EObject lv_strategy_3_0 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:583:28: ( (otherlv_0= 'ConstText' otherlv_1= '(' ( (lv_value_2_0= RULE_STRING ) ) ( (lv_strategy_3_0= ruleRStrategy ) ) otherlv_4= ')' otherlv_5= 'be' ( (lv_name_6_0= RULE_ID ) ) ) )
            // InternalRunstar.g:584:1: (otherlv_0= 'ConstText' otherlv_1= '(' ( (lv_value_2_0= RULE_STRING ) ) ( (lv_strategy_3_0= ruleRStrategy ) ) otherlv_4= ')' otherlv_5= 'be' ( (lv_name_6_0= RULE_ID ) ) )
            {
            // InternalRunstar.g:584:1: (otherlv_0= 'ConstText' otherlv_1= '(' ( (lv_value_2_0= RULE_STRING ) ) ( (lv_strategy_3_0= ruleRStrategy ) ) otherlv_4= ')' otherlv_5= 'be' ( (lv_name_6_0= RULE_ID ) ) )
            // InternalRunstar.g:584:3: otherlv_0= 'ConstText' otherlv_1= '(' ( (lv_value_2_0= RULE_STRING ) ) ( (lv_strategy_3_0= ruleRStrategy ) ) otherlv_4= ')' otherlv_5= 'be' ( (lv_name_6_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,23,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_0, grammarAccess.getConstTextVarAccess().getConstTextKeyword_0());
                
            otherlv_1=(Token)match(input,18,FollowSets000.FOLLOW_11); 

                	newLeafNode(otherlv_1, grammarAccess.getConstTextVarAccess().getLeftParenthesisKeyword_1());
                
            // InternalRunstar.g:592:1: ( (lv_value_2_0= RULE_STRING ) )
            // InternalRunstar.g:593:1: (lv_value_2_0= RULE_STRING )
            {
            // InternalRunstar.g:593:1: (lv_value_2_0= RULE_STRING )
            // InternalRunstar.g:594:3: lv_value_2_0= RULE_STRING
            {
            lv_value_2_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_12); 

            			newLeafNode(lv_value_2_0, grammarAccess.getConstTextVarAccess().getValueSTRINGTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getConstTextVarRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_2_0, 
                    		"org.eclipse.xtext.common.Terminals.STRING");
            	    

            }


            }

            // InternalRunstar.g:610:2: ( (lv_strategy_3_0= ruleRStrategy ) )
            // InternalRunstar.g:611:1: (lv_strategy_3_0= ruleRStrategy )
            {
            // InternalRunstar.g:611:1: (lv_strategy_3_0= ruleRStrategy )
            // InternalRunstar.g:612:3: lv_strategy_3_0= ruleRStrategy
            {
             
            	        newCompositeNode(grammarAccess.getConstTextVarAccess().getStrategyRStrategyParserRuleCall_3_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_13);
            lv_strategy_3_0=ruleRStrategy();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getConstTextVarRule());
            	        }
                   		set(
                   			current, 
                   			"strategy",
                    		lv_strategy_3_0, 
                    		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.RStrategy");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,19,FollowSets000.FOLLOW_14); 

                	newLeafNode(otherlv_4, grammarAccess.getConstTextVarAccess().getRightParenthesisKeyword_4());
                
            otherlv_5=(Token)match(input,20,FollowSets000.FOLLOW_15); 

                	newLeafNode(otherlv_5, grammarAccess.getConstTextVarAccess().getBeKeyword_5());
                
            // InternalRunstar.g:636:1: ( (lv_name_6_0= RULE_ID ) )
            // InternalRunstar.g:637:1: (lv_name_6_0= RULE_ID )
            {
            // InternalRunstar.g:637:1: (lv_name_6_0= RULE_ID )
            // InternalRunstar.g:638:3: lv_name_6_0= RULE_ID
            {
            lv_name_6_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

            			newLeafNode(lv_name_6_0, grammarAccess.getConstTextVarAccess().getNameIDTerminalRuleCall_6_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getConstTextVarRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_6_0, 
                    		"org.eclipse.xtext.common.Terminals.ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstTextVar"


    // $ANTLR start "entryRuleChartVar"
    // InternalRunstar.g:662:1: entryRuleChartVar returns [EObject current=null] : iv_ruleChartVar= ruleChartVar EOF ;
    public final EObject entryRuleChartVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleChartVar = null;


        try {
            // InternalRunstar.g:663:2: (iv_ruleChartVar= ruleChartVar EOF )
            // InternalRunstar.g:664:2: iv_ruleChartVar= ruleChartVar EOF
            {
             newCompositeNode(grammarAccess.getChartVarRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleChartVar=ruleChartVar();

            state._fsp--;

             current =iv_ruleChartVar; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleChartVar"


    // $ANTLR start "ruleChartVar"
    // InternalRunstar.g:671:1: ruleChartVar returns [EObject current=null] : ( (otherlv_0= 'Chart' otherlv_1= '(' otherlv_2= 'x' otherlv_3= '=' ( (otherlv_4= RULE_ID ) ) otherlv_5= ',' otherlv_6= 'y' otherlv_7= '=' ( (otherlv_8= RULE_ID ) ) (otherlv_9= '+' ( (otherlv_10= RULE_ID ) ) )* otherlv_11= ',' otherlv_12= 'path' otherlv_13= '=' ( (lv_path_14_0= RULE_STRING ) ) otherlv_15= ')' (otherlv_16= 'with' otherlv_17= '{' ( ( ( ( ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) ) )* ) ) ) otherlv_39= '}' otherlv_40= 'be' ( (lv_name_41_0= RULE_ID ) ) ) ) | (otherlv_42= 'be' ( (lv_name_43_0= RULE_ID ) ) ) ) ;
    public final EObject ruleChartVar() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token lv_path_14_0=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token lv_title_21_0=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token lv_xlabel_25_0=null;
        Token otherlv_26=null;
        Token otherlv_27=null;
        Token otherlv_28=null;
        Token lv_ylabel_29_0=null;
        Token otherlv_30=null;
        Token otherlv_31=null;
        Token otherlv_32=null;
        Token lv_nbValuesDisplayed_33_0=null;
        Token otherlv_34=null;
        Token otherlv_35=null;
        Token otherlv_36=null;
        Token otherlv_38=null;
        Token otherlv_39=null;
        Token otherlv_40=null;
        Token lv_name_41_0=null;
        Token otherlv_42=null;
        Token lv_name_43_0=null;
        Enumerator lv_lineStyle_37_0 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:674:28: ( ( (otherlv_0= 'Chart' otherlv_1= '(' otherlv_2= 'x' otherlv_3= '=' ( (otherlv_4= RULE_ID ) ) otherlv_5= ',' otherlv_6= 'y' otherlv_7= '=' ( (otherlv_8= RULE_ID ) ) (otherlv_9= '+' ( (otherlv_10= RULE_ID ) ) )* otherlv_11= ',' otherlv_12= 'path' otherlv_13= '=' ( (lv_path_14_0= RULE_STRING ) ) otherlv_15= ')' (otherlv_16= 'with' otherlv_17= '{' ( ( ( ( ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) ) )* ) ) ) otherlv_39= '}' otherlv_40= 'be' ( (lv_name_41_0= RULE_ID ) ) ) ) | (otherlv_42= 'be' ( (lv_name_43_0= RULE_ID ) ) ) ) )
            // InternalRunstar.g:675:1: ( (otherlv_0= 'Chart' otherlv_1= '(' otherlv_2= 'x' otherlv_3= '=' ( (otherlv_4= RULE_ID ) ) otherlv_5= ',' otherlv_6= 'y' otherlv_7= '=' ( (otherlv_8= RULE_ID ) ) (otherlv_9= '+' ( (otherlv_10= RULE_ID ) ) )* otherlv_11= ',' otherlv_12= 'path' otherlv_13= '=' ( (lv_path_14_0= RULE_STRING ) ) otherlv_15= ')' (otherlv_16= 'with' otherlv_17= '{' ( ( ( ( ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) ) )* ) ) ) otherlv_39= '}' otherlv_40= 'be' ( (lv_name_41_0= RULE_ID ) ) ) ) | (otherlv_42= 'be' ( (lv_name_43_0= RULE_ID ) ) ) )
            {
            // InternalRunstar.g:675:1: ( (otherlv_0= 'Chart' otherlv_1= '(' otherlv_2= 'x' otherlv_3= '=' ( (otherlv_4= RULE_ID ) ) otherlv_5= ',' otherlv_6= 'y' otherlv_7= '=' ( (otherlv_8= RULE_ID ) ) (otherlv_9= '+' ( (otherlv_10= RULE_ID ) ) )* otherlv_11= ',' otherlv_12= 'path' otherlv_13= '=' ( (lv_path_14_0= RULE_STRING ) ) otherlv_15= ')' (otherlv_16= 'with' otherlv_17= '{' ( ( ( ( ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) ) )* ) ) ) otherlv_39= '}' otherlv_40= 'be' ( (lv_name_41_0= RULE_ID ) ) ) ) | (otherlv_42= 'be' ( (lv_name_43_0= RULE_ID ) ) ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==24) ) {
                alt11=1;
            }
            else if ( (LA11_0==20) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalRunstar.g:675:2: (otherlv_0= 'Chart' otherlv_1= '(' otherlv_2= 'x' otherlv_3= '=' ( (otherlv_4= RULE_ID ) ) otherlv_5= ',' otherlv_6= 'y' otherlv_7= '=' ( (otherlv_8= RULE_ID ) ) (otherlv_9= '+' ( (otherlv_10= RULE_ID ) ) )* otherlv_11= ',' otherlv_12= 'path' otherlv_13= '=' ( (lv_path_14_0= RULE_STRING ) ) otherlv_15= ')' (otherlv_16= 'with' otherlv_17= '{' ( ( ( ( ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) ) )* ) ) ) otherlv_39= '}' otherlv_40= 'be' ( (lv_name_41_0= RULE_ID ) ) ) )
                    {
                    // InternalRunstar.g:675:2: (otherlv_0= 'Chart' otherlv_1= '(' otherlv_2= 'x' otherlv_3= '=' ( (otherlv_4= RULE_ID ) ) otherlv_5= ',' otherlv_6= 'y' otherlv_7= '=' ( (otherlv_8= RULE_ID ) ) (otherlv_9= '+' ( (otherlv_10= RULE_ID ) ) )* otherlv_11= ',' otherlv_12= 'path' otherlv_13= '=' ( (lv_path_14_0= RULE_STRING ) ) otherlv_15= ')' (otherlv_16= 'with' otherlv_17= '{' ( ( ( ( ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) ) )* ) ) ) otherlv_39= '}' otherlv_40= 'be' ( (lv_name_41_0= RULE_ID ) ) ) )
                    // InternalRunstar.g:675:4: otherlv_0= 'Chart' otherlv_1= '(' otherlv_2= 'x' otherlv_3= '=' ( (otherlv_4= RULE_ID ) ) otherlv_5= ',' otherlv_6= 'y' otherlv_7= '=' ( (otherlv_8= RULE_ID ) ) (otherlv_9= '+' ( (otherlv_10= RULE_ID ) ) )* otherlv_11= ',' otherlv_12= 'path' otherlv_13= '=' ( (lv_path_14_0= RULE_STRING ) ) otherlv_15= ')' (otherlv_16= 'with' otherlv_17= '{' ( ( ( ( ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) ) )* ) ) ) otherlv_39= '}' otherlv_40= 'be' ( (lv_name_41_0= RULE_ID ) ) )
                    {
                    otherlv_0=(Token)match(input,24,FollowSets000.FOLLOW_10); 

                        	newLeafNode(otherlv_0, grammarAccess.getChartVarAccess().getChartKeyword_0_0());
                        
                    otherlv_1=(Token)match(input,18,FollowSets000.FOLLOW_17); 

                        	newLeafNode(otherlv_1, grammarAccess.getChartVarAccess().getLeftParenthesisKeyword_0_1());
                        
                    otherlv_2=(Token)match(input,25,FollowSets000.FOLLOW_18); 

                        	newLeafNode(otherlv_2, grammarAccess.getChartVarAccess().getXKeyword_0_2());
                        
                    otherlv_3=(Token)match(input,26,FollowSets000.FOLLOW_15); 

                        	newLeafNode(otherlv_3, grammarAccess.getChartVarAccess().getEqualsSignKeyword_0_3());
                        
                    // InternalRunstar.g:691:1: ( (otherlv_4= RULE_ID ) )
                    // InternalRunstar.g:692:1: (otherlv_4= RULE_ID )
                    {
                    // InternalRunstar.g:692:1: (otherlv_4= RULE_ID )
                    // InternalRunstar.g:693:3: otherlv_4= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getChartVarRule());
                    	        }
                            
                    otherlv_4=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_19); 

                    		newLeafNode(otherlv_4, grammarAccess.getChartVarAccess().getXrtdRTDVarCrossReference_0_4_0()); 
                    	

                    }


                    }

                    otherlv_5=(Token)match(input,22,FollowSets000.FOLLOW_20); 

                        	newLeafNode(otherlv_5, grammarAccess.getChartVarAccess().getCommaKeyword_0_5());
                        
                    otherlv_6=(Token)match(input,27,FollowSets000.FOLLOW_18); 

                        	newLeafNode(otherlv_6, grammarAccess.getChartVarAccess().getYKeyword_0_6());
                        
                    otherlv_7=(Token)match(input,26,FollowSets000.FOLLOW_15); 

                        	newLeafNode(otherlv_7, grammarAccess.getChartVarAccess().getEqualsSignKeyword_0_7());
                        
                    // InternalRunstar.g:716:1: ( (otherlv_8= RULE_ID ) )
                    // InternalRunstar.g:717:1: (otherlv_8= RULE_ID )
                    {
                    // InternalRunstar.g:717:1: (otherlv_8= RULE_ID )
                    // InternalRunstar.g:718:3: otherlv_8= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getChartVarRule());
                    	        }
                            
                    otherlv_8=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_21); 

                    		newLeafNode(otherlv_8, grammarAccess.getChartVarAccess().getYrtdsRTDVarCrossReference_0_8_0()); 
                    	

                    }


                    }

                    // InternalRunstar.g:729:2: (otherlv_9= '+' ( (otherlv_10= RULE_ID ) ) )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==28) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // InternalRunstar.g:729:4: otherlv_9= '+' ( (otherlv_10= RULE_ID ) )
                    	    {
                    	    otherlv_9=(Token)match(input,28,FollowSets000.FOLLOW_15); 

                    	        	newLeafNode(otherlv_9, grammarAccess.getChartVarAccess().getPlusSignKeyword_0_9_0());
                    	        
                    	    // InternalRunstar.g:733:1: ( (otherlv_10= RULE_ID ) )
                    	    // InternalRunstar.g:734:1: (otherlv_10= RULE_ID )
                    	    {
                    	    // InternalRunstar.g:734:1: (otherlv_10= RULE_ID )
                    	    // InternalRunstar.g:735:3: otherlv_10= RULE_ID
                    	    {

                    	    			if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getChartVarRule());
                    	    	        }
                    	            
                    	    otherlv_10=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_21); 

                    	    		newLeafNode(otherlv_10, grammarAccess.getChartVarAccess().getYrtdsRTDVarCrossReference_0_9_1_0()); 
                    	    	

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    otherlv_11=(Token)match(input,22,FollowSets000.FOLLOW_22); 

                        	newLeafNode(otherlv_11, grammarAccess.getChartVarAccess().getCommaKeyword_0_10());
                        
                    otherlv_12=(Token)match(input,29,FollowSets000.FOLLOW_18); 

                        	newLeafNode(otherlv_12, grammarAccess.getChartVarAccess().getPathKeyword_0_11());
                        
                    otherlv_13=(Token)match(input,26,FollowSets000.FOLLOW_11); 

                        	newLeafNode(otherlv_13, grammarAccess.getChartVarAccess().getEqualsSignKeyword_0_12());
                        
                    // InternalRunstar.g:758:1: ( (lv_path_14_0= RULE_STRING ) )
                    // InternalRunstar.g:759:1: (lv_path_14_0= RULE_STRING )
                    {
                    // InternalRunstar.g:759:1: (lv_path_14_0= RULE_STRING )
                    // InternalRunstar.g:760:3: lv_path_14_0= RULE_STRING
                    {
                    lv_path_14_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_13); 

                    			newLeafNode(lv_path_14_0, grammarAccess.getChartVarAccess().getPathSTRINGTerminalRuleCall_0_13_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getChartVarRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"path",
                            		lv_path_14_0, 
                            		"org.eclipse.xtext.common.Terminals.STRING");
                    	    

                    }


                    }

                    otherlv_15=(Token)match(input,19,FollowSets000.FOLLOW_23); 

                        	newLeafNode(otherlv_15, grammarAccess.getChartVarAccess().getRightParenthesisKeyword_0_14());
                        
                    // InternalRunstar.g:780:1: (otherlv_16= 'with' otherlv_17= '{' ( ( ( ( ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) ) )* ) ) ) otherlv_39= '}' otherlv_40= 'be' ( (lv_name_41_0= RULE_ID ) ) )
                    // InternalRunstar.g:780:3: otherlv_16= 'with' otherlv_17= '{' ( ( ( ( ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) ) )* ) ) ) otherlv_39= '}' otherlv_40= 'be' ( (lv_name_41_0= RULE_ID ) )
                    {
                    otherlv_16=(Token)match(input,30,FollowSets000.FOLLOW_24); 

                        	newLeafNode(otherlv_16, grammarAccess.getChartVarAccess().getWithKeyword_0_15_0());
                        
                    otherlv_17=(Token)match(input,31,FollowSets000.FOLLOW_25); 

                        	newLeafNode(otherlv_17, grammarAccess.getChartVarAccess().getLeftCurlyBracketKeyword_0_15_1());
                        
                    // InternalRunstar.g:788:1: ( ( ( ( ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) ) )* ) ) )
                    // InternalRunstar.g:790:1: ( ( ( ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) ) )* ) )
                    {
                    // InternalRunstar.g:790:1: ( ( ( ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) ) )* ) )
                    // InternalRunstar.g:791:2: ( ( ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) ) )* )
                    {
                     
                    	  getUnorderedGroupHelper().enter(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2());
                    	
                    // InternalRunstar.g:794:2: ( ( ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) ) )* )
                    // InternalRunstar.g:795:3: ( ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) ) )*
                    {
                    // InternalRunstar.g:795:3: ( ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) ) )*
                    loop10:
                    do {
                        int alt10=6;
                        int LA10_0 = input.LA(1);

                        if ( LA10_0 == 32 && getUnorderedGroupHelper().canSelect(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 0) ) {
                            alt10=1;
                        }
                        else if ( LA10_0 == 33 && getUnorderedGroupHelper().canSelect(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 1) ) {
                            alt10=2;
                        }
                        else if ( LA10_0 == 34 && getUnorderedGroupHelper().canSelect(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 2) ) {
                            alt10=3;
                        }
                        else if ( LA10_0 == 35 && getUnorderedGroupHelper().canSelect(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 3) ) {
                            alt10=4;
                        }
                        else if ( LA10_0 == 36 && getUnorderedGroupHelper().canSelect(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 4) ) {
                            alt10=5;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // InternalRunstar.g:797:4: ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) )
                    	    {
                    	    // InternalRunstar.g:797:4: ({...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) ) )
                    	    // InternalRunstar.g:798:5: {...}? => ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 0) ) {
                    	        throw new FailedPredicateException(input, "ruleChartVar", "getUnorderedGroupHelper().canSelect(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 0)");
                    	    }
                    	    // InternalRunstar.g:798:110: ( ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) ) )
                    	    // InternalRunstar.g:799:6: ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) )
                    	    {
                    	     
                    	    	 				  getUnorderedGroupHelper().select(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 0);
                    	    	 				
                    	    // InternalRunstar.g:802:6: ({...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' ) )
                    	    // InternalRunstar.g:802:7: {...}? => ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' )
                    	    {
                    	    if ( !((true)) ) {
                    	        throw new FailedPredicateException(input, "ruleChartVar", "true");
                    	    }
                    	    // InternalRunstar.g:802:16: ( (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';' )
                    	    // InternalRunstar.g:802:17: (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) ) otherlv_22= ';'
                    	    {
                    	    // InternalRunstar.g:802:17: (otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) ) )
                    	    // InternalRunstar.g:802:19: otherlv_19= 'title' otherlv_20= '=' ( (lv_title_21_0= RULE_STRING ) )
                    	    {
                    	    otherlv_19=(Token)match(input,32,FollowSets000.FOLLOW_18); 

                    	        	newLeafNode(otherlv_19, grammarAccess.getChartVarAccess().getTitleKeyword_0_15_2_0_0_0());
                    	        
                    	    otherlv_20=(Token)match(input,26,FollowSets000.FOLLOW_11); 

                    	        	newLeafNode(otherlv_20, grammarAccess.getChartVarAccess().getEqualsSignKeyword_0_15_2_0_0_1());
                    	        
                    	    // InternalRunstar.g:810:1: ( (lv_title_21_0= RULE_STRING ) )
                    	    // InternalRunstar.g:811:1: (lv_title_21_0= RULE_STRING )
                    	    {
                    	    // InternalRunstar.g:811:1: (lv_title_21_0= RULE_STRING )
                    	    // InternalRunstar.g:812:3: lv_title_21_0= RULE_STRING
                    	    {
                    	    lv_title_21_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_9); 

                    	    			newLeafNode(lv_title_21_0, grammarAccess.getChartVarAccess().getTitleSTRINGTerminalRuleCall_0_15_2_0_0_2_0()); 
                    	    		

                    	    	        if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getChartVarRule());
                    	    	        }
                    	           		setWithLastConsumed(
                    	           			current, 
                    	           			"title",
                    	            		lv_title_21_0, 
                    	            		"org.eclipse.xtext.common.Terminals.STRING");
                    	    	    

                    	    }


                    	    }


                    	    }

                    	    otherlv_22=(Token)match(input,16,FollowSets000.FOLLOW_25); 

                    	        	newLeafNode(otherlv_22, grammarAccess.getChartVarAccess().getSemicolonKeyword_0_15_2_0_1());
                    	        

                    	    }


                    	    }

                    	     
                    	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2());
                    	    	 				

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalRunstar.g:839:4: ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) )
                    	    {
                    	    // InternalRunstar.g:839:4: ({...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) ) )
                    	    // InternalRunstar.g:840:5: {...}? => ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 1) ) {
                    	        throw new FailedPredicateException(input, "ruleChartVar", "getUnorderedGroupHelper().canSelect(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 1)");
                    	    }
                    	    // InternalRunstar.g:840:110: ( ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) ) )
                    	    // InternalRunstar.g:841:6: ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) )
                    	    {
                    	     
                    	    	 				  getUnorderedGroupHelper().select(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 1);
                    	    	 				
                    	    // InternalRunstar.g:844:6: ({...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' ) )
                    	    // InternalRunstar.g:844:7: {...}? => ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' )
                    	    {
                    	    if ( !((true)) ) {
                    	        throw new FailedPredicateException(input, "ruleChartVar", "true");
                    	    }
                    	    // InternalRunstar.g:844:16: ( (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';' )
                    	    // InternalRunstar.g:844:17: (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) ) otherlv_26= ';'
                    	    {
                    	    // InternalRunstar.g:844:17: (otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) ) )
                    	    // InternalRunstar.g:844:19: otherlv_23= 'xLabel' otherlv_24= '=' ( (lv_xlabel_25_0= RULE_STRING ) )
                    	    {
                    	    otherlv_23=(Token)match(input,33,FollowSets000.FOLLOW_18); 

                    	        	newLeafNode(otherlv_23, grammarAccess.getChartVarAccess().getXLabelKeyword_0_15_2_1_0_0());
                    	        
                    	    otherlv_24=(Token)match(input,26,FollowSets000.FOLLOW_11); 

                    	        	newLeafNode(otherlv_24, grammarAccess.getChartVarAccess().getEqualsSignKeyword_0_15_2_1_0_1());
                    	        
                    	    // InternalRunstar.g:852:1: ( (lv_xlabel_25_0= RULE_STRING ) )
                    	    // InternalRunstar.g:853:1: (lv_xlabel_25_0= RULE_STRING )
                    	    {
                    	    // InternalRunstar.g:853:1: (lv_xlabel_25_0= RULE_STRING )
                    	    // InternalRunstar.g:854:3: lv_xlabel_25_0= RULE_STRING
                    	    {
                    	    lv_xlabel_25_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_9); 

                    	    			newLeafNode(lv_xlabel_25_0, grammarAccess.getChartVarAccess().getXlabelSTRINGTerminalRuleCall_0_15_2_1_0_2_0()); 
                    	    		

                    	    	        if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getChartVarRule());
                    	    	        }
                    	           		setWithLastConsumed(
                    	           			current, 
                    	           			"xlabel",
                    	            		lv_xlabel_25_0, 
                    	            		"org.eclipse.xtext.common.Terminals.STRING");
                    	    	    

                    	    }


                    	    }


                    	    }

                    	    otherlv_26=(Token)match(input,16,FollowSets000.FOLLOW_25); 

                    	        	newLeafNode(otherlv_26, grammarAccess.getChartVarAccess().getSemicolonKeyword_0_15_2_1_1());
                    	        

                    	    }


                    	    }

                    	     
                    	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2());
                    	    	 				

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 3 :
                    	    // InternalRunstar.g:881:4: ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) )
                    	    {
                    	    // InternalRunstar.g:881:4: ({...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) ) )
                    	    // InternalRunstar.g:882:5: {...}? => ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 2) ) {
                    	        throw new FailedPredicateException(input, "ruleChartVar", "getUnorderedGroupHelper().canSelect(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 2)");
                    	    }
                    	    // InternalRunstar.g:882:110: ( ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) ) )
                    	    // InternalRunstar.g:883:6: ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) )
                    	    {
                    	     
                    	    	 				  getUnorderedGroupHelper().select(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 2);
                    	    	 				
                    	    // InternalRunstar.g:886:6: ({...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' ) )
                    	    // InternalRunstar.g:886:7: {...}? => ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' )
                    	    {
                    	    if ( !((true)) ) {
                    	        throw new FailedPredicateException(input, "ruleChartVar", "true");
                    	    }
                    	    // InternalRunstar.g:886:16: ( (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';' )
                    	    // InternalRunstar.g:886:17: (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) ) otherlv_30= ';'
                    	    {
                    	    // InternalRunstar.g:886:17: (otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) ) )
                    	    // InternalRunstar.g:886:19: otherlv_27= 'yLabel' otherlv_28= '=' ( (lv_ylabel_29_0= RULE_STRING ) )
                    	    {
                    	    otherlv_27=(Token)match(input,34,FollowSets000.FOLLOW_18); 

                    	        	newLeafNode(otherlv_27, grammarAccess.getChartVarAccess().getYLabelKeyword_0_15_2_2_0_0());
                    	        
                    	    otherlv_28=(Token)match(input,26,FollowSets000.FOLLOW_11); 

                    	        	newLeafNode(otherlv_28, grammarAccess.getChartVarAccess().getEqualsSignKeyword_0_15_2_2_0_1());
                    	        
                    	    // InternalRunstar.g:894:1: ( (lv_ylabel_29_0= RULE_STRING ) )
                    	    // InternalRunstar.g:895:1: (lv_ylabel_29_0= RULE_STRING )
                    	    {
                    	    // InternalRunstar.g:895:1: (lv_ylabel_29_0= RULE_STRING )
                    	    // InternalRunstar.g:896:3: lv_ylabel_29_0= RULE_STRING
                    	    {
                    	    lv_ylabel_29_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_9); 

                    	    			newLeafNode(lv_ylabel_29_0, grammarAccess.getChartVarAccess().getYlabelSTRINGTerminalRuleCall_0_15_2_2_0_2_0()); 
                    	    		

                    	    	        if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getChartVarRule());
                    	    	        }
                    	           		setWithLastConsumed(
                    	           			current, 
                    	           			"ylabel",
                    	            		lv_ylabel_29_0, 
                    	            		"org.eclipse.xtext.common.Terminals.STRING");
                    	    	    

                    	    }


                    	    }


                    	    }

                    	    otherlv_30=(Token)match(input,16,FollowSets000.FOLLOW_25); 

                    	        	newLeafNode(otherlv_30, grammarAccess.getChartVarAccess().getSemicolonKeyword_0_15_2_2_1());
                    	        

                    	    }


                    	    }

                    	     
                    	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2());
                    	    	 				

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 4 :
                    	    // InternalRunstar.g:923:4: ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) )
                    	    {
                    	    // InternalRunstar.g:923:4: ({...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) ) )
                    	    // InternalRunstar.g:924:5: {...}? => ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 3) ) {
                    	        throw new FailedPredicateException(input, "ruleChartVar", "getUnorderedGroupHelper().canSelect(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 3)");
                    	    }
                    	    // InternalRunstar.g:924:110: ( ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) ) )
                    	    // InternalRunstar.g:925:6: ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) )
                    	    {
                    	     
                    	    	 				  getUnorderedGroupHelper().select(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 3);
                    	    	 				
                    	    // InternalRunstar.g:928:6: ({...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' ) )
                    	    // InternalRunstar.g:928:7: {...}? => ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' )
                    	    {
                    	    if ( !((true)) ) {
                    	        throw new FailedPredicateException(input, "ruleChartVar", "true");
                    	    }
                    	    // InternalRunstar.g:928:16: ( (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';' )
                    	    // InternalRunstar.g:928:17: (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) ) otherlv_34= ';'
                    	    {
                    	    // InternalRunstar.g:928:17: (otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) ) )
                    	    // InternalRunstar.g:928:19: otherlv_31= 'nbValues' otherlv_32= '=' ( (lv_nbValuesDisplayed_33_0= RULE_INT ) )
                    	    {
                    	    otherlv_31=(Token)match(input,35,FollowSets000.FOLLOW_18); 

                    	        	newLeafNode(otherlv_31, grammarAccess.getChartVarAccess().getNbValuesKeyword_0_15_2_3_0_0());
                    	        
                    	    otherlv_32=(Token)match(input,26,FollowSets000.FOLLOW_26); 

                    	        	newLeafNode(otherlv_32, grammarAccess.getChartVarAccess().getEqualsSignKeyword_0_15_2_3_0_1());
                    	        
                    	    // InternalRunstar.g:936:1: ( (lv_nbValuesDisplayed_33_0= RULE_INT ) )
                    	    // InternalRunstar.g:937:1: (lv_nbValuesDisplayed_33_0= RULE_INT )
                    	    {
                    	    // InternalRunstar.g:937:1: (lv_nbValuesDisplayed_33_0= RULE_INT )
                    	    // InternalRunstar.g:938:3: lv_nbValuesDisplayed_33_0= RULE_INT
                    	    {
                    	    lv_nbValuesDisplayed_33_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_9); 

                    	    			newLeafNode(lv_nbValuesDisplayed_33_0, grammarAccess.getChartVarAccess().getNbValuesDisplayedINTTerminalRuleCall_0_15_2_3_0_2_0()); 
                    	    		

                    	    	        if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getChartVarRule());
                    	    	        }
                    	           		setWithLastConsumed(
                    	           			current, 
                    	           			"nbValuesDisplayed",
                    	            		lv_nbValuesDisplayed_33_0, 
                    	            		"org.eclipse.xtext.common.Terminals.INT");
                    	    	    

                    	    }


                    	    }


                    	    }

                    	    otherlv_34=(Token)match(input,16,FollowSets000.FOLLOW_25); 

                    	        	newLeafNode(otherlv_34, grammarAccess.getChartVarAccess().getSemicolonKeyword_0_15_2_3_1());
                    	        

                    	    }


                    	    }

                    	     
                    	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2());
                    	    	 				

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 5 :
                    	    // InternalRunstar.g:965:4: ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) )
                    	    {
                    	    // InternalRunstar.g:965:4: ({...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) ) )
                    	    // InternalRunstar.g:966:5: {...}? => ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 4) ) {
                    	        throw new FailedPredicateException(input, "ruleChartVar", "getUnorderedGroupHelper().canSelect(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 4)");
                    	    }
                    	    // InternalRunstar.g:966:110: ( ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) ) )
                    	    // InternalRunstar.g:967:6: ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) )
                    	    {
                    	     
                    	    	 				  getUnorderedGroupHelper().select(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2(), 4);
                    	    	 				
                    	    // InternalRunstar.g:970:6: ({...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' ) )
                    	    // InternalRunstar.g:970:7: {...}? => ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' )
                    	    {
                    	    if ( !((true)) ) {
                    	        throw new FailedPredicateException(input, "ruleChartVar", "true");
                    	    }
                    	    // InternalRunstar.g:970:16: ( (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';' )
                    	    // InternalRunstar.g:970:17: (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) ) otherlv_38= ';'
                    	    {
                    	    // InternalRunstar.g:970:17: (otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) ) )
                    	    // InternalRunstar.g:970:19: otherlv_35= 'lineStyle' otherlv_36= '=' ( (lv_lineStyle_37_0= ruleLineStyle ) )
                    	    {
                    	    otherlv_35=(Token)match(input,36,FollowSets000.FOLLOW_18); 

                    	        	newLeafNode(otherlv_35, grammarAccess.getChartVarAccess().getLineStyleKeyword_0_15_2_4_0_0());
                    	        
                    	    otherlv_36=(Token)match(input,26,FollowSets000.FOLLOW_27); 

                    	        	newLeafNode(otherlv_36, grammarAccess.getChartVarAccess().getEqualsSignKeyword_0_15_2_4_0_1());
                    	        
                    	    // InternalRunstar.g:978:1: ( (lv_lineStyle_37_0= ruleLineStyle ) )
                    	    // InternalRunstar.g:979:1: (lv_lineStyle_37_0= ruleLineStyle )
                    	    {
                    	    // InternalRunstar.g:979:1: (lv_lineStyle_37_0= ruleLineStyle )
                    	    // InternalRunstar.g:980:3: lv_lineStyle_37_0= ruleLineStyle
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getChartVarAccess().getLineStyleLineStyleEnumRuleCall_0_15_2_4_0_2_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_9);
                    	    lv_lineStyle_37_0=ruleLineStyle();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getChartVarRule());
                    	    	        }
                    	           		set(
                    	           			current, 
                    	           			"lineStyle",
                    	            		lv_lineStyle_37_0, 
                    	            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.LineStyle");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }

                    	    otherlv_38=(Token)match(input,16,FollowSets000.FOLLOW_25); 

                    	        	newLeafNode(otherlv_38, grammarAccess.getChartVarAccess().getSemicolonKeyword_0_15_2_4_1());
                    	        

                    	    }


                    	    }

                    	     
                    	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2());
                    	    	 				

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);


                    }


                    }

                     
                    	  getUnorderedGroupHelper().leave(grammarAccess.getChartVarAccess().getUnorderedGroup_0_15_2());
                    	

                    }

                    otherlv_39=(Token)match(input,37,FollowSets000.FOLLOW_14); 

                        	newLeafNode(otherlv_39, grammarAccess.getChartVarAccess().getRightCurlyBracketKeyword_0_15_3());
                        
                    otherlv_40=(Token)match(input,20,FollowSets000.FOLLOW_15); 

                        	newLeafNode(otherlv_40, grammarAccess.getChartVarAccess().getBeKeyword_0_15_4());
                        
                    // InternalRunstar.g:1022:1: ( (lv_name_41_0= RULE_ID ) )
                    // InternalRunstar.g:1023:1: (lv_name_41_0= RULE_ID )
                    {
                    // InternalRunstar.g:1023:1: (lv_name_41_0= RULE_ID )
                    // InternalRunstar.g:1024:3: lv_name_41_0= RULE_ID
                    {
                    lv_name_41_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

                    			newLeafNode(lv_name_41_0, grammarAccess.getChartVarAccess().getNameIDTerminalRuleCall_0_15_5_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getChartVarRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_41_0, 
                            		"org.eclipse.xtext.common.Terminals.ID");
                    	    

                    }


                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalRunstar.g:1041:6: (otherlv_42= 'be' ( (lv_name_43_0= RULE_ID ) ) )
                    {
                    // InternalRunstar.g:1041:6: (otherlv_42= 'be' ( (lv_name_43_0= RULE_ID ) ) )
                    // InternalRunstar.g:1041:8: otherlv_42= 'be' ( (lv_name_43_0= RULE_ID ) )
                    {
                    otherlv_42=(Token)match(input,20,FollowSets000.FOLLOW_15); 

                        	newLeafNode(otherlv_42, grammarAccess.getChartVarAccess().getBeKeyword_1_0());
                        
                    // InternalRunstar.g:1045:1: ( (lv_name_43_0= RULE_ID ) )
                    // InternalRunstar.g:1046:1: (lv_name_43_0= RULE_ID )
                    {
                    // InternalRunstar.g:1046:1: (lv_name_43_0= RULE_ID )
                    // InternalRunstar.g:1047:3: lv_name_43_0= RULE_ID
                    {
                    lv_name_43_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

                    			newLeafNode(lv_name_43_0, grammarAccess.getChartVarAccess().getNameIDTerminalRuleCall_1_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getChartVarRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_43_0, 
                            		"org.eclipse.xtext.common.Terminals.ID");
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleChartVar"


    // $ANTLR start "entryRuleDecoratedTextVar"
    // InternalRunstar.g:1071:1: entryRuleDecoratedTextVar returns [EObject current=null] : iv_ruleDecoratedTextVar= ruleDecoratedTextVar EOF ;
    public final EObject entryRuleDecoratedTextVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDecoratedTextVar = null;


        try {
            // InternalRunstar.g:1072:2: (iv_ruleDecoratedTextVar= ruleDecoratedTextVar EOF )
            // InternalRunstar.g:1073:2: iv_ruleDecoratedTextVar= ruleDecoratedTextVar EOF
            {
             newCompositeNode(grammarAccess.getDecoratedTextVarRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleDecoratedTextVar=ruleDecoratedTextVar();

            state._fsp--;

             current =iv_ruleDecoratedTextVar; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDecoratedTextVar"


    // $ANTLR start "ruleDecoratedTextVar"
    // InternalRunstar.g:1080:1: ruleDecoratedTextVar returns [EObject current=null] : (otherlv_0= 'DecoratedText' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' otherlv_4= 'with' otherlv_5= '{' ( ( ( ( ({...}? => ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) ) ) )* ) ) ) otherlv_39= '}' otherlv_40= 'be' ( (lv_name_41_0= RULE_ID ) ) ) ;
    public final EObject ruleDecoratedTextVar() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token lv_prefix_9_0=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token lv_suffix_13_0=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token lv_font_17_0=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token lv_fontSize_21_0=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_27=null;
        Token otherlv_28=null;
        Token otherlv_30=null;
        Token otherlv_31=null;
        Token otherlv_32=null;
        Token lv_errorText_33_0=null;
        Token otherlv_34=null;
        Token otherlv_35=null;
        Token otherlv_36=null;
        Token otherlv_38=null;
        Token otherlv_39=null;
        Token otherlv_40=null;
        Token lv_name_41_0=null;
        Enumerator lv_fontStyle_25_0 = null;

        EObject lv_color_29_0 = null;

        EObject lv_errorColor_37_0 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:1083:28: ( (otherlv_0= 'DecoratedText' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' otherlv_4= 'with' otherlv_5= '{' ( ( ( ( ({...}? => ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) ) ) )* ) ) ) otherlv_39= '}' otherlv_40= 'be' ( (lv_name_41_0= RULE_ID ) ) ) )
            // InternalRunstar.g:1084:1: (otherlv_0= 'DecoratedText' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' otherlv_4= 'with' otherlv_5= '{' ( ( ( ( ({...}? => ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) ) ) )* ) ) ) otherlv_39= '}' otherlv_40= 'be' ( (lv_name_41_0= RULE_ID ) ) )
            {
            // InternalRunstar.g:1084:1: (otherlv_0= 'DecoratedText' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' otherlv_4= 'with' otherlv_5= '{' ( ( ( ( ({...}? => ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) ) ) )* ) ) ) otherlv_39= '}' otherlv_40= 'be' ( (lv_name_41_0= RULE_ID ) ) )
            // InternalRunstar.g:1084:3: otherlv_0= 'DecoratedText' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' otherlv_4= 'with' otherlv_5= '{' ( ( ( ( ({...}? => ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) ) ) )* ) ) ) otherlv_39= '}' otherlv_40= 'be' ( (lv_name_41_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,38,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_0, grammarAccess.getDecoratedTextVarAccess().getDecoratedTextKeyword_0());
                
            otherlv_1=(Token)match(input,18,FollowSets000.FOLLOW_15); 

                	newLeafNode(otherlv_1, grammarAccess.getDecoratedTextVarAccess().getLeftParenthesisKeyword_1());
                
            // InternalRunstar.g:1092:1: ( (otherlv_2= RULE_ID ) )
            // InternalRunstar.g:1093:1: (otherlv_2= RULE_ID )
            {
            // InternalRunstar.g:1093:1: (otherlv_2= RULE_ID )
            // InternalRunstar.g:1094:3: otherlv_2= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getDecoratedTextVarRule());
            	        }
                    
            otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_13); 

            		newLeafNode(otherlv_2, grammarAccess.getDecoratedTextVarAccess().getDecoratedTextVarCrossReference_2_0()); 
            	

            }


            }

            otherlv_3=(Token)match(input,19,FollowSets000.FOLLOW_23); 

                	newLeafNode(otherlv_3, grammarAccess.getDecoratedTextVarAccess().getRightParenthesisKeyword_3());
                
            otherlv_4=(Token)match(input,30,FollowSets000.FOLLOW_24); 

                	newLeafNode(otherlv_4, grammarAccess.getDecoratedTextVarAccess().getWithKeyword_4());
                
            otherlv_5=(Token)match(input,31,FollowSets000.FOLLOW_28); 

                	newLeafNode(otherlv_5, grammarAccess.getDecoratedTextVarAccess().getLeftCurlyBracketKeyword_5());
                
            // InternalRunstar.g:1117:1: ( ( ( ( ({...}? => ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) ) ) )* ) ) )
            // InternalRunstar.g:1119:1: ( ( ( ({...}? => ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) ) ) )* ) )
            {
            // InternalRunstar.g:1119:1: ( ( ( ({...}? => ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) ) ) )* ) )
            // InternalRunstar.g:1120:2: ( ( ({...}? => ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) ) ) )* )
            {
             
            	  getUnorderedGroupHelper().enter(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6());
            	
            // InternalRunstar.g:1123:2: ( ( ({...}? => ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) ) ) )* )
            // InternalRunstar.g:1124:3: ( ({...}? => ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) ) ) )*
            {
            // InternalRunstar.g:1124:3: ( ({...}? => ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) ) ) )*
            loop12:
            do {
                int alt12=9;
                alt12 = dfa12.predict(input);
                switch (alt12) {
            	case 1 :
            	    // InternalRunstar.g:1126:4: ({...}? => ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) ) )
            	    {
            	    // InternalRunstar.g:1126:4: ({...}? => ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) ) )
            	    // InternalRunstar.g:1127:5: {...}? => ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleDecoratedTextVar", "getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 0)");
            	    }
            	    // InternalRunstar.g:1127:113: ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) )
            	    // InternalRunstar.g:1128:6: ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 0);
            	    	 				
            	    // InternalRunstar.g:1131:6: ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) )
            	    // InternalRunstar.g:1131:7: {...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleDecoratedTextVar", "true");
            	    }
            	    // InternalRunstar.g:1131:16: ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' )
            	    // InternalRunstar.g:1131:17: (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';'
            	    {
            	    // InternalRunstar.g:1131:17: (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) )
            	    // InternalRunstar.g:1131:19: otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) )
            	    {
            	    otherlv_7=(Token)match(input,39,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_7, grammarAccess.getDecoratedTextVarAccess().getPrefixKeyword_6_0_0_0());
            	        
            	    otherlv_8=(Token)match(input,26,FollowSets000.FOLLOW_11); 

            	        	newLeafNode(otherlv_8, grammarAccess.getDecoratedTextVarAccess().getEqualsSignKeyword_6_0_0_1());
            	        
            	    // InternalRunstar.g:1139:1: ( (lv_prefix_9_0= RULE_STRING ) )
            	    // InternalRunstar.g:1140:1: (lv_prefix_9_0= RULE_STRING )
            	    {
            	    // InternalRunstar.g:1140:1: (lv_prefix_9_0= RULE_STRING )
            	    // InternalRunstar.g:1141:3: lv_prefix_9_0= RULE_STRING
            	    {
            	    lv_prefix_9_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_9); 

            	    			newLeafNode(lv_prefix_9_0, grammarAccess.getDecoratedTextVarAccess().getPrefixSTRINGTerminalRuleCall_6_0_0_2_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getDecoratedTextVarRule());
            	    	        }
            	           		setWithLastConsumed(
            	           			current, 
            	           			"prefix",
            	            		lv_prefix_9_0, 
            	            		"org.eclipse.xtext.common.Terminals.STRING");
            	    	    

            	    }


            	    }


            	    }

            	    otherlv_10=(Token)match(input,16,FollowSets000.FOLLOW_28); 

            	        	newLeafNode(otherlv_10, grammarAccess.getDecoratedTextVarAccess().getSemicolonKeyword_6_0_1());
            	        

            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalRunstar.g:1168:4: ({...}? => ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) ) )
            	    {
            	    // InternalRunstar.g:1168:4: ({...}? => ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) ) )
            	    // InternalRunstar.g:1169:5: {...}? => ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleDecoratedTextVar", "getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 1)");
            	    }
            	    // InternalRunstar.g:1169:113: ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) )
            	    // InternalRunstar.g:1170:6: ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 1);
            	    	 				
            	    // InternalRunstar.g:1173:6: ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) )
            	    // InternalRunstar.g:1173:7: {...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleDecoratedTextVar", "true");
            	    }
            	    // InternalRunstar.g:1173:16: ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' )
            	    // InternalRunstar.g:1173:17: (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';'
            	    {
            	    // InternalRunstar.g:1173:17: (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) )
            	    // InternalRunstar.g:1173:19: otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) )
            	    {
            	    otherlv_11=(Token)match(input,40,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_11, grammarAccess.getDecoratedTextVarAccess().getSuffixKeyword_6_1_0_0());
            	        
            	    otherlv_12=(Token)match(input,26,FollowSets000.FOLLOW_11); 

            	        	newLeafNode(otherlv_12, grammarAccess.getDecoratedTextVarAccess().getEqualsSignKeyword_6_1_0_1());
            	        
            	    // InternalRunstar.g:1181:1: ( (lv_suffix_13_0= RULE_STRING ) )
            	    // InternalRunstar.g:1182:1: (lv_suffix_13_0= RULE_STRING )
            	    {
            	    // InternalRunstar.g:1182:1: (lv_suffix_13_0= RULE_STRING )
            	    // InternalRunstar.g:1183:3: lv_suffix_13_0= RULE_STRING
            	    {
            	    lv_suffix_13_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_9); 

            	    			newLeafNode(lv_suffix_13_0, grammarAccess.getDecoratedTextVarAccess().getSuffixSTRINGTerminalRuleCall_6_1_0_2_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getDecoratedTextVarRule());
            	    	        }
            	           		setWithLastConsumed(
            	           			current, 
            	           			"suffix",
            	            		lv_suffix_13_0, 
            	            		"org.eclipse.xtext.common.Terminals.STRING");
            	    	    

            	    }


            	    }


            	    }

            	    otherlv_14=(Token)match(input,16,FollowSets000.FOLLOW_28); 

            	        	newLeafNode(otherlv_14, grammarAccess.getDecoratedTextVarAccess().getSemicolonKeyword_6_1_1());
            	        

            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalRunstar.g:1210:4: ({...}? => ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) ) )
            	    {
            	    // InternalRunstar.g:1210:4: ({...}? => ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) ) )
            	    // InternalRunstar.g:1211:5: {...}? => ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleDecoratedTextVar", "getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 2)");
            	    }
            	    // InternalRunstar.g:1211:113: ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) )
            	    // InternalRunstar.g:1212:6: ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 2);
            	    	 				
            	    // InternalRunstar.g:1215:6: ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) )
            	    // InternalRunstar.g:1215:7: {...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleDecoratedTextVar", "true");
            	    }
            	    // InternalRunstar.g:1215:16: ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' )
            	    // InternalRunstar.g:1215:17: (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';'
            	    {
            	    // InternalRunstar.g:1215:17: (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) )
            	    // InternalRunstar.g:1215:19: otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) )
            	    {
            	    otherlv_15=(Token)match(input,41,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_15, grammarAccess.getDecoratedTextVarAccess().getFontKeyword_6_2_0_0());
            	        
            	    otherlv_16=(Token)match(input,26,FollowSets000.FOLLOW_11); 

            	        	newLeafNode(otherlv_16, grammarAccess.getDecoratedTextVarAccess().getEqualsSignKeyword_6_2_0_1());
            	        
            	    // InternalRunstar.g:1223:1: ( (lv_font_17_0= RULE_STRING ) )
            	    // InternalRunstar.g:1224:1: (lv_font_17_0= RULE_STRING )
            	    {
            	    // InternalRunstar.g:1224:1: (lv_font_17_0= RULE_STRING )
            	    // InternalRunstar.g:1225:3: lv_font_17_0= RULE_STRING
            	    {
            	    lv_font_17_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_9); 

            	    			newLeafNode(lv_font_17_0, grammarAccess.getDecoratedTextVarAccess().getFontSTRINGTerminalRuleCall_6_2_0_2_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getDecoratedTextVarRule());
            	    	        }
            	           		setWithLastConsumed(
            	           			current, 
            	           			"font",
            	            		lv_font_17_0, 
            	            		"org.eclipse.xtext.common.Terminals.STRING");
            	    	    

            	    }


            	    }


            	    }

            	    otherlv_18=(Token)match(input,16,FollowSets000.FOLLOW_28); 

            	        	newLeafNode(otherlv_18, grammarAccess.getDecoratedTextVarAccess().getSemicolonKeyword_6_2_1());
            	        

            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalRunstar.g:1252:4: ({...}? => ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) ) )
            	    {
            	    // InternalRunstar.g:1252:4: ({...}? => ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) ) )
            	    // InternalRunstar.g:1253:5: {...}? => ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 3) ) {
            	        throw new FailedPredicateException(input, "ruleDecoratedTextVar", "getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 3)");
            	    }
            	    // InternalRunstar.g:1253:113: ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) )
            	    // InternalRunstar.g:1254:6: ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 3);
            	    	 				
            	    // InternalRunstar.g:1257:6: ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) )
            	    // InternalRunstar.g:1257:7: {...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleDecoratedTextVar", "true");
            	    }
            	    // InternalRunstar.g:1257:16: ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' )
            	    // InternalRunstar.g:1257:17: (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';'
            	    {
            	    // InternalRunstar.g:1257:17: (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) )
            	    // InternalRunstar.g:1257:19: otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) )
            	    {
            	    otherlv_19=(Token)match(input,42,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_19, grammarAccess.getDecoratedTextVarAccess().getFontSizeKeyword_6_3_0_0());
            	        
            	    otherlv_20=(Token)match(input,26,FollowSets000.FOLLOW_26); 

            	        	newLeafNode(otherlv_20, grammarAccess.getDecoratedTextVarAccess().getEqualsSignKeyword_6_3_0_1());
            	        
            	    // InternalRunstar.g:1265:1: ( (lv_fontSize_21_0= RULE_INT ) )
            	    // InternalRunstar.g:1266:1: (lv_fontSize_21_0= RULE_INT )
            	    {
            	    // InternalRunstar.g:1266:1: (lv_fontSize_21_0= RULE_INT )
            	    // InternalRunstar.g:1267:3: lv_fontSize_21_0= RULE_INT
            	    {
            	    lv_fontSize_21_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_9); 

            	    			newLeafNode(lv_fontSize_21_0, grammarAccess.getDecoratedTextVarAccess().getFontSizeINTTerminalRuleCall_6_3_0_2_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getDecoratedTextVarRule());
            	    	        }
            	           		setWithLastConsumed(
            	           			current, 
            	           			"fontSize",
            	            		lv_fontSize_21_0, 
            	            		"org.eclipse.xtext.common.Terminals.INT");
            	    	    

            	    }


            	    }


            	    }

            	    otherlv_22=(Token)match(input,16,FollowSets000.FOLLOW_28); 

            	        	newLeafNode(otherlv_22, grammarAccess.getDecoratedTextVarAccess().getSemicolonKeyword_6_3_1());
            	        

            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 5 :
            	    // InternalRunstar.g:1294:4: ({...}? => ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) ) )
            	    {
            	    // InternalRunstar.g:1294:4: ({...}? => ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) ) )
            	    // InternalRunstar.g:1295:5: {...}? => ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 4) ) {
            	        throw new FailedPredicateException(input, "ruleDecoratedTextVar", "getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 4)");
            	    }
            	    // InternalRunstar.g:1295:113: ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) )
            	    // InternalRunstar.g:1296:6: ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 4);
            	    	 				
            	    // InternalRunstar.g:1299:6: ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) )
            	    // InternalRunstar.g:1299:7: {...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleDecoratedTextVar", "true");
            	    }
            	    // InternalRunstar.g:1299:16: ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' )
            	    // InternalRunstar.g:1299:17: (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';'
            	    {
            	    // InternalRunstar.g:1299:17: (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) )
            	    // InternalRunstar.g:1299:19: otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) )
            	    {
            	    otherlv_23=(Token)match(input,43,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_23, grammarAccess.getDecoratedTextVarAccess().getFontStyleKeyword_6_4_0_0());
            	        
            	    otherlv_24=(Token)match(input,26,FollowSets000.FOLLOW_29); 

            	        	newLeafNode(otherlv_24, grammarAccess.getDecoratedTextVarAccess().getEqualsSignKeyword_6_4_0_1());
            	        
            	    // InternalRunstar.g:1307:1: ( (lv_fontStyle_25_0= ruleFontStyle ) )
            	    // InternalRunstar.g:1308:1: (lv_fontStyle_25_0= ruleFontStyle )
            	    {
            	    // InternalRunstar.g:1308:1: (lv_fontStyle_25_0= ruleFontStyle )
            	    // InternalRunstar.g:1309:3: lv_fontStyle_25_0= ruleFontStyle
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getDecoratedTextVarAccess().getFontStyleFontStyleEnumRuleCall_6_4_0_2_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_9);
            	    lv_fontStyle_25_0=ruleFontStyle();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getDecoratedTextVarRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"fontStyle",
            	            		lv_fontStyle_25_0, 
            	            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.FontStyle");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }

            	    otherlv_26=(Token)match(input,16,FollowSets000.FOLLOW_28); 

            	        	newLeafNode(otherlv_26, grammarAccess.getDecoratedTextVarAccess().getSemicolonKeyword_6_4_1());
            	        

            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 6 :
            	    // InternalRunstar.g:1336:4: ({...}? => ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) ) )
            	    {
            	    // InternalRunstar.g:1336:4: ({...}? => ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) ) )
            	    // InternalRunstar.g:1337:5: {...}? => ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 5) ) {
            	        throw new FailedPredicateException(input, "ruleDecoratedTextVar", "getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 5)");
            	    }
            	    // InternalRunstar.g:1337:113: ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) )
            	    // InternalRunstar.g:1338:6: ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 5);
            	    	 				
            	    // InternalRunstar.g:1341:6: ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) )
            	    // InternalRunstar.g:1341:7: {...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleDecoratedTextVar", "true");
            	    }
            	    // InternalRunstar.g:1341:16: ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' )
            	    // InternalRunstar.g:1341:17: (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';'
            	    {
            	    // InternalRunstar.g:1341:17: (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) )
            	    // InternalRunstar.g:1341:19: otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) )
            	    {
            	    otherlv_27=(Token)match(input,44,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_27, grammarAccess.getDecoratedTextVarAccess().getColorKeyword_6_5_0_0());
            	        
            	    otherlv_28=(Token)match(input,26,FollowSets000.FOLLOW_10); 

            	        	newLeafNode(otherlv_28, grammarAccess.getDecoratedTextVarAccess().getEqualsSignKeyword_6_5_0_1());
            	        
            	    // InternalRunstar.g:1349:1: ( (lv_color_29_0= ruleRColor ) )
            	    // InternalRunstar.g:1350:1: (lv_color_29_0= ruleRColor )
            	    {
            	    // InternalRunstar.g:1350:1: (lv_color_29_0= ruleRColor )
            	    // InternalRunstar.g:1351:3: lv_color_29_0= ruleRColor
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getDecoratedTextVarAccess().getColorRColorParserRuleCall_6_5_0_2_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_9);
            	    lv_color_29_0=ruleRColor();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getDecoratedTextVarRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"color",
            	            		lv_color_29_0, 
            	            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.RColor");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }

            	    otherlv_30=(Token)match(input,16,FollowSets000.FOLLOW_28); 

            	        	newLeafNode(otherlv_30, grammarAccess.getDecoratedTextVarAccess().getSemicolonKeyword_6_5_1());
            	        

            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 7 :
            	    // InternalRunstar.g:1378:4: ({...}? => ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) ) )
            	    {
            	    // InternalRunstar.g:1378:4: ({...}? => ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) ) )
            	    // InternalRunstar.g:1379:5: {...}? => ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 6) ) {
            	        throw new FailedPredicateException(input, "ruleDecoratedTextVar", "getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 6)");
            	    }
            	    // InternalRunstar.g:1379:113: ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) )
            	    // InternalRunstar.g:1380:6: ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 6);
            	    	 				
            	    // InternalRunstar.g:1383:6: ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) )
            	    // InternalRunstar.g:1383:7: {...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleDecoratedTextVar", "true");
            	    }
            	    // InternalRunstar.g:1383:16: ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' )
            	    // InternalRunstar.g:1383:17: (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';'
            	    {
            	    // InternalRunstar.g:1383:17: (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) )
            	    // InternalRunstar.g:1383:19: otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) )
            	    {
            	    otherlv_31=(Token)match(input,45,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_31, grammarAccess.getDecoratedTextVarAccess().getErrorTextKeyword_6_6_0_0());
            	        
            	    otherlv_32=(Token)match(input,26,FollowSets000.FOLLOW_11); 

            	        	newLeafNode(otherlv_32, grammarAccess.getDecoratedTextVarAccess().getEqualsSignKeyword_6_6_0_1());
            	        
            	    // InternalRunstar.g:1391:1: ( (lv_errorText_33_0= RULE_STRING ) )
            	    // InternalRunstar.g:1392:1: (lv_errorText_33_0= RULE_STRING )
            	    {
            	    // InternalRunstar.g:1392:1: (lv_errorText_33_0= RULE_STRING )
            	    // InternalRunstar.g:1393:3: lv_errorText_33_0= RULE_STRING
            	    {
            	    lv_errorText_33_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_9); 

            	    			newLeafNode(lv_errorText_33_0, grammarAccess.getDecoratedTextVarAccess().getErrorTextSTRINGTerminalRuleCall_6_6_0_2_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getDecoratedTextVarRule());
            	    	        }
            	           		setWithLastConsumed(
            	           			current, 
            	           			"errorText",
            	            		lv_errorText_33_0, 
            	            		"org.eclipse.xtext.common.Terminals.STRING");
            	    	    

            	    }


            	    }


            	    }

            	    otherlv_34=(Token)match(input,16,FollowSets000.FOLLOW_28); 

            	        	newLeafNode(otherlv_34, grammarAccess.getDecoratedTextVarAccess().getSemicolonKeyword_6_6_1());
            	        

            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 8 :
            	    // InternalRunstar.g:1420:4: ({...}? => ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) ) )
            	    {
            	    // InternalRunstar.g:1420:4: ({...}? => ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) ) )
            	    // InternalRunstar.g:1421:5: {...}? => ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 7) ) {
            	        throw new FailedPredicateException(input, "ruleDecoratedTextVar", "getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 7)");
            	    }
            	    // InternalRunstar.g:1421:113: ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) )
            	    // InternalRunstar.g:1422:6: ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 7);
            	    	 				
            	    // InternalRunstar.g:1425:6: ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) )
            	    // InternalRunstar.g:1425:7: {...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleDecoratedTextVar", "true");
            	    }
            	    // InternalRunstar.g:1425:16: ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' )
            	    // InternalRunstar.g:1425:17: (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';'
            	    {
            	    // InternalRunstar.g:1425:17: (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) )
            	    // InternalRunstar.g:1425:19: otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) )
            	    {
            	    otherlv_35=(Token)match(input,46,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_35, grammarAccess.getDecoratedTextVarAccess().getErrorColorKeyword_6_7_0_0());
            	        
            	    otherlv_36=(Token)match(input,26,FollowSets000.FOLLOW_10); 

            	        	newLeafNode(otherlv_36, grammarAccess.getDecoratedTextVarAccess().getEqualsSignKeyword_6_7_0_1());
            	        
            	    // InternalRunstar.g:1433:1: ( (lv_errorColor_37_0= ruleRColor ) )
            	    // InternalRunstar.g:1434:1: (lv_errorColor_37_0= ruleRColor )
            	    {
            	    // InternalRunstar.g:1434:1: (lv_errorColor_37_0= ruleRColor )
            	    // InternalRunstar.g:1435:3: lv_errorColor_37_0= ruleRColor
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getDecoratedTextVarAccess().getErrorColorRColorParserRuleCall_6_7_0_2_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_9);
            	    lv_errorColor_37_0=ruleRColor();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getDecoratedTextVarRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"errorColor",
            	            		lv_errorColor_37_0, 
            	            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.RColor");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }

            	    otherlv_38=(Token)match(input,16,FollowSets000.FOLLOW_28); 

            	        	newLeafNode(otherlv_38, grammarAccess.getDecoratedTextVarAccess().getSemicolonKeyword_6_7_1());
            	        

            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6());
            	    	 				

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }


            }

             
            	  getUnorderedGroupHelper().leave(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6());
            	

            }

            otherlv_39=(Token)match(input,37,FollowSets000.FOLLOW_14); 

                	newLeafNode(otherlv_39, grammarAccess.getDecoratedTextVarAccess().getRightCurlyBracketKeyword_7());
                
            otherlv_40=(Token)match(input,20,FollowSets000.FOLLOW_15); 

                	newLeafNode(otherlv_40, grammarAccess.getDecoratedTextVarAccess().getBeKeyword_8());
                
            // InternalRunstar.g:1477:1: ( (lv_name_41_0= RULE_ID ) )
            // InternalRunstar.g:1478:1: (lv_name_41_0= RULE_ID )
            {
            // InternalRunstar.g:1478:1: (lv_name_41_0= RULE_ID )
            // InternalRunstar.g:1479:3: lv_name_41_0= RULE_ID
            {
            lv_name_41_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

            			newLeafNode(lv_name_41_0, grammarAccess.getDecoratedTextVarAccess().getNameIDTerminalRuleCall_9_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDecoratedTextVarRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_41_0, 
                    		"org.eclipse.xtext.common.Terminals.ID");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDecoratedTextVar"


    // $ANTLR start "entryRuleRStrategy"
    // InternalRunstar.g:1503:1: entryRuleRStrategy returns [EObject current=null] : iv_ruleRStrategy= ruleRStrategy EOF ;
    public final EObject entryRuleRStrategy() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRStrategy = null;


        try {
            // InternalRunstar.g:1504:2: (iv_ruleRStrategy= ruleRStrategy EOF )
            // InternalRunstar.g:1505:2: iv_ruleRStrategy= ruleRStrategy EOF
            {
             newCompositeNode(grammarAccess.getRStrategyRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRStrategy=ruleRStrategy();

            state._fsp--;

             current =iv_ruleRStrategy; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRStrategy"


    // $ANTLR start "ruleRStrategy"
    // InternalRunstar.g:1512:1: ruleRStrategy returns [EObject current=null] : (otherlv_0= 'for' ( ( ruleQualifiedName ) ) ( (lv_ownedUpdateStrategy_2_0= ruleRUpdateStrategy ) )? ) ;
    public final EObject ruleRStrategy() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_ownedUpdateStrategy_2_0 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:1515:28: ( (otherlv_0= 'for' ( ( ruleQualifiedName ) ) ( (lv_ownedUpdateStrategy_2_0= ruleRUpdateStrategy ) )? ) )
            // InternalRunstar.g:1516:1: (otherlv_0= 'for' ( ( ruleQualifiedName ) ) ( (lv_ownedUpdateStrategy_2_0= ruleRUpdateStrategy ) )? )
            {
            // InternalRunstar.g:1516:1: (otherlv_0= 'for' ( ( ruleQualifiedName ) ) ( (lv_ownedUpdateStrategy_2_0= ruleRUpdateStrategy ) )? )
            // InternalRunstar.g:1516:3: otherlv_0= 'for' ( ( ruleQualifiedName ) ) ( (lv_ownedUpdateStrategy_2_0= ruleRUpdateStrategy ) )?
            {
            otherlv_0=(Token)match(input,47,FollowSets000.FOLLOW_15); 

                	newLeafNode(otherlv_0, grammarAccess.getRStrategyAccess().getForKeyword_0());
                
            // InternalRunstar.g:1520:1: ( ( ruleQualifiedName ) )
            // InternalRunstar.g:1521:1: ( ruleQualifiedName )
            {
            // InternalRunstar.g:1521:1: ( ruleQualifiedName )
            // InternalRunstar.g:1522:3: ruleQualifiedName
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getRStrategyRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getRStrategyAccess().getOnConceptEObjectCrossReference_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_30);
            ruleQualifiedName();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalRunstar.g:1535:2: ( (lv_ownedUpdateStrategy_2_0= ruleRUpdateStrategy ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==50||(LA13_0>=86 && LA13_0<=87)) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalRunstar.g:1536:1: (lv_ownedUpdateStrategy_2_0= ruleRUpdateStrategy )
                    {
                    // InternalRunstar.g:1536:1: (lv_ownedUpdateStrategy_2_0= ruleRUpdateStrategy )
                    // InternalRunstar.g:1537:3: lv_ownedUpdateStrategy_2_0= ruleRUpdateStrategy
                    {
                     
                    	        newCompositeNode(grammarAccess.getRStrategyAccess().getOwnedUpdateStrategyRUpdateStrategyParserRuleCall_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_ownedUpdateStrategy_2_0=ruleRUpdateStrategy();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getRStrategyRule());
                    	        }
                           		set(
                           			current, 
                           			"ownedUpdateStrategy",
                            		lv_ownedUpdateStrategy_2_0, 
                            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.RUpdateStrategy");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRStrategy"


    // $ANTLR start "entryRuleRUpdateStrategy"
    // InternalRunstar.g:1561:1: entryRuleRUpdateStrategy returns [EObject current=null] : iv_ruleRUpdateStrategy= ruleRUpdateStrategy EOF ;
    public final EObject entryRuleRUpdateStrategy() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRUpdateStrategy = null;


        try {
            // InternalRunstar.g:1562:2: (iv_ruleRUpdateStrategy= ruleRUpdateStrategy EOF )
            // InternalRunstar.g:1563:2: iv_ruleRUpdateStrategy= ruleRUpdateStrategy EOF
            {
             newCompositeNode(grammarAccess.getRUpdateStrategyRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRUpdateStrategy=ruleRUpdateStrategy();

            state._fsp--;

             current =iv_ruleRUpdateStrategy; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRUpdateStrategy"


    // $ANTLR start "ruleRUpdateStrategy"
    // InternalRunstar.g:1570:1: ruleRUpdateStrategy returns [EObject current=null] : ( ( ( (lv_stepExecution_0_0= ruleStepExecution ) ) otherlv_1= 'a' otherlv_2= 'step' ) | ( ( (lv_stepExecution_3_0= ruleStepExecution ) ) ( (otherlv_4= RULE_ID ) ) ) | (otherlv_5= 'between' ( (otherlv_6= RULE_ID ) ) otherlv_7= 'and' ( (otherlv_8= RULE_ID ) ) ) ) ;
    public final EObject ruleRUpdateStrategy() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Enumerator lv_stepExecution_0_0 = null;

        Enumerator lv_stepExecution_3_0 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:1573:28: ( ( ( ( (lv_stepExecution_0_0= ruleStepExecution ) ) otherlv_1= 'a' otherlv_2= 'step' ) | ( ( (lv_stepExecution_3_0= ruleStepExecution ) ) ( (otherlv_4= RULE_ID ) ) ) | (otherlv_5= 'between' ( (otherlv_6= RULE_ID ) ) otherlv_7= 'and' ( (otherlv_8= RULE_ID ) ) ) ) )
            // InternalRunstar.g:1574:1: ( ( ( (lv_stepExecution_0_0= ruleStepExecution ) ) otherlv_1= 'a' otherlv_2= 'step' ) | ( ( (lv_stepExecution_3_0= ruleStepExecution ) ) ( (otherlv_4= RULE_ID ) ) ) | (otherlv_5= 'between' ( (otherlv_6= RULE_ID ) ) otherlv_7= 'and' ( (otherlv_8= RULE_ID ) ) ) )
            {
            // InternalRunstar.g:1574:1: ( ( ( (lv_stepExecution_0_0= ruleStepExecution ) ) otherlv_1= 'a' otherlv_2= 'step' ) | ( ( (lv_stepExecution_3_0= ruleStepExecution ) ) ( (otherlv_4= RULE_ID ) ) ) | (otherlv_5= 'between' ( (otherlv_6= RULE_ID ) ) otherlv_7= 'and' ( (otherlv_8= RULE_ID ) ) ) )
            int alt14=3;
            switch ( input.LA(1) ) {
            case 86:
                {
                int LA14_1 = input.LA(2);

                if ( (LA14_1==48) ) {
                    alt14=1;
                }
                else if ( (LA14_1==RULE_ID) ) {
                    alt14=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 14, 1, input);

                    throw nvae;
                }
                }
                break;
            case 87:
                {
                int LA14_2 = input.LA(2);

                if ( (LA14_2==48) ) {
                    alt14=1;
                }
                else if ( (LA14_2==RULE_ID) ) {
                    alt14=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 14, 2, input);

                    throw nvae;
                }
                }
                break;
            case 50:
                {
                alt14=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // InternalRunstar.g:1574:2: ( ( (lv_stepExecution_0_0= ruleStepExecution ) ) otherlv_1= 'a' otherlv_2= 'step' )
                    {
                    // InternalRunstar.g:1574:2: ( ( (lv_stepExecution_0_0= ruleStepExecution ) ) otherlv_1= 'a' otherlv_2= 'step' )
                    // InternalRunstar.g:1574:3: ( (lv_stepExecution_0_0= ruleStepExecution ) ) otherlv_1= 'a' otherlv_2= 'step'
                    {
                    // InternalRunstar.g:1574:3: ( (lv_stepExecution_0_0= ruleStepExecution ) )
                    // InternalRunstar.g:1575:1: (lv_stepExecution_0_0= ruleStepExecution )
                    {
                    // InternalRunstar.g:1575:1: (lv_stepExecution_0_0= ruleStepExecution )
                    // InternalRunstar.g:1576:3: lv_stepExecution_0_0= ruleStepExecution
                    {
                     
                    	        newCompositeNode(grammarAccess.getRUpdateStrategyAccess().getStepExecutionStepExecutionEnumRuleCall_0_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_31);
                    lv_stepExecution_0_0=ruleStepExecution();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getRUpdateStrategyRule());
                    	        }
                           		set(
                           			current, 
                           			"stepExecution",
                            		lv_stepExecution_0_0, 
                            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.StepExecution");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_1=(Token)match(input,48,FollowSets000.FOLLOW_32); 

                        	newLeafNode(otherlv_1, grammarAccess.getRUpdateStrategyAccess().getAKeyword_0_1());
                        
                    otherlv_2=(Token)match(input,49,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_2, grammarAccess.getRUpdateStrategyAccess().getStepKeyword_0_2());
                        

                    }


                    }
                    break;
                case 2 :
                    // InternalRunstar.g:1601:6: ( ( (lv_stepExecution_3_0= ruleStepExecution ) ) ( (otherlv_4= RULE_ID ) ) )
                    {
                    // InternalRunstar.g:1601:6: ( ( (lv_stepExecution_3_0= ruleStepExecution ) ) ( (otherlv_4= RULE_ID ) ) )
                    // InternalRunstar.g:1601:7: ( (lv_stepExecution_3_0= ruleStepExecution ) ) ( (otherlv_4= RULE_ID ) )
                    {
                    // InternalRunstar.g:1601:7: ( (lv_stepExecution_3_0= ruleStepExecution ) )
                    // InternalRunstar.g:1602:1: (lv_stepExecution_3_0= ruleStepExecution )
                    {
                    // InternalRunstar.g:1602:1: (lv_stepExecution_3_0= ruleStepExecution )
                    // InternalRunstar.g:1603:3: lv_stepExecution_3_0= ruleStepExecution
                    {
                     
                    	        newCompositeNode(grammarAccess.getRUpdateStrategyAccess().getStepExecutionStepExecutionEnumRuleCall_1_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_15);
                    lv_stepExecution_3_0=ruleStepExecution();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getRUpdateStrategyRule());
                    	        }
                           		set(
                           			current, 
                           			"stepExecution",
                            		lv_stepExecution_3_0, 
                            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.StepExecution");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalRunstar.g:1619:2: ( (otherlv_4= RULE_ID ) )
                    // InternalRunstar.g:1620:1: (otherlv_4= RULE_ID )
                    {
                    // InternalRunstar.g:1620:1: (otherlv_4= RULE_ID )
                    // InternalRunstar.g:1621:3: otherlv_4= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getRUpdateStrategyRule());
                    	        }
                            
                    otherlv_4=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

                    		newLeafNode(otherlv_4, grammarAccess.getRUpdateStrategyAccess().getEventsDefPropertyCSCrossReference_1_1_0()); 
                    	

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalRunstar.g:1633:6: (otherlv_5= 'between' ( (otherlv_6= RULE_ID ) ) otherlv_7= 'and' ( (otherlv_8= RULE_ID ) ) )
                    {
                    // InternalRunstar.g:1633:6: (otherlv_5= 'between' ( (otherlv_6= RULE_ID ) ) otherlv_7= 'and' ( (otherlv_8= RULE_ID ) ) )
                    // InternalRunstar.g:1633:8: otherlv_5= 'between' ( (otherlv_6= RULE_ID ) ) otherlv_7= 'and' ( (otherlv_8= RULE_ID ) )
                    {
                    otherlv_5=(Token)match(input,50,FollowSets000.FOLLOW_15); 

                        	newLeafNode(otherlv_5, grammarAccess.getRUpdateStrategyAccess().getBetweenKeyword_2_0());
                        
                    // InternalRunstar.g:1637:1: ( (otherlv_6= RULE_ID ) )
                    // InternalRunstar.g:1638:1: (otherlv_6= RULE_ID )
                    {
                    // InternalRunstar.g:1638:1: (otherlv_6= RULE_ID )
                    // InternalRunstar.g:1639:3: otherlv_6= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getRUpdateStrategyRule());
                    	        }
                            
                    otherlv_6=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_33); 

                    		newLeafNode(otherlv_6, grammarAccess.getRUpdateStrategyAccess().getEventsDefPropertyCSCrossReference_2_1_0()); 
                    	

                    }


                    }

                    otherlv_7=(Token)match(input,51,FollowSets000.FOLLOW_15); 

                        	newLeafNode(otherlv_7, grammarAccess.getRUpdateStrategyAccess().getAndKeyword_2_2());
                        
                    // InternalRunstar.g:1654:1: ( (otherlv_8= RULE_ID ) )
                    // InternalRunstar.g:1655:1: (otherlv_8= RULE_ID )
                    {
                    // InternalRunstar.g:1655:1: (otherlv_8= RULE_ID )
                    // InternalRunstar.g:1656:3: otherlv_8= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getRUpdateStrategyRule());
                    	        }
                            
                    otherlv_8=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

                    		newLeafNode(otherlv_8, grammarAccess.getRUpdateStrategyAccess().getEventsDefPropertyCSCrossReference_2_3_0()); 
                    	

                    }


                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRUpdateStrategy"


    // $ANTLR start "entryRuleImportStatement"
    // InternalRunstar.g:1675:1: entryRuleImportStatement returns [EObject current=null] : iv_ruleImportStatement= ruleImportStatement EOF ;
    public final EObject entryRuleImportStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImportStatement = null;


        try {
            // InternalRunstar.g:1676:2: (iv_ruleImportStatement= ruleImportStatement EOF )
            // InternalRunstar.g:1677:2: iv_ruleImportStatement= ruleImportStatement EOF
            {
             newCompositeNode(grammarAccess.getImportStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleImportStatement=ruleImportStatement();

            state._fsp--;

             current =iv_ruleImportStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImportStatement"


    // $ANTLR start "ruleImportStatement"
    // InternalRunstar.g:1684:1: ruleImportStatement returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= ruleEString ) ) ( ( (lv_alias_2_0= ';' ) ) | (otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';' ) ) ) ;
    public final EObject ruleImportStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_alias_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_importURI_1_0 = null;

        AntlrDatatypeRuleToken lv_alias_4_0 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:1687:28: ( (otherlv_0= 'import' ( (lv_importURI_1_0= ruleEString ) ) ( ( (lv_alias_2_0= ';' ) ) | (otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';' ) ) ) )
            // InternalRunstar.g:1688:1: (otherlv_0= 'import' ( (lv_importURI_1_0= ruleEString ) ) ( ( (lv_alias_2_0= ';' ) ) | (otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';' ) ) )
            {
            // InternalRunstar.g:1688:1: (otherlv_0= 'import' ( (lv_importURI_1_0= ruleEString ) ) ( ( (lv_alias_2_0= ';' ) ) | (otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';' ) ) )
            // InternalRunstar.g:1688:3: otherlv_0= 'import' ( (lv_importURI_1_0= ruleEString ) ) ( ( (lv_alias_2_0= ';' ) ) | (otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';' ) )
            {
            otherlv_0=(Token)match(input,52,FollowSets000.FOLLOW_5); 

                	newLeafNode(otherlv_0, grammarAccess.getImportStatementAccess().getImportKeyword_0());
                
            // InternalRunstar.g:1692:1: ( (lv_importURI_1_0= ruleEString ) )
            // InternalRunstar.g:1693:1: (lv_importURI_1_0= ruleEString )
            {
            // InternalRunstar.g:1693:1: (lv_importURI_1_0= ruleEString )
            // InternalRunstar.g:1694:3: lv_importURI_1_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getImportStatementAccess().getImportURIEStringParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_34);
            lv_importURI_1_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getImportStatementRule());
            	        }
                   		set(
                   			current, 
                   			"importURI",
                    		lv_importURI_1_0, 
                    		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalRunstar.g:1710:2: ( ( (lv_alias_2_0= ';' ) ) | (otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';' ) )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==16) ) {
                alt15=1;
            }
            else if ( (LA15_0==53) ) {
                alt15=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // InternalRunstar.g:1710:3: ( (lv_alias_2_0= ';' ) )
                    {
                    // InternalRunstar.g:1710:3: ( (lv_alias_2_0= ';' ) )
                    // InternalRunstar.g:1711:1: (lv_alias_2_0= ';' )
                    {
                    // InternalRunstar.g:1711:1: (lv_alias_2_0= ';' )
                    // InternalRunstar.g:1712:3: lv_alias_2_0= ';'
                    {
                    lv_alias_2_0=(Token)match(input,16,FollowSets000.FOLLOW_2); 

                            newLeafNode(lv_alias_2_0, grammarAccess.getImportStatementAccess().getAliasSemicolonKeyword_2_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getImportStatementRule());
                    	        }
                           		setWithLastConsumed(current, "alias", lv_alias_2_0, ";");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalRunstar.g:1726:6: (otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';' )
                    {
                    // InternalRunstar.g:1726:6: (otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';' )
                    // InternalRunstar.g:1726:8: otherlv_3= 'as' ( (lv_alias_4_0= ruleEString ) ) otherlv_5= ';'
                    {
                    otherlv_3=(Token)match(input,53,FollowSets000.FOLLOW_5); 

                        	newLeafNode(otherlv_3, grammarAccess.getImportStatementAccess().getAsKeyword_2_1_0());
                        
                    // InternalRunstar.g:1730:1: ( (lv_alias_4_0= ruleEString ) )
                    // InternalRunstar.g:1731:1: (lv_alias_4_0= ruleEString )
                    {
                    // InternalRunstar.g:1731:1: (lv_alias_4_0= ruleEString )
                    // InternalRunstar.g:1732:3: lv_alias_4_0= ruleEString
                    {
                     
                    	        newCompositeNode(grammarAccess.getImportStatementAccess().getAliasEStringParserRuleCall_2_1_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_9);
                    lv_alias_4_0=ruleEString();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getImportStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"alias",
                            		lv_alias_4_0, 
                            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.EString");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_5=(Token)match(input,16,FollowSets000.FOLLOW_2); 

                        	newLeafNode(otherlv_5, grammarAccess.getImportStatementAccess().getSemicolonKeyword_2_1_2());
                        

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImportStatement"


    // $ANTLR start "entryRuleImportJavaStatement"
    // InternalRunstar.g:1760:1: entryRuleImportJavaStatement returns [EObject current=null] : iv_ruleImportJavaStatement= ruleImportJavaStatement EOF ;
    public final EObject entryRuleImportJavaStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImportJavaStatement = null;


        try {
            // InternalRunstar.g:1761:2: (iv_ruleImportJavaStatement= ruleImportJavaStatement EOF )
            // InternalRunstar.g:1762:2: iv_ruleImportJavaStatement= ruleImportJavaStatement EOF
            {
             newCompositeNode(grammarAccess.getImportJavaStatementRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleImportJavaStatement=ruleImportJavaStatement();

            state._fsp--;

             current =iv_ruleImportJavaStatement; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImportJavaStatement"


    // $ANTLR start "ruleImportJavaStatement"
    // InternalRunstar.g:1769:1: ruleImportJavaStatement returns [EObject current=null] : ( ( (otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) ) ) | (otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) ) ) ) otherlv_4= ';' ) ;
    public final EObject ruleImportJavaStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_importedNamespace_1_0 = null;

        AntlrDatatypeRuleToken lv_importedNamespace_3_0 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:1772:28: ( ( ( (otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) ) ) | (otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) ) ) ) otherlv_4= ';' ) )
            // InternalRunstar.g:1773:1: ( ( (otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) ) ) | (otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) ) ) ) otherlv_4= ';' )
            {
            // InternalRunstar.g:1773:1: ( ( (otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) ) ) | (otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) ) ) ) otherlv_4= ';' )
            // InternalRunstar.g:1773:2: ( (otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) ) ) | (otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) ) ) ) otherlv_4= ';'
            {
            // InternalRunstar.g:1773:2: ( (otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) ) ) | (otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) ) ) )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==54) ) {
                alt16=1;
            }
            else if ( (LA16_0==55) ) {
                alt16=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // InternalRunstar.g:1773:3: (otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) ) )
                    {
                    // InternalRunstar.g:1773:3: (otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) ) )
                    // InternalRunstar.g:1773:5: otherlv_0= 'importClass' ( (lv_importedNamespace_1_0= ruleQualifiedName ) )
                    {
                    otherlv_0=(Token)match(input,54,FollowSets000.FOLLOW_15); 

                        	newLeafNode(otherlv_0, grammarAccess.getImportJavaStatementAccess().getImportClassKeyword_0_0_0());
                        
                    // InternalRunstar.g:1777:1: ( (lv_importedNamespace_1_0= ruleQualifiedName ) )
                    // InternalRunstar.g:1778:1: (lv_importedNamespace_1_0= ruleQualifiedName )
                    {
                    // InternalRunstar.g:1778:1: (lv_importedNamespace_1_0= ruleQualifiedName )
                    // InternalRunstar.g:1779:3: lv_importedNamespace_1_0= ruleQualifiedName
                    {
                     
                    	        newCompositeNode(grammarAccess.getImportJavaStatementAccess().getImportedNamespaceQualifiedNameParserRuleCall_0_0_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_9);
                    lv_importedNamespace_1_0=ruleQualifiedName();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getImportJavaStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"importedNamespace",
                            		lv_importedNamespace_1_0, 
                            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.QualifiedName");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalRunstar.g:1796:6: (otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) ) )
                    {
                    // InternalRunstar.g:1796:6: (otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) ) )
                    // InternalRunstar.g:1796:8: otherlv_2= 'importPackage' ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) )
                    {
                    otherlv_2=(Token)match(input,55,FollowSets000.FOLLOW_15); 

                        	newLeafNode(otherlv_2, grammarAccess.getImportJavaStatementAccess().getImportPackageKeyword_0_1_0());
                        
                    // InternalRunstar.g:1800:1: ( (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard ) )
                    // InternalRunstar.g:1801:1: (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard )
                    {
                    // InternalRunstar.g:1801:1: (lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard )
                    // InternalRunstar.g:1802:3: lv_importedNamespace_3_0= ruleQualifiedNameWithWildCard
                    {
                     
                    	        newCompositeNode(grammarAccess.getImportJavaStatementAccess().getImportedNamespaceQualifiedNameWithWildCardParserRuleCall_0_1_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_9);
                    lv_importedNamespace_3_0=ruleQualifiedNameWithWildCard();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getImportJavaStatementRule());
                    	        }
                           		set(
                           			current, 
                           			"importedNamespace",
                            		lv_importedNamespace_3_0, 
                            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.QualifiedNameWithWildCard");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,16,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_4, grammarAccess.getImportJavaStatementAccess().getSemicolonKeyword_1());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImportJavaStatement"


    // $ANTLR start "entryRuleQualifiedNameWithWildCard"
    // InternalRunstar.g:1830:1: entryRuleQualifiedNameWithWildCard returns [String current=null] : iv_ruleQualifiedNameWithWildCard= ruleQualifiedNameWithWildCard EOF ;
    public final String entryRuleQualifiedNameWithWildCard() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameWithWildCard = null;


        try {
            // InternalRunstar.g:1831:2: (iv_ruleQualifiedNameWithWildCard= ruleQualifiedNameWithWildCard EOF )
            // InternalRunstar.g:1832:2: iv_ruleQualifiedNameWithWildCard= ruleQualifiedNameWithWildCard EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameWithWildCardRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleQualifiedNameWithWildCard=ruleQualifiedNameWithWildCard();

            state._fsp--;

             current =iv_ruleQualifiedNameWithWildCard.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildCard"


    // $ANTLR start "ruleQualifiedNameWithWildCard"
    // InternalRunstar.g:1839:1: ruleQualifiedNameWithWildCard returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedName_0= ruleQualifiedName kw= '.' kw= '*' ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameWithWildCard() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedName_0 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:1842:28: ( (this_QualifiedName_0= ruleQualifiedName kw= '.' kw= '*' ) )
            // InternalRunstar.g:1843:1: (this_QualifiedName_0= ruleQualifiedName kw= '.' kw= '*' )
            {
            // InternalRunstar.g:1843:1: (this_QualifiedName_0= ruleQualifiedName kw= '.' kw= '*' )
            // InternalRunstar.g:1844:5: this_QualifiedName_0= ruleQualifiedName kw= '.' kw= '*'
            {
             
                    newCompositeNode(grammarAccess.getQualifiedNameWithWildCardAccess().getQualifiedNameParserRuleCall_0()); 
                
            pushFollow(FollowSets000.FOLLOW_35);
            this_QualifiedName_0=ruleQualifiedName();

            state._fsp--;


            		current.merge(this_QualifiedName_0);
                
             
                    afterParserOrEnumRuleCall();
                
            kw=(Token)match(input,56,FollowSets000.FOLLOW_36); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getQualifiedNameWithWildCardAccess().getFullStopKeyword_1()); 
                
            kw=(Token)match(input,57,FollowSets000.FOLLOW_2); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getQualifiedNameWithWildCardAccess().getAsteriskKeyword_2()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameWithWildCard"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalRunstar.g:1874:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalRunstar.g:1875:2: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalRunstar.g:1876:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalRunstar.g:1883:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // InternalRunstar.g:1886:28: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalRunstar.g:1887:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalRunstar.g:1887:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalRunstar.g:1887:6: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_37); 

            		current.merge(this_ID_0);
                
             
                newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
                
            // InternalRunstar.g:1894:1: (kw= '.' this_ID_2= RULE_ID )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==56) ) {
                    int LA17_2 = input.LA(2);

                    if ( (LA17_2==RULE_ID) ) {
                        alt17=1;
                    }


                }


                switch (alt17) {
            	case 1 :
            	    // InternalRunstar.g:1895:2: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,56,FollowSets000.FOLLOW_15); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            	        
            	    this_ID_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_37); 

            	    		current.merge(this_ID_2);
            	        
            	     
            	        newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            	        

            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleRepresentation"
    // InternalRunstar.g:1915:1: entryRuleRepresentation returns [EObject current=null] : iv_ruleRepresentation= ruleRepresentation EOF ;
    public final EObject entryRuleRepresentation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRepresentation = null;


        try {
            // InternalRunstar.g:1916:2: (iv_ruleRepresentation= ruleRepresentation EOF )
            // InternalRunstar.g:1917:2: iv_ruleRepresentation= ruleRepresentation EOF
            {
             newCompositeNode(grammarAccess.getRepresentationRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRepresentation=ruleRepresentation();

            state._fsp--;

             current =iv_ruleRepresentation; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRepresentation"


    // $ANTLR start "ruleRepresentation"
    // InternalRunstar.g:1924:1: ruleRepresentation returns [EObject current=null] : (otherlv_0= 'create' otherlv_1= ':' (this_Hover_2= ruleHover | this_Label_3= ruleLabel | this_Image_4= ruleImage | this_Stack_5= ruleStack | this_Paint_6= rulePaint | this_Mark_7= ruleMark | this_Comment_8= ruleComment ) otherlv_9= ';' ) ;
    public final EObject ruleRepresentation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_9=null;
        EObject this_Hover_2 = null;

        EObject this_Label_3 = null;

        EObject this_Image_4 = null;

        EObject this_Stack_5 = null;

        EObject this_Paint_6 = null;

        EObject this_Mark_7 = null;

        EObject this_Comment_8 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:1927:28: ( (otherlv_0= 'create' otherlv_1= ':' (this_Hover_2= ruleHover | this_Label_3= ruleLabel | this_Image_4= ruleImage | this_Stack_5= ruleStack | this_Paint_6= rulePaint | this_Mark_7= ruleMark | this_Comment_8= ruleComment ) otherlv_9= ';' ) )
            // InternalRunstar.g:1928:1: (otherlv_0= 'create' otherlv_1= ':' (this_Hover_2= ruleHover | this_Label_3= ruleLabel | this_Image_4= ruleImage | this_Stack_5= ruleStack | this_Paint_6= rulePaint | this_Mark_7= ruleMark | this_Comment_8= ruleComment ) otherlv_9= ';' )
            {
            // InternalRunstar.g:1928:1: (otherlv_0= 'create' otherlv_1= ':' (this_Hover_2= ruleHover | this_Label_3= ruleLabel | this_Image_4= ruleImage | this_Stack_5= ruleStack | this_Paint_6= rulePaint | this_Mark_7= ruleMark | this_Comment_8= ruleComment ) otherlv_9= ';' )
            // InternalRunstar.g:1928:3: otherlv_0= 'create' otherlv_1= ':' (this_Hover_2= ruleHover | this_Label_3= ruleLabel | this_Image_4= ruleImage | this_Stack_5= ruleStack | this_Paint_6= rulePaint | this_Mark_7= ruleMark | this_Comment_8= ruleComment ) otherlv_9= ';'
            {
            otherlv_0=(Token)match(input,58,FollowSets000.FOLLOW_38); 

                	newLeafNode(otherlv_0, grammarAccess.getRepresentationAccess().getCreateKeyword_0());
                
            otherlv_1=(Token)match(input,59,FollowSets000.FOLLOW_39); 

                	newLeafNode(otherlv_1, grammarAccess.getRepresentationAccess().getColonKeyword_1());
                
            // InternalRunstar.g:1936:1: (this_Hover_2= ruleHover | this_Label_3= ruleLabel | this_Image_4= ruleImage | this_Stack_5= ruleStack | this_Paint_6= rulePaint | this_Mark_7= ruleMark | this_Comment_8= ruleComment )
            int alt18=7;
            switch ( input.LA(1) ) {
            case 65:
                {
                alt18=1;
                }
                break;
            case 69:
                {
                alt18=2;
                }
                break;
            case 17:
                {
                alt18=3;
                }
                break;
            case 73:
                {
                alt18=4;
                }
                break;
            case 60:
                {
                alt18=5;
                }
                break;
            case 64:
                {
                alt18=6;
                }
                break;
            case 68:
                {
                alt18=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }

            switch (alt18) {
                case 1 :
                    // InternalRunstar.g:1937:5: this_Hover_2= ruleHover
                    {
                     
                            newCompositeNode(grammarAccess.getRepresentationAccess().getHoverParserRuleCall_2_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_9);
                    this_Hover_2=ruleHover();

                    state._fsp--;

                     
                            current = this_Hover_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalRunstar.g:1947:5: this_Label_3= ruleLabel
                    {
                     
                            newCompositeNode(grammarAccess.getRepresentationAccess().getLabelParserRuleCall_2_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_9);
                    this_Label_3=ruleLabel();

                    state._fsp--;

                     
                            current = this_Label_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalRunstar.g:1957:5: this_Image_4= ruleImage
                    {
                     
                            newCompositeNode(grammarAccess.getRepresentationAccess().getImageParserRuleCall_2_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_9);
                    this_Image_4=ruleImage();

                    state._fsp--;

                     
                            current = this_Image_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // InternalRunstar.g:1967:5: this_Stack_5= ruleStack
                    {
                     
                            newCompositeNode(grammarAccess.getRepresentationAccess().getStackParserRuleCall_2_3()); 
                        
                    pushFollow(FollowSets000.FOLLOW_9);
                    this_Stack_5=ruleStack();

                    state._fsp--;

                     
                            current = this_Stack_5; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // InternalRunstar.g:1977:5: this_Paint_6= rulePaint
                    {
                     
                            newCompositeNode(grammarAccess.getRepresentationAccess().getPaintParserRuleCall_2_4()); 
                        
                    pushFollow(FollowSets000.FOLLOW_9);
                    this_Paint_6=rulePaint();

                    state._fsp--;

                     
                            current = this_Paint_6; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // InternalRunstar.g:1987:5: this_Mark_7= ruleMark
                    {
                     
                            newCompositeNode(grammarAccess.getRepresentationAccess().getMarkParserRuleCall_2_5()); 
                        
                    pushFollow(FollowSets000.FOLLOW_9);
                    this_Mark_7=ruleMark();

                    state._fsp--;

                     
                            current = this_Mark_7; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 7 :
                    // InternalRunstar.g:1997:5: this_Comment_8= ruleComment
                    {
                     
                            newCompositeNode(grammarAccess.getRepresentationAccess().getCommentParserRuleCall_2_6()); 
                        
                    pushFollow(FollowSets000.FOLLOW_9);
                    this_Comment_8=ruleComment();

                    state._fsp--;

                     
                            current = this_Comment_8; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }

            otherlv_9=(Token)match(input,16,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_9, grammarAccess.getRepresentationAccess().getSemicolonKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRepresentation"


    // $ANTLR start "entryRulePaint"
    // InternalRunstar.g:2017:1: entryRulePaint returns [EObject current=null] : iv_rulePaint= rulePaint EOF ;
    public final EObject entryRulePaint() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePaint = null;


        try {
            // InternalRunstar.g:2018:2: (iv_rulePaint= rulePaint EOF )
            // InternalRunstar.g:2019:2: iv_rulePaint= rulePaint EOF
            {
             newCompositeNode(grammarAccess.getPaintRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_rulePaint=rulePaint();

            state._fsp--;

             current =iv_rulePaint; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePaint"


    // $ANTLR start "rulePaint"
    // InternalRunstar.g:2026:1: rulePaint returns [EObject current=null] : (otherlv_0= 'Paint' otherlv_1= '(' ( ( (lv_color_2_0= ruleRColor ) ) | otherlv_3= 'clear' ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) ) ) ) )+ {...}?) ) ) otherlv_13= ')' (otherlv_14= 'for' ( ( ruleQualifiedName ) ) )? ( (lv_ownedUpdateStrategy_16_0= ruleRUpdateStrategy ) ) ) ;
    public final EObject rulePaint() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        EObject lv_color_2_0 = null;

        Enumerator lv_type_8_0 = null;

        Enumerator lv_persistent_12_0 = null;

        EObject lv_ownedUpdateStrategy_16_0 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:2029:28: ( (otherlv_0= 'Paint' otherlv_1= '(' ( ( (lv_color_2_0= ruleRColor ) ) | otherlv_3= 'clear' ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) ) ) ) )+ {...}?) ) ) otherlv_13= ')' (otherlv_14= 'for' ( ( ruleQualifiedName ) ) )? ( (lv_ownedUpdateStrategy_16_0= ruleRUpdateStrategy ) ) ) )
            // InternalRunstar.g:2030:1: (otherlv_0= 'Paint' otherlv_1= '(' ( ( (lv_color_2_0= ruleRColor ) ) | otherlv_3= 'clear' ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) ) ) ) )+ {...}?) ) ) otherlv_13= ')' (otherlv_14= 'for' ( ( ruleQualifiedName ) ) )? ( (lv_ownedUpdateStrategy_16_0= ruleRUpdateStrategy ) ) )
            {
            // InternalRunstar.g:2030:1: (otherlv_0= 'Paint' otherlv_1= '(' ( ( (lv_color_2_0= ruleRColor ) ) | otherlv_3= 'clear' ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) ) ) ) )+ {...}?) ) ) otherlv_13= ')' (otherlv_14= 'for' ( ( ruleQualifiedName ) ) )? ( (lv_ownedUpdateStrategy_16_0= ruleRUpdateStrategy ) ) )
            // InternalRunstar.g:2030:3: otherlv_0= 'Paint' otherlv_1= '(' ( ( (lv_color_2_0= ruleRColor ) ) | otherlv_3= 'clear' ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) ) ) ) )+ {...}?) ) ) otherlv_13= ')' (otherlv_14= 'for' ( ( ruleQualifiedName ) ) )? ( (lv_ownedUpdateStrategy_16_0= ruleRUpdateStrategy ) )
            {
            otherlv_0=(Token)match(input,60,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_0, grammarAccess.getPaintAccess().getPaintKeyword_0());
                
            otherlv_1=(Token)match(input,18,FollowSets000.FOLLOW_40); 

                	newLeafNode(otherlv_1, grammarAccess.getPaintAccess().getLeftParenthesisKeyword_1());
                
            // InternalRunstar.g:2038:1: ( ( (lv_color_2_0= ruleRColor ) ) | otherlv_3= 'clear' )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==18) ) {
                alt19=1;
            }
            else if ( (LA19_0==61) ) {
                alt19=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalRunstar.g:2038:2: ( (lv_color_2_0= ruleRColor ) )
                    {
                    // InternalRunstar.g:2038:2: ( (lv_color_2_0= ruleRColor ) )
                    // InternalRunstar.g:2039:1: (lv_color_2_0= ruleRColor )
                    {
                    // InternalRunstar.g:2039:1: (lv_color_2_0= ruleRColor )
                    // InternalRunstar.g:2040:3: lv_color_2_0= ruleRColor
                    {
                     
                    	        newCompositeNode(grammarAccess.getPaintAccess().getColorRColorParserRuleCall_2_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_19);
                    lv_color_2_0=ruleRColor();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getPaintRule());
                    	        }
                           		set(
                           			current, 
                           			"color",
                            		lv_color_2_0, 
                            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.RColor");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalRunstar.g:2057:7: otherlv_3= 'clear'
                    {
                    otherlv_3=(Token)match(input,61,FollowSets000.FOLLOW_19); 

                        	newLeafNode(otherlv_3, grammarAccess.getPaintAccess().getClearKeyword_2_1());
                        

                    }
                    break;

            }

            // InternalRunstar.g:2061:2: ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) ) ) ) )+ {...}?) ) )
            // InternalRunstar.g:2063:1: ( ( ( ({...}? => ( ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) ) ) ) )+ {...}?) )
            {
            // InternalRunstar.g:2063:1: ( ( ( ({...}? => ( ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) ) ) ) )+ {...}?) )
            // InternalRunstar.g:2064:2: ( ( ({...}? => ( ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) ) ) ) )+ {...}?)
            {
             
            	  getUnorderedGroupHelper().enter(grammarAccess.getPaintAccess().getUnorderedGroup_3());
            	
            // InternalRunstar.g:2067:2: ( ( ({...}? => ( ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) ) ) ) )+ {...}?)
            // InternalRunstar.g:2068:3: ( ({...}? => ( ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) ) ) ) )+ {...}?
            {
            // InternalRunstar.g:2068:3: ( ({...}? => ( ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) ) ) ) )+
            int cnt20=0;
            loop20:
            do {
                int alt20=3;
                int LA20_0 = input.LA(1);

                if ( LA20_0 == 22 && ( getUnorderedGroupHelper().canSelect(grammarAccess.getPaintAccess().getUnorderedGroup_3(), 0) || getUnorderedGroupHelper().canSelect(grammarAccess.getPaintAccess().getUnorderedGroup_3(), 1) ) ) {
                    int LA20_2 = input.LA(2);

                    if ( LA20_2 == 62 && getUnorderedGroupHelper().canSelect(grammarAccess.getPaintAccess().getUnorderedGroup_3(), 0) ) {
                        alt20=1;
                    }
                    else if ( LA20_2 == 63 && getUnorderedGroupHelper().canSelect(grammarAccess.getPaintAccess().getUnorderedGroup_3(), 1) ) {
                        alt20=2;
                    }


                }


                switch (alt20) {
            	case 1 :
            	    // InternalRunstar.g:2070:4: ({...}? => ( ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) ) ) )
            	    {
            	    // InternalRunstar.g:2070:4: ({...}? => ( ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) ) ) )
            	    // InternalRunstar.g:2071:5: {...}? => ( ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getPaintAccess().getUnorderedGroup_3(), 0) ) {
            	        throw new FailedPredicateException(input, "rulePaint", "getUnorderedGroupHelper().canSelect(grammarAccess.getPaintAccess().getUnorderedGroup_3(), 0)");
            	    }
            	    // InternalRunstar.g:2071:102: ( ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) ) )
            	    // InternalRunstar.g:2072:6: ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getPaintAccess().getUnorderedGroup_3(), 0);
            	    	 				
            	    // InternalRunstar.g:2075:6: ({...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) ) )
            	    // InternalRunstar.g:2075:7: {...}? => (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "rulePaint", "true");
            	    }
            	    // InternalRunstar.g:2075:16: (otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) ) )
            	    // InternalRunstar.g:2075:18: otherlv_5= ',' otherlv_6= 'type' otherlv_7= '=' ( (lv_type_8_0= rulePaintType ) )
            	    {
            	    otherlv_5=(Token)match(input,22,FollowSets000.FOLLOW_41); 

            	        	newLeafNode(otherlv_5, grammarAccess.getPaintAccess().getCommaKeyword_3_0_0());
            	        
            	    otherlv_6=(Token)match(input,62,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_6, grammarAccess.getPaintAccess().getTypeKeyword_3_0_1());
            	        
            	    otherlv_7=(Token)match(input,26,FollowSets000.FOLLOW_42); 

            	        	newLeafNode(otherlv_7, grammarAccess.getPaintAccess().getEqualsSignKeyword_3_0_2());
            	        
            	    // InternalRunstar.g:2087:1: ( (lv_type_8_0= rulePaintType ) )
            	    // InternalRunstar.g:2088:1: (lv_type_8_0= rulePaintType )
            	    {
            	    // InternalRunstar.g:2088:1: (lv_type_8_0= rulePaintType )
            	    // InternalRunstar.g:2089:3: lv_type_8_0= rulePaintType
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPaintAccess().getTypePaintTypeEnumRuleCall_3_0_3_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_43);
            	    lv_type_8_0=rulePaintType();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPaintRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"type",
            	            		lv_type_8_0, 
            	            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.PaintType");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getPaintAccess().getUnorderedGroup_3());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalRunstar.g:2112:4: ({...}? => ( ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) ) ) )
            	    {
            	    // InternalRunstar.g:2112:4: ({...}? => ( ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) ) ) )
            	    // InternalRunstar.g:2113:5: {...}? => ( ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getPaintAccess().getUnorderedGroup_3(), 1) ) {
            	        throw new FailedPredicateException(input, "rulePaint", "getUnorderedGroupHelper().canSelect(grammarAccess.getPaintAccess().getUnorderedGroup_3(), 1)");
            	    }
            	    // InternalRunstar.g:2113:102: ( ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) ) )
            	    // InternalRunstar.g:2114:6: ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getPaintAccess().getUnorderedGroup_3(), 1);
            	    	 				
            	    // InternalRunstar.g:2117:6: ({...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) ) )
            	    // InternalRunstar.g:2117:7: {...}? => (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "rulePaint", "true");
            	    }
            	    // InternalRunstar.g:2117:16: (otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) ) )
            	    // InternalRunstar.g:2117:18: otherlv_9= ',' otherlv_10= 'persistent' otherlv_11= '=' ( (lv_persistent_12_0= ruleRBoolean ) )
            	    {
            	    otherlv_9=(Token)match(input,22,FollowSets000.FOLLOW_44); 

            	        	newLeafNode(otherlv_9, grammarAccess.getPaintAccess().getCommaKeyword_3_1_0());
            	        
            	    otherlv_10=(Token)match(input,63,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_10, grammarAccess.getPaintAccess().getPersistentKeyword_3_1_1());
            	        
            	    otherlv_11=(Token)match(input,26,FollowSets000.FOLLOW_45); 

            	        	newLeafNode(otherlv_11, grammarAccess.getPaintAccess().getEqualsSignKeyword_3_1_2());
            	        
            	    // InternalRunstar.g:2129:1: ( (lv_persistent_12_0= ruleRBoolean ) )
            	    // InternalRunstar.g:2130:1: (lv_persistent_12_0= ruleRBoolean )
            	    {
            	    // InternalRunstar.g:2130:1: (lv_persistent_12_0= ruleRBoolean )
            	    // InternalRunstar.g:2131:3: lv_persistent_12_0= ruleRBoolean
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPaintAccess().getPersistentRBooleanEnumRuleCall_3_1_3_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_43);
            	    lv_persistent_12_0=ruleRBoolean();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPaintRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"persistent",
            	            		lv_persistent_12_0, 
            	            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.RBoolean");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getPaintAccess().getUnorderedGroup_3());
            	    	 				

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt20 >= 1 ) break loop20;
                        EarlyExitException eee =
                            new EarlyExitException(20, input);
                        throw eee;
                }
                cnt20++;
            } while (true);

            if ( ! getUnorderedGroupHelper().canLeave(grammarAccess.getPaintAccess().getUnorderedGroup_3()) ) {
                throw new FailedPredicateException(input, "rulePaint", "getUnorderedGroupHelper().canLeave(grammarAccess.getPaintAccess().getUnorderedGroup_3())");
            }

            }


            }

             
            	  getUnorderedGroupHelper().leave(grammarAccess.getPaintAccess().getUnorderedGroup_3());
            	

            }

            otherlv_13=(Token)match(input,19,FollowSets000.FOLLOW_46); 

                	newLeafNode(otherlv_13, grammarAccess.getPaintAccess().getRightParenthesisKeyword_4());
                
            // InternalRunstar.g:2166:1: (otherlv_14= 'for' ( ( ruleQualifiedName ) ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==47) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalRunstar.g:2166:3: otherlv_14= 'for' ( ( ruleQualifiedName ) )
                    {
                    otherlv_14=(Token)match(input,47,FollowSets000.FOLLOW_15); 

                        	newLeafNode(otherlv_14, grammarAccess.getPaintAccess().getForKeyword_5_0());
                        
                    // InternalRunstar.g:2170:1: ( ( ruleQualifiedName ) )
                    // InternalRunstar.g:2171:1: ( ruleQualifiedName )
                    {
                    // InternalRunstar.g:2171:1: ( ruleQualifiedName )
                    // InternalRunstar.g:2172:3: ruleQualifiedName
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getPaintRule());
                    	        }
                            
                     
                    	        newCompositeNode(grammarAccess.getPaintAccess().getOnConceptEObjectCrossReference_5_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_47);
                    ruleQualifiedName();

                    state._fsp--;

                     
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // InternalRunstar.g:2185:4: ( (lv_ownedUpdateStrategy_16_0= ruleRUpdateStrategy ) )
            // InternalRunstar.g:2186:1: (lv_ownedUpdateStrategy_16_0= ruleRUpdateStrategy )
            {
            // InternalRunstar.g:2186:1: (lv_ownedUpdateStrategy_16_0= ruleRUpdateStrategy )
            // InternalRunstar.g:2187:3: lv_ownedUpdateStrategy_16_0= ruleRUpdateStrategy
            {
             
            	        newCompositeNode(grammarAccess.getPaintAccess().getOwnedUpdateStrategyRUpdateStrategyParserRuleCall_6_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_2);
            lv_ownedUpdateStrategy_16_0=ruleRUpdateStrategy();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPaintRule());
            	        }
                   		set(
                   			current, 
                   			"ownedUpdateStrategy",
                    		lv_ownedUpdateStrategy_16_0, 
                    		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.RUpdateStrategy");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePaint"


    // $ANTLR start "entryRuleMark"
    // InternalRunstar.g:2211:1: entryRuleMark returns [EObject current=null] : iv_ruleMark= ruleMark EOF ;
    public final EObject entryRuleMark() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMark = null;


        try {
            // InternalRunstar.g:2212:2: (iv_ruleMark= ruleMark EOF )
            // InternalRunstar.g:2213:2: iv_ruleMark= ruleMark EOF
            {
             newCompositeNode(grammarAccess.getMarkRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleMark=ruleMark();

            state._fsp--;

             current =iv_ruleMark; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMark"


    // $ANTLR start "ruleMark"
    // InternalRunstar.g:2220:1: ruleMark returns [EObject current=null] : (otherlv_0= 'Mark' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' otherlv_4= 'type' otherlv_5= '=' ( (lv_markerType_6_0= ruleMarkerType ) ) ) otherlv_7= ')' ) ;
    public final EObject ruleMark() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Enumerator lv_markerType_6_0 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:2223:28: ( (otherlv_0= 'Mark' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' otherlv_4= 'type' otherlv_5= '=' ( (lv_markerType_6_0= ruleMarkerType ) ) ) otherlv_7= ')' ) )
            // InternalRunstar.g:2224:1: (otherlv_0= 'Mark' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' otherlv_4= 'type' otherlv_5= '=' ( (lv_markerType_6_0= ruleMarkerType ) ) ) otherlv_7= ')' )
            {
            // InternalRunstar.g:2224:1: (otherlv_0= 'Mark' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' otherlv_4= 'type' otherlv_5= '=' ( (lv_markerType_6_0= ruleMarkerType ) ) ) otherlv_7= ')' )
            // InternalRunstar.g:2224:3: otherlv_0= 'Mark' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' otherlv_4= 'type' otherlv_5= '=' ( (lv_markerType_6_0= ruleMarkerType ) ) ) otherlv_7= ')'
            {
            otherlv_0=(Token)match(input,64,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_0, grammarAccess.getMarkAccess().getMarkKeyword_0());
                
            otherlv_1=(Token)match(input,18,FollowSets000.FOLLOW_15); 

                	newLeafNode(otherlv_1, grammarAccess.getMarkAccess().getLeftParenthesisKeyword_1());
                
            // InternalRunstar.g:2232:1: ( (otherlv_2= RULE_ID ) )
            // InternalRunstar.g:2233:1: (otherlv_2= RULE_ID )
            {
            // InternalRunstar.g:2233:1: (otherlv_2= RULE_ID )
            // InternalRunstar.g:2234:3: otherlv_2= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getMarkRule());
            	        }
                    
            otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_19); 

            		newLeafNode(otherlv_2, grammarAccess.getMarkAccess().getTextTextVarCrossReference_2_0()); 
            	

            }


            }

            // InternalRunstar.g:2245:2: (otherlv_3= ',' otherlv_4= 'type' otherlv_5= '=' ( (lv_markerType_6_0= ruleMarkerType ) ) )
            // InternalRunstar.g:2245:4: otherlv_3= ',' otherlv_4= 'type' otherlv_5= '=' ( (lv_markerType_6_0= ruleMarkerType ) )
            {
            otherlv_3=(Token)match(input,22,FollowSets000.FOLLOW_41); 

                	newLeafNode(otherlv_3, grammarAccess.getMarkAccess().getCommaKeyword_3_0());
                
            otherlv_4=(Token)match(input,62,FollowSets000.FOLLOW_18); 

                	newLeafNode(otherlv_4, grammarAccess.getMarkAccess().getTypeKeyword_3_1());
                
            otherlv_5=(Token)match(input,26,FollowSets000.FOLLOW_48); 

                	newLeafNode(otherlv_5, grammarAccess.getMarkAccess().getEqualsSignKeyword_3_2());
                
            // InternalRunstar.g:2257:1: ( (lv_markerType_6_0= ruleMarkerType ) )
            // InternalRunstar.g:2258:1: (lv_markerType_6_0= ruleMarkerType )
            {
            // InternalRunstar.g:2258:1: (lv_markerType_6_0= ruleMarkerType )
            // InternalRunstar.g:2259:3: lv_markerType_6_0= ruleMarkerType
            {
             
            	        newCompositeNode(grammarAccess.getMarkAccess().getMarkerTypeMarkerTypeEnumRuleCall_3_3_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_13);
            lv_markerType_6_0=ruleMarkerType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMarkRule());
            	        }
                   		set(
                   			current, 
                   			"markerType",
                    		lv_markerType_6_0, 
                    		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.MarkerType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

            otherlv_7=(Token)match(input,19,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_7, grammarAccess.getMarkAccess().getRightParenthesisKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMark"


    // $ANTLR start "entryRuleHover"
    // InternalRunstar.g:2287:1: entryRuleHover returns [EObject current=null] : iv_ruleHover= ruleHover EOF ;
    public final EObject entryRuleHover() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHover = null;


        try {
            // InternalRunstar.g:2288:2: (iv_ruleHover= ruleHover EOF )
            // InternalRunstar.g:2289:2: iv_ruleHover= ruleHover EOF
            {
             newCompositeNode(grammarAccess.getHoverRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleHover=ruleHover();

            state._fsp--;

             current =iv_ruleHover; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHover"


    // $ANTLR start "ruleHover"
    // InternalRunstar.g:2296:1: ruleHover returns [EObject current=null] : (otherlv_0= 'Hover' otherlv_1= '(' ( ( ( (otherlv_2= RULE_ID ) ) | (otherlv_3= 'image' otherlv_4= '=' ( (otherlv_5= RULE_ID ) ) ) ) | ( ( (otherlv_6= RULE_ID ) ) otherlv_7= ',' otherlv_8= 'image' otherlv_9= '=' ( (otherlv_10= RULE_ID ) ) ) ) (otherlv_11= ',' otherlv_12= 'fromStart' otherlv_13= '=' ( (lv_fromStart_14_0= ruleRBoolean ) ) )? otherlv_15= ')' ) ;
    public final EObject ruleHover() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Enumerator lv_fromStart_14_0 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:2299:28: ( (otherlv_0= 'Hover' otherlv_1= '(' ( ( ( (otherlv_2= RULE_ID ) ) | (otherlv_3= 'image' otherlv_4= '=' ( (otherlv_5= RULE_ID ) ) ) ) | ( ( (otherlv_6= RULE_ID ) ) otherlv_7= ',' otherlv_8= 'image' otherlv_9= '=' ( (otherlv_10= RULE_ID ) ) ) ) (otherlv_11= ',' otherlv_12= 'fromStart' otherlv_13= '=' ( (lv_fromStart_14_0= ruleRBoolean ) ) )? otherlv_15= ')' ) )
            // InternalRunstar.g:2300:1: (otherlv_0= 'Hover' otherlv_1= '(' ( ( ( (otherlv_2= RULE_ID ) ) | (otherlv_3= 'image' otherlv_4= '=' ( (otherlv_5= RULE_ID ) ) ) ) | ( ( (otherlv_6= RULE_ID ) ) otherlv_7= ',' otherlv_8= 'image' otherlv_9= '=' ( (otherlv_10= RULE_ID ) ) ) ) (otherlv_11= ',' otherlv_12= 'fromStart' otherlv_13= '=' ( (lv_fromStart_14_0= ruleRBoolean ) ) )? otherlv_15= ')' )
            {
            // InternalRunstar.g:2300:1: (otherlv_0= 'Hover' otherlv_1= '(' ( ( ( (otherlv_2= RULE_ID ) ) | (otherlv_3= 'image' otherlv_4= '=' ( (otherlv_5= RULE_ID ) ) ) ) | ( ( (otherlv_6= RULE_ID ) ) otherlv_7= ',' otherlv_8= 'image' otherlv_9= '=' ( (otherlv_10= RULE_ID ) ) ) ) (otherlv_11= ',' otherlv_12= 'fromStart' otherlv_13= '=' ( (lv_fromStart_14_0= ruleRBoolean ) ) )? otherlv_15= ')' )
            // InternalRunstar.g:2300:3: otherlv_0= 'Hover' otherlv_1= '(' ( ( ( (otherlv_2= RULE_ID ) ) | (otherlv_3= 'image' otherlv_4= '=' ( (otherlv_5= RULE_ID ) ) ) ) | ( ( (otherlv_6= RULE_ID ) ) otherlv_7= ',' otherlv_8= 'image' otherlv_9= '=' ( (otherlv_10= RULE_ID ) ) ) ) (otherlv_11= ',' otherlv_12= 'fromStart' otherlv_13= '=' ( (lv_fromStart_14_0= ruleRBoolean ) ) )? otherlv_15= ')'
            {
            otherlv_0=(Token)match(input,65,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_0, grammarAccess.getHoverAccess().getHoverKeyword_0());
                
            otherlv_1=(Token)match(input,18,FollowSets000.FOLLOW_49); 

                	newLeafNode(otherlv_1, grammarAccess.getHoverAccess().getLeftParenthesisKeyword_1());
                
            // InternalRunstar.g:2308:1: ( ( ( (otherlv_2= RULE_ID ) ) | (otherlv_3= 'image' otherlv_4= '=' ( (otherlv_5= RULE_ID ) ) ) ) | ( ( (otherlv_6= RULE_ID ) ) otherlv_7= ',' otherlv_8= 'image' otherlv_9= '=' ( (otherlv_10= RULE_ID ) ) ) )
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==RULE_ID) ) {
                int LA23_1 = input.LA(2);

                if ( (LA23_1==22) ) {
                    int LA23_3 = input.LA(3);

                    if ( (LA23_3==67) ) {
                        alt23=1;
                    }
                    else if ( (LA23_3==66) ) {
                        alt23=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 23, 3, input);

                        throw nvae;
                    }
                }
                else if ( (LA23_1==19) ) {
                    alt23=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 23, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA23_0==66) ) {
                alt23=1;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }
            switch (alt23) {
                case 1 :
                    // InternalRunstar.g:2308:2: ( ( (otherlv_2= RULE_ID ) ) | (otherlv_3= 'image' otherlv_4= '=' ( (otherlv_5= RULE_ID ) ) ) )
                    {
                    // InternalRunstar.g:2308:2: ( ( (otherlv_2= RULE_ID ) ) | (otherlv_3= 'image' otherlv_4= '=' ( (otherlv_5= RULE_ID ) ) ) )
                    int alt22=2;
                    int LA22_0 = input.LA(1);

                    if ( (LA22_0==RULE_ID) ) {
                        alt22=1;
                    }
                    else if ( (LA22_0==66) ) {
                        alt22=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 22, 0, input);

                        throw nvae;
                    }
                    switch (alt22) {
                        case 1 :
                            // InternalRunstar.g:2308:3: ( (otherlv_2= RULE_ID ) )
                            {
                            // InternalRunstar.g:2308:3: ( (otherlv_2= RULE_ID ) )
                            // InternalRunstar.g:2309:1: (otherlv_2= RULE_ID )
                            {
                            // InternalRunstar.g:2309:1: (otherlv_2= RULE_ID )
                            // InternalRunstar.g:2310:3: otherlv_2= RULE_ID
                            {

                            			if (current==null) {
                            	            current = createModelElement(grammarAccess.getHoverRule());
                            	        }
                                    
                            otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_43); 

                            		newLeafNode(otherlv_2, grammarAccess.getHoverAccess().getTextTextVarCrossReference_2_0_0_0()); 
                            	

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalRunstar.g:2322:6: (otherlv_3= 'image' otherlv_4= '=' ( (otherlv_5= RULE_ID ) ) )
                            {
                            // InternalRunstar.g:2322:6: (otherlv_3= 'image' otherlv_4= '=' ( (otherlv_5= RULE_ID ) ) )
                            // InternalRunstar.g:2322:8: otherlv_3= 'image' otherlv_4= '=' ( (otherlv_5= RULE_ID ) )
                            {
                            otherlv_3=(Token)match(input,66,FollowSets000.FOLLOW_18); 

                                	newLeafNode(otherlv_3, grammarAccess.getHoverAccess().getImageKeyword_2_0_1_0());
                                
                            otherlv_4=(Token)match(input,26,FollowSets000.FOLLOW_15); 

                                	newLeafNode(otherlv_4, grammarAccess.getHoverAccess().getEqualsSignKeyword_2_0_1_1());
                                
                            // InternalRunstar.g:2330:1: ( (otherlv_5= RULE_ID ) )
                            // InternalRunstar.g:2331:1: (otherlv_5= RULE_ID )
                            {
                            // InternalRunstar.g:2331:1: (otherlv_5= RULE_ID )
                            // InternalRunstar.g:2332:3: otherlv_5= RULE_ID
                            {

                            			if (current==null) {
                            	            current = createModelElement(grammarAccess.getHoverRule());
                            	        }
                                    
                            otherlv_5=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_43); 

                            		newLeafNode(otherlv_5, grammarAccess.getHoverAccess().getImageImageVarCrossReference_2_0_1_2_0()); 
                            	

                            }


                            }


                            }


                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // InternalRunstar.g:2344:6: ( ( (otherlv_6= RULE_ID ) ) otherlv_7= ',' otherlv_8= 'image' otherlv_9= '=' ( (otherlv_10= RULE_ID ) ) )
                    {
                    // InternalRunstar.g:2344:6: ( ( (otherlv_6= RULE_ID ) ) otherlv_7= ',' otherlv_8= 'image' otherlv_9= '=' ( (otherlv_10= RULE_ID ) ) )
                    // InternalRunstar.g:2344:7: ( (otherlv_6= RULE_ID ) ) otherlv_7= ',' otherlv_8= 'image' otherlv_9= '=' ( (otherlv_10= RULE_ID ) )
                    {
                    // InternalRunstar.g:2344:7: ( (otherlv_6= RULE_ID ) )
                    // InternalRunstar.g:2345:1: (otherlv_6= RULE_ID )
                    {
                    // InternalRunstar.g:2345:1: (otherlv_6= RULE_ID )
                    // InternalRunstar.g:2346:3: otherlv_6= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getHoverRule());
                    	        }
                            
                    otherlv_6=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_19); 

                    		newLeafNode(otherlv_6, grammarAccess.getHoverAccess().getTextTextVarCrossReference_2_1_0_0()); 
                    	

                    }


                    }

                    otherlv_7=(Token)match(input,22,FollowSets000.FOLLOW_50); 

                        	newLeafNode(otherlv_7, grammarAccess.getHoverAccess().getCommaKeyword_2_1_1());
                        
                    otherlv_8=(Token)match(input,66,FollowSets000.FOLLOW_18); 

                        	newLeafNode(otherlv_8, grammarAccess.getHoverAccess().getImageKeyword_2_1_2());
                        
                    otherlv_9=(Token)match(input,26,FollowSets000.FOLLOW_15); 

                        	newLeafNode(otherlv_9, grammarAccess.getHoverAccess().getEqualsSignKeyword_2_1_3());
                        
                    // InternalRunstar.g:2369:1: ( (otherlv_10= RULE_ID ) )
                    // InternalRunstar.g:2370:1: (otherlv_10= RULE_ID )
                    {
                    // InternalRunstar.g:2370:1: (otherlv_10= RULE_ID )
                    // InternalRunstar.g:2371:3: otherlv_10= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getHoverRule());
                    	        }
                            
                    otherlv_10=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_43); 

                    		newLeafNode(otherlv_10, grammarAccess.getHoverAccess().getImageImageVarCrossReference_2_1_4_0()); 
                    	

                    }


                    }


                    }


                    }
                    break;

            }

            // InternalRunstar.g:2382:4: (otherlv_11= ',' otherlv_12= 'fromStart' otherlv_13= '=' ( (lv_fromStart_14_0= ruleRBoolean ) ) )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==22) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalRunstar.g:2382:6: otherlv_11= ',' otherlv_12= 'fromStart' otherlv_13= '=' ( (lv_fromStart_14_0= ruleRBoolean ) )
                    {
                    otherlv_11=(Token)match(input,22,FollowSets000.FOLLOW_51); 

                        	newLeafNode(otherlv_11, grammarAccess.getHoverAccess().getCommaKeyword_3_0());
                        
                    otherlv_12=(Token)match(input,67,FollowSets000.FOLLOW_18); 

                        	newLeafNode(otherlv_12, grammarAccess.getHoverAccess().getFromStartKeyword_3_1());
                        
                    otherlv_13=(Token)match(input,26,FollowSets000.FOLLOW_45); 

                        	newLeafNode(otherlv_13, grammarAccess.getHoverAccess().getEqualsSignKeyword_3_2());
                        
                    // InternalRunstar.g:2394:1: ( (lv_fromStart_14_0= ruleRBoolean ) )
                    // InternalRunstar.g:2395:1: (lv_fromStart_14_0= ruleRBoolean )
                    {
                    // InternalRunstar.g:2395:1: (lv_fromStart_14_0= ruleRBoolean )
                    // InternalRunstar.g:2396:3: lv_fromStart_14_0= ruleRBoolean
                    {
                     
                    	        newCompositeNode(grammarAccess.getHoverAccess().getFromStartRBooleanEnumRuleCall_3_3_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_13);
                    lv_fromStart_14_0=ruleRBoolean();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getHoverRule());
                    	        }
                           		set(
                           			current, 
                           			"fromStart",
                            		lv_fromStart_14_0, 
                            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.RBoolean");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_15=(Token)match(input,19,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_15, grammarAccess.getHoverAccess().getRightParenthesisKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHover"


    // $ANTLR start "entryRuleComment"
    // InternalRunstar.g:2424:1: entryRuleComment returns [EObject current=null] : iv_ruleComment= ruleComment EOF ;
    public final EObject entryRuleComment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComment = null;


        try {
            // InternalRunstar.g:2425:2: (iv_ruleComment= ruleComment EOF )
            // InternalRunstar.g:2426:2: iv_ruleComment= ruleComment EOF
            {
             newCompositeNode(grammarAccess.getCommentRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleComment=ruleComment();

            state._fsp--;

             current =iv_ruleComment; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComment"


    // $ANTLR start "ruleComment"
    // InternalRunstar.g:2433:1: ruleComment returns [EObject current=null] : (otherlv_0= 'Comment' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' ) ;
    public final EObject ruleComment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // InternalRunstar.g:2436:28: ( (otherlv_0= 'Comment' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' ) )
            // InternalRunstar.g:2437:1: (otherlv_0= 'Comment' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' )
            {
            // InternalRunstar.g:2437:1: (otherlv_0= 'Comment' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' )
            // InternalRunstar.g:2437:3: otherlv_0= 'Comment' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')'
            {
            otherlv_0=(Token)match(input,68,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_0, grammarAccess.getCommentAccess().getCommentKeyword_0());
                
            otherlv_1=(Token)match(input,18,FollowSets000.FOLLOW_15); 

                	newLeafNode(otherlv_1, grammarAccess.getCommentAccess().getLeftParenthesisKeyword_1());
                
            // InternalRunstar.g:2445:1: ( (otherlv_2= RULE_ID ) )
            // InternalRunstar.g:2446:1: (otherlv_2= RULE_ID )
            {
            // InternalRunstar.g:2446:1: (otherlv_2= RULE_ID )
            // InternalRunstar.g:2447:3: otherlv_2= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getCommentRule());
            	        }
                    
            otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_13); 

            		newLeafNode(otherlv_2, grammarAccess.getCommentAccess().getTextTextVarCrossReference_2_0()); 
            	

            }


            }

            otherlv_3=(Token)match(input,19,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getCommentAccess().getRightParenthesisKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComment"


    // $ANTLR start "entryRuleLabel"
    // InternalRunstar.g:2470:1: entryRuleLabel returns [EObject current=null] : iv_ruleLabel= ruleLabel EOF ;
    public final EObject entryRuleLabel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLabel = null;


        try {
            // InternalRunstar.g:2471:2: (iv_ruleLabel= ruleLabel EOF )
            // InternalRunstar.g:2472:2: iv_ruleLabel= ruleLabel EOF
            {
             newCompositeNode(grammarAccess.getLabelRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleLabel=ruleLabel();

            state._fsp--;

             current =iv_ruleLabel; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLabel"


    // $ANTLR start "ruleLabel"
    // InternalRunstar.g:2479:1: ruleLabel returns [EObject current=null] : (otherlv_0= 'Label' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' ) ;
    public final EObject ruleLabel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // InternalRunstar.g:2482:28: ( (otherlv_0= 'Label' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' ) )
            // InternalRunstar.g:2483:1: (otherlv_0= 'Label' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' )
            {
            // InternalRunstar.g:2483:1: (otherlv_0= 'Label' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' )
            // InternalRunstar.g:2483:3: otherlv_0= 'Label' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')'
            {
            otherlv_0=(Token)match(input,69,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_0, grammarAccess.getLabelAccess().getLabelKeyword_0());
                
            otherlv_1=(Token)match(input,18,FollowSets000.FOLLOW_15); 

                	newLeafNode(otherlv_1, grammarAccess.getLabelAccess().getLeftParenthesisKeyword_1());
                
            // InternalRunstar.g:2491:1: ( (otherlv_2= RULE_ID ) )
            // InternalRunstar.g:2492:1: (otherlv_2= RULE_ID )
            {
            // InternalRunstar.g:2492:1: (otherlv_2= RULE_ID )
            // InternalRunstar.g:2493:3: otherlv_2= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getLabelRule());
            	        }
                    
            otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_13); 

            		newLeafNode(otherlv_2, grammarAccess.getLabelAccess().getTextTextVarCrossReference_2_0()); 
            	

            }


            }

            otherlv_3=(Token)match(input,19,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getLabelAccess().getRightParenthesisKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLabel"


    // $ANTLR start "entryRuleImage"
    // InternalRunstar.g:2516:1: entryRuleImage returns [EObject current=null] : iv_ruleImage= ruleImage EOF ;
    public final EObject entryRuleImage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImage = null;


        try {
            // InternalRunstar.g:2517:2: (iv_ruleImage= ruleImage EOF )
            // InternalRunstar.g:2518:2: iv_ruleImage= ruleImage EOF
            {
             newCompositeNode(grammarAccess.getImageRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleImage=ruleImage();

            state._fsp--;

             current =iv_ruleImage; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImage"


    // $ANTLR start "ruleImage"
    // InternalRunstar.g:2525:1: ruleImage returns [EObject current=null] : (otherlv_0= 'Image' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) ) ) ) )* ) ) ) otherlv_16= ')' ) ;
    public final EObject ruleImage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token lv_initialSize_7_0=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token lv_depth_11_0=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;

         enterRule(); 
            
        try {
            // InternalRunstar.g:2528:28: ( (otherlv_0= 'Image' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) ) ) ) )* ) ) ) otherlv_16= ')' ) )
            // InternalRunstar.g:2529:1: (otherlv_0= 'Image' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) ) ) ) )* ) ) ) otherlv_16= ')' )
            {
            // InternalRunstar.g:2529:1: (otherlv_0= 'Image' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) ) ) ) )* ) ) ) otherlv_16= ')' )
            // InternalRunstar.g:2529:3: otherlv_0= 'Image' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) ) ) ) )* ) ) ) otherlv_16= ')'
            {
            otherlv_0=(Token)match(input,17,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_0, grammarAccess.getImageAccess().getImageKeyword_0());
                
            otherlv_1=(Token)match(input,18,FollowSets000.FOLLOW_15); 

                	newLeafNode(otherlv_1, grammarAccess.getImageAccess().getLeftParenthesisKeyword_1());
                
            // InternalRunstar.g:2537:1: ( (otherlv_2= RULE_ID ) )
            // InternalRunstar.g:2538:1: (otherlv_2= RULE_ID )
            {
            // InternalRunstar.g:2538:1: (otherlv_2= RULE_ID )
            // InternalRunstar.g:2539:3: otherlv_2= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getImageRule());
            	        }
                    
            otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_43); 

            		newLeafNode(otherlv_2, grammarAccess.getImageAccess().getImageImageVarCrossReference_2_0()); 
            	

            }


            }

            // InternalRunstar.g:2550:2: ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) ) ) ) )* ) ) )
            // InternalRunstar.g:2552:1: ( ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) ) ) ) )* ) )
            {
            // InternalRunstar.g:2552:1: ( ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) ) ) ) )* ) )
            // InternalRunstar.g:2553:2: ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) ) ) ) )* )
            {
             
            	  getUnorderedGroupHelper().enter(grammarAccess.getImageAccess().getUnorderedGroup_3());
            	
            // InternalRunstar.g:2556:2: ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) ) ) ) )* )
            // InternalRunstar.g:2557:3: ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) ) ) ) )*
            {
            // InternalRunstar.g:2557:3: ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) ) ) ) )*
            loop25:
            do {
                int alt25=4;
                int LA25_0 = input.LA(1);

                if ( LA25_0 == 22 && ( getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 1) || getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 2) || getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 0) ) ) {
                    int LA25_2 = input.LA(2);

                    if ( LA25_2 == 70 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 0) ) {
                        alt25=1;
                    }
                    else if ( LA25_2 == 71 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 1) ) {
                        alt25=2;
                    }
                    else if ( LA25_2 == 72 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 2) ) {
                        alt25=3;
                    }


                }


                switch (alt25) {
            	case 1 :
            	    // InternalRunstar.g:2559:4: ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) ) ) )
            	    {
            	    // InternalRunstar.g:2559:4: ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) ) ) )
            	    // InternalRunstar.g:2560:5: {...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleImage", "getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 0)");
            	    }
            	    // InternalRunstar.g:2560:102: ( ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) ) )
            	    // InternalRunstar.g:2561:6: ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getImageAccess().getUnorderedGroup_3(), 0);
            	    	 				
            	    // InternalRunstar.g:2564:6: ({...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) ) )
            	    // InternalRunstar.g:2564:7: {...}? => (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleImage", "true");
            	    }
            	    // InternalRunstar.g:2564:16: (otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) ) )
            	    // InternalRunstar.g:2564:18: otherlv_4= ',' otherlv_5= 'size' otherlv_6= '=' ( (lv_initialSize_7_0= RULE_INT ) )
            	    {
            	    otherlv_4=(Token)match(input,22,FollowSets000.FOLLOW_52); 

            	        	newLeafNode(otherlv_4, grammarAccess.getImageAccess().getCommaKeyword_3_0_0());
            	        
            	    otherlv_5=(Token)match(input,70,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_5, grammarAccess.getImageAccess().getSizeKeyword_3_0_1());
            	        
            	    otherlv_6=(Token)match(input,26,FollowSets000.FOLLOW_26); 

            	        	newLeafNode(otherlv_6, grammarAccess.getImageAccess().getEqualsSignKeyword_3_0_2());
            	        
            	    // InternalRunstar.g:2576:1: ( (lv_initialSize_7_0= RULE_INT ) )
            	    // InternalRunstar.g:2577:1: (lv_initialSize_7_0= RULE_INT )
            	    {
            	    // InternalRunstar.g:2577:1: (lv_initialSize_7_0= RULE_INT )
            	    // InternalRunstar.g:2578:3: lv_initialSize_7_0= RULE_INT
            	    {
            	    lv_initialSize_7_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_43); 

            	    			newLeafNode(lv_initialSize_7_0, grammarAccess.getImageAccess().getInitialSizeINTTerminalRuleCall_3_0_3_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getImageRule());
            	    	        }
            	           		setWithLastConsumed(
            	           			current, 
            	           			"initialSize",
            	            		lv_initialSize_7_0, 
            	            		"org.eclipse.xtext.common.Terminals.INT");
            	    	    

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getImageAccess().getUnorderedGroup_3());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalRunstar.g:2601:4: ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) ) ) )
            	    {
            	    // InternalRunstar.g:2601:4: ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) ) ) )
            	    // InternalRunstar.g:2602:5: {...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleImage", "getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 1)");
            	    }
            	    // InternalRunstar.g:2602:102: ( ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) ) )
            	    // InternalRunstar.g:2603:6: ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getImageAccess().getUnorderedGroup_3(), 1);
            	    	 				
            	    // InternalRunstar.g:2606:6: ({...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) ) )
            	    // InternalRunstar.g:2606:7: {...}? => (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleImage", "true");
            	    }
            	    // InternalRunstar.g:2606:16: (otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) ) )
            	    // InternalRunstar.g:2606:18: otherlv_8= ',' otherlv_9= 'depth' otherlv_10= '=' ( (lv_depth_11_0= RULE_INT ) )
            	    {
            	    otherlv_8=(Token)match(input,22,FollowSets000.FOLLOW_53); 

            	        	newLeafNode(otherlv_8, grammarAccess.getImageAccess().getCommaKeyword_3_1_0());
            	        
            	    otherlv_9=(Token)match(input,71,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_9, grammarAccess.getImageAccess().getDepthKeyword_3_1_1());
            	        
            	    otherlv_10=(Token)match(input,26,FollowSets000.FOLLOW_26); 

            	        	newLeafNode(otherlv_10, grammarAccess.getImageAccess().getEqualsSignKeyword_3_1_2());
            	        
            	    // InternalRunstar.g:2618:1: ( (lv_depth_11_0= RULE_INT ) )
            	    // InternalRunstar.g:2619:1: (lv_depth_11_0= RULE_INT )
            	    {
            	    // InternalRunstar.g:2619:1: (lv_depth_11_0= RULE_INT )
            	    // InternalRunstar.g:2620:3: lv_depth_11_0= RULE_INT
            	    {
            	    lv_depth_11_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_43); 

            	    			newLeafNode(lv_depth_11_0, grammarAccess.getImageAccess().getDepthINTTerminalRuleCall_3_1_3_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getImageRule());
            	    	        }
            	           		setWithLastConsumed(
            	           			current, 
            	           			"depth",
            	            		lv_depth_11_0, 
            	            		"org.eclipse.xtext.common.Terminals.INT");
            	    	    

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getImageAccess().getUnorderedGroup_3());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalRunstar.g:2643:4: ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) ) ) )
            	    {
            	    // InternalRunstar.g:2643:4: ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) ) ) )
            	    // InternalRunstar.g:2644:5: {...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleImage", "getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 2)");
            	    }
            	    // InternalRunstar.g:2644:102: ( ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) ) )
            	    // InternalRunstar.g:2645:6: ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getImageAccess().getUnorderedGroup_3(), 2);
            	    	 				
            	    // InternalRunstar.g:2648:6: ({...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) ) )
            	    // InternalRunstar.g:2648:7: {...}? => (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleImage", "true");
            	    }
            	    // InternalRunstar.g:2648:16: (otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) ) )
            	    // InternalRunstar.g:2648:18: otherlv_12= ',' otherlv_13= 'label' otherlv_14= '=' ( (otherlv_15= RULE_ID ) )
            	    {
            	    otherlv_12=(Token)match(input,22,FollowSets000.FOLLOW_54); 

            	        	newLeafNode(otherlv_12, grammarAccess.getImageAccess().getCommaKeyword_3_2_0());
            	        
            	    otherlv_13=(Token)match(input,72,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_13, grammarAccess.getImageAccess().getLabelKeyword_3_2_1());
            	        
            	    otherlv_14=(Token)match(input,26,FollowSets000.FOLLOW_15); 

            	        	newLeafNode(otherlv_14, grammarAccess.getImageAccess().getEqualsSignKeyword_3_2_2());
            	        
            	    // InternalRunstar.g:2660:1: ( (otherlv_15= RULE_ID ) )
            	    // InternalRunstar.g:2661:1: (otherlv_15= RULE_ID )
            	    {
            	    // InternalRunstar.g:2661:1: (otherlv_15= RULE_ID )
            	    // InternalRunstar.g:2662:3: otherlv_15= RULE_ID
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getImageRule());
            	    	        }
            	            
            	    otherlv_15=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_43); 

            	    		newLeafNode(otherlv_15, grammarAccess.getImageAccess().getTextTextVarCrossReference_3_2_3_0()); 
            	    	

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getImageAccess().getUnorderedGroup_3());
            	    	 				

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);


            }


            }

             
            	  getUnorderedGroupHelper().leave(grammarAccess.getImageAccess().getUnorderedGroup_3());
            	

            }

            otherlv_16=(Token)match(input,19,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_16, grammarAccess.getImageAccess().getRightParenthesisKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImage"


    // $ANTLR start "entryRuleStack"
    // InternalRunstar.g:2699:1: entryRuleStack returns [EObject current=null] : iv_ruleStack= ruleStack EOF ;
    public final EObject entryRuleStack() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStack = null;


        try {
            // InternalRunstar.g:2700:2: (iv_ruleStack= ruleStack EOF )
            // InternalRunstar.g:2701:2: iv_ruleStack= ruleStack EOF
            {
             newCompositeNode(grammarAccess.getStackRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStack=ruleStack();

            state._fsp--;

             current =iv_ruleStack; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStack"


    // $ANTLR start "ruleStack"
    // InternalRunstar.g:2708:1: ruleStack returns [EObject current=null] : (otherlv_0= 'Stack' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) ) ) ) )* ) ) ) otherlv_28= ')' ) ;
    public final EObject ruleStack() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token lv_maxLength_7_0=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token lv_title_23_0=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        Token otherlv_26=null;
        Token lv_depth_27_0=null;
        Token otherlv_28=null;
        Enumerator lv_isVertical_11_0 = null;

        Enumerator lv_shape_15_0 = null;

        Enumerator lv_strategy_19_0 = null;


         enterRule(); 
            
        try {
            // InternalRunstar.g:2711:28: ( (otherlv_0= 'Stack' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) ) ) ) )* ) ) ) otherlv_28= ')' ) )
            // InternalRunstar.g:2712:1: (otherlv_0= 'Stack' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) ) ) ) )* ) ) ) otherlv_28= ')' )
            {
            // InternalRunstar.g:2712:1: (otherlv_0= 'Stack' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) ) ) ) )* ) ) ) otherlv_28= ')' )
            // InternalRunstar.g:2712:3: otherlv_0= 'Stack' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) ) ) ) )* ) ) ) otherlv_28= ')'
            {
            otherlv_0=(Token)match(input,73,FollowSets000.FOLLOW_10); 

                	newLeafNode(otherlv_0, grammarAccess.getStackAccess().getStackKeyword_0());
                
            otherlv_1=(Token)match(input,18,FollowSets000.FOLLOW_15); 

                	newLeafNode(otherlv_1, grammarAccess.getStackAccess().getLeftParenthesisKeyword_1());
                
            // InternalRunstar.g:2720:1: ( (otherlv_2= RULE_ID ) )
            // InternalRunstar.g:2721:1: (otherlv_2= RULE_ID )
            {
            // InternalRunstar.g:2721:1: (otherlv_2= RULE_ID )
            // InternalRunstar.g:2722:3: otherlv_2= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getStackRule());
            	        }
                    
            otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_43); 

            		newLeafNode(otherlv_2, grammarAccess.getStackAccess().getTextTextVarCrossReference_2_0()); 
            	

            }


            }

            // InternalRunstar.g:2733:2: ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) ) ) ) )* ) ) )
            // InternalRunstar.g:2735:1: ( ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) ) ) ) )* ) )
            {
            // InternalRunstar.g:2735:1: ( ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) ) ) ) )* ) )
            // InternalRunstar.g:2736:2: ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) ) ) ) )* )
            {
             
            	  getUnorderedGroupHelper().enter(grammarAccess.getStackAccess().getUnorderedGroup_3());
            	
            // InternalRunstar.g:2739:2: ( ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) ) ) ) )* )
            // InternalRunstar.g:2740:3: ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) ) ) ) )*
            {
            // InternalRunstar.g:2740:3: ( ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) ) ) ) )*
            loop26:
            do {
                int alt26=7;
                int LA26_0 = input.LA(1);

                if ( LA26_0 == 22 && ( getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 2) || getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 3) || getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 4) || getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 5) || getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 0) || getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 1) ) ) {
                    int LA26_2 = input.LA(2);

                    if ( LA26_2 == 74 && getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 0) ) {
                        alt26=1;
                    }
                    else if ( LA26_2 == 75 && getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 1) ) {
                        alt26=2;
                    }
                    else if ( LA26_2 == 76 && getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 2) ) {
                        alt26=3;
                    }
                    else if ( LA26_2 == 77 && getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 3) ) {
                        alt26=4;
                    }
                    else if ( LA26_2 == 32 && getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 4) ) {
                        alt26=5;
                    }
                    else if ( LA26_2 == 71 && getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 5) ) {
                        alt26=6;
                    }


                }


                switch (alt26) {
            	case 1 :
            	    // InternalRunstar.g:2742:4: ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) ) ) )
            	    {
            	    // InternalRunstar.g:2742:4: ({...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) ) ) )
            	    // InternalRunstar.g:2743:5: {...}? => ( ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleStack", "getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 0)");
            	    }
            	    // InternalRunstar.g:2743:102: ( ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) ) )
            	    // InternalRunstar.g:2744:6: ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getStackAccess().getUnorderedGroup_3(), 0);
            	    	 				
            	    // InternalRunstar.g:2747:6: ({...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) ) )
            	    // InternalRunstar.g:2747:7: {...}? => (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleStack", "true");
            	    }
            	    // InternalRunstar.g:2747:16: (otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) ) )
            	    // InternalRunstar.g:2747:18: otherlv_4= ',' otherlv_5= 'length' otherlv_6= '=' ( (lv_maxLength_7_0= RULE_INT ) )
            	    {
            	    otherlv_4=(Token)match(input,22,FollowSets000.FOLLOW_55); 

            	        	newLeafNode(otherlv_4, grammarAccess.getStackAccess().getCommaKeyword_3_0_0());
            	        
            	    otherlv_5=(Token)match(input,74,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_5, grammarAccess.getStackAccess().getLengthKeyword_3_0_1());
            	        
            	    otherlv_6=(Token)match(input,26,FollowSets000.FOLLOW_26); 

            	        	newLeafNode(otherlv_6, grammarAccess.getStackAccess().getEqualsSignKeyword_3_0_2());
            	        
            	    // InternalRunstar.g:2759:1: ( (lv_maxLength_7_0= RULE_INT ) )
            	    // InternalRunstar.g:2760:1: (lv_maxLength_7_0= RULE_INT )
            	    {
            	    // InternalRunstar.g:2760:1: (lv_maxLength_7_0= RULE_INT )
            	    // InternalRunstar.g:2761:3: lv_maxLength_7_0= RULE_INT
            	    {
            	    lv_maxLength_7_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_43); 

            	    			newLeafNode(lv_maxLength_7_0, grammarAccess.getStackAccess().getMaxLengthINTTerminalRuleCall_3_0_3_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getStackRule());
            	    	        }
            	           		setWithLastConsumed(
            	           			current, 
            	           			"maxLength",
            	            		lv_maxLength_7_0, 
            	            		"org.eclipse.xtext.common.Terminals.INT");
            	    	    

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getStackAccess().getUnorderedGroup_3());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalRunstar.g:2784:4: ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) ) ) )
            	    {
            	    // InternalRunstar.g:2784:4: ({...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) ) ) )
            	    // InternalRunstar.g:2785:5: {...}? => ( ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleStack", "getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 1)");
            	    }
            	    // InternalRunstar.g:2785:102: ( ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) ) )
            	    // InternalRunstar.g:2786:6: ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getStackAccess().getUnorderedGroup_3(), 1);
            	    	 				
            	    // InternalRunstar.g:2789:6: ({...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) ) )
            	    // InternalRunstar.g:2789:7: {...}? => (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleStack", "true");
            	    }
            	    // InternalRunstar.g:2789:16: (otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) ) )
            	    // InternalRunstar.g:2789:18: otherlv_8= ',' otherlv_9= 'vertical' otherlv_10= '=' ( (lv_isVertical_11_0= ruleRBoolean ) )
            	    {
            	    otherlv_8=(Token)match(input,22,FollowSets000.FOLLOW_56); 

            	        	newLeafNode(otherlv_8, grammarAccess.getStackAccess().getCommaKeyword_3_1_0());
            	        
            	    otherlv_9=(Token)match(input,75,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_9, grammarAccess.getStackAccess().getVerticalKeyword_3_1_1());
            	        
            	    otherlv_10=(Token)match(input,26,FollowSets000.FOLLOW_45); 

            	        	newLeafNode(otherlv_10, grammarAccess.getStackAccess().getEqualsSignKeyword_3_1_2());
            	        
            	    // InternalRunstar.g:2801:1: ( (lv_isVertical_11_0= ruleRBoolean ) )
            	    // InternalRunstar.g:2802:1: (lv_isVertical_11_0= ruleRBoolean )
            	    {
            	    // InternalRunstar.g:2802:1: (lv_isVertical_11_0= ruleRBoolean )
            	    // InternalRunstar.g:2803:3: lv_isVertical_11_0= ruleRBoolean
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getStackAccess().getIsVerticalRBooleanEnumRuleCall_3_1_3_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_43);
            	    lv_isVertical_11_0=ruleRBoolean();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getStackRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"isVertical",
            	            		lv_isVertical_11_0, 
            	            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.RBoolean");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getStackAccess().getUnorderedGroup_3());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalRunstar.g:2826:4: ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) ) ) )
            	    {
            	    // InternalRunstar.g:2826:4: ({...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) ) ) )
            	    // InternalRunstar.g:2827:5: {...}? => ( ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleStack", "getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 2)");
            	    }
            	    // InternalRunstar.g:2827:102: ( ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) ) )
            	    // InternalRunstar.g:2828:6: ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getStackAccess().getUnorderedGroup_3(), 2);
            	    	 				
            	    // InternalRunstar.g:2831:6: ({...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) ) )
            	    // InternalRunstar.g:2831:7: {...}? => (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleStack", "true");
            	    }
            	    // InternalRunstar.g:2831:16: (otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) ) )
            	    // InternalRunstar.g:2831:18: otherlv_12= ',' otherlv_13= 'shape' otherlv_14= '=' ( (lv_shape_15_0= ruleStackShape ) )
            	    {
            	    otherlv_12=(Token)match(input,22,FollowSets000.FOLLOW_57); 

            	        	newLeafNode(otherlv_12, grammarAccess.getStackAccess().getCommaKeyword_3_2_0());
            	        
            	    otherlv_13=(Token)match(input,76,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_13, grammarAccess.getStackAccess().getShapeKeyword_3_2_1());
            	        
            	    otherlv_14=(Token)match(input,26,FollowSets000.FOLLOW_58); 

            	        	newLeafNode(otherlv_14, grammarAccess.getStackAccess().getEqualsSignKeyword_3_2_2());
            	        
            	    // InternalRunstar.g:2843:1: ( (lv_shape_15_0= ruleStackShape ) )
            	    // InternalRunstar.g:2844:1: (lv_shape_15_0= ruleStackShape )
            	    {
            	    // InternalRunstar.g:2844:1: (lv_shape_15_0= ruleStackShape )
            	    // InternalRunstar.g:2845:3: lv_shape_15_0= ruleStackShape
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getStackAccess().getShapeStackShapeEnumRuleCall_3_2_3_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_43);
            	    lv_shape_15_0=ruleStackShape();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getStackRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"shape",
            	            		lv_shape_15_0, 
            	            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.StackShape");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getStackAccess().getUnorderedGroup_3());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalRunstar.g:2868:4: ({...}? => ( ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) ) ) )
            	    {
            	    // InternalRunstar.g:2868:4: ({...}? => ( ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) ) ) )
            	    // InternalRunstar.g:2869:5: {...}? => ( ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 3) ) {
            	        throw new FailedPredicateException(input, "ruleStack", "getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 3)");
            	    }
            	    // InternalRunstar.g:2869:102: ( ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) ) )
            	    // InternalRunstar.g:2870:6: ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getStackAccess().getUnorderedGroup_3(), 3);
            	    	 				
            	    // InternalRunstar.g:2873:6: ({...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) ) )
            	    // InternalRunstar.g:2873:7: {...}? => (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleStack", "true");
            	    }
            	    // InternalRunstar.g:2873:16: (otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) ) )
            	    // InternalRunstar.g:2873:18: otherlv_16= ',' otherlv_17= 'strategy' otherlv_18= '=' ( (lv_strategy_19_0= ruleStackStrategy ) )
            	    {
            	    otherlv_16=(Token)match(input,22,FollowSets000.FOLLOW_59); 

            	        	newLeafNode(otherlv_16, grammarAccess.getStackAccess().getCommaKeyword_3_3_0());
            	        
            	    otherlv_17=(Token)match(input,77,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_17, grammarAccess.getStackAccess().getStrategyKeyword_3_3_1());
            	        
            	    otherlv_18=(Token)match(input,26,FollowSets000.FOLLOW_60); 

            	        	newLeafNode(otherlv_18, grammarAccess.getStackAccess().getEqualsSignKeyword_3_3_2());
            	        
            	    // InternalRunstar.g:2885:1: ( (lv_strategy_19_0= ruleStackStrategy ) )
            	    // InternalRunstar.g:2886:1: (lv_strategy_19_0= ruleStackStrategy )
            	    {
            	    // InternalRunstar.g:2886:1: (lv_strategy_19_0= ruleStackStrategy )
            	    // InternalRunstar.g:2887:3: lv_strategy_19_0= ruleStackStrategy
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getStackAccess().getStrategyStackStrategyEnumRuleCall_3_3_3_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_43);
            	    lv_strategy_19_0=ruleStackStrategy();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getStackRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"strategy",
            	            		lv_strategy_19_0, 
            	            		"org.eclipse.gemoc.execution.moccml.runstar.xtext.Runstar.StackStrategy");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getStackAccess().getUnorderedGroup_3());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 5 :
            	    // InternalRunstar.g:2910:4: ({...}? => ( ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalRunstar.g:2910:4: ({...}? => ( ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) ) ) )
            	    // InternalRunstar.g:2911:5: {...}? => ( ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 4) ) {
            	        throw new FailedPredicateException(input, "ruleStack", "getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 4)");
            	    }
            	    // InternalRunstar.g:2911:102: ( ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) ) )
            	    // InternalRunstar.g:2912:6: ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getStackAccess().getUnorderedGroup_3(), 4);
            	    	 				
            	    // InternalRunstar.g:2915:6: ({...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) ) )
            	    // InternalRunstar.g:2915:7: {...}? => (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleStack", "true");
            	    }
            	    // InternalRunstar.g:2915:16: (otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) ) )
            	    // InternalRunstar.g:2915:18: otherlv_20= ',' otherlv_21= 'title' otherlv_22= '=' ( (lv_title_23_0= RULE_STRING ) )
            	    {
            	    otherlv_20=(Token)match(input,22,FollowSets000.FOLLOW_61); 

            	        	newLeafNode(otherlv_20, grammarAccess.getStackAccess().getCommaKeyword_3_4_0());
            	        
            	    otherlv_21=(Token)match(input,32,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_21, grammarAccess.getStackAccess().getTitleKeyword_3_4_1());
            	        
            	    otherlv_22=(Token)match(input,26,FollowSets000.FOLLOW_11); 

            	        	newLeafNode(otherlv_22, grammarAccess.getStackAccess().getEqualsSignKeyword_3_4_2());
            	        
            	    // InternalRunstar.g:2927:1: ( (lv_title_23_0= RULE_STRING ) )
            	    // InternalRunstar.g:2928:1: (lv_title_23_0= RULE_STRING )
            	    {
            	    // InternalRunstar.g:2928:1: (lv_title_23_0= RULE_STRING )
            	    // InternalRunstar.g:2929:3: lv_title_23_0= RULE_STRING
            	    {
            	    lv_title_23_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_43); 

            	    			newLeafNode(lv_title_23_0, grammarAccess.getStackAccess().getTitleSTRINGTerminalRuleCall_3_4_3_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getStackRule());
            	    	        }
            	           		setWithLastConsumed(
            	           			current, 
            	           			"title",
            	            		lv_title_23_0, 
            	            		"org.eclipse.xtext.common.Terminals.STRING");
            	    	    

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getStackAccess().getUnorderedGroup_3());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 6 :
            	    // InternalRunstar.g:2952:4: ({...}? => ( ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) ) ) )
            	    {
            	    // InternalRunstar.g:2952:4: ({...}? => ( ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) ) ) )
            	    // InternalRunstar.g:2953:5: {...}? => ( ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 5) ) {
            	        throw new FailedPredicateException(input, "ruleStack", "getUnorderedGroupHelper().canSelect(grammarAccess.getStackAccess().getUnorderedGroup_3(), 5)");
            	    }
            	    // InternalRunstar.g:2953:102: ( ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) ) )
            	    // InternalRunstar.g:2954:6: ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getStackAccess().getUnorderedGroup_3(), 5);
            	    	 				
            	    // InternalRunstar.g:2957:6: ({...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) ) )
            	    // InternalRunstar.g:2957:7: {...}? => (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleStack", "true");
            	    }
            	    // InternalRunstar.g:2957:16: (otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) ) )
            	    // InternalRunstar.g:2957:18: otherlv_24= ',' otherlv_25= 'depth' otherlv_26= '=' ( (lv_depth_27_0= RULE_INT ) )
            	    {
            	    otherlv_24=(Token)match(input,22,FollowSets000.FOLLOW_53); 

            	        	newLeafNode(otherlv_24, grammarAccess.getStackAccess().getCommaKeyword_3_5_0());
            	        
            	    otherlv_25=(Token)match(input,71,FollowSets000.FOLLOW_18); 

            	        	newLeafNode(otherlv_25, grammarAccess.getStackAccess().getDepthKeyword_3_5_1());
            	        
            	    otherlv_26=(Token)match(input,26,FollowSets000.FOLLOW_26); 

            	        	newLeafNode(otherlv_26, grammarAccess.getStackAccess().getEqualsSignKeyword_3_5_2());
            	        
            	    // InternalRunstar.g:2969:1: ( (lv_depth_27_0= RULE_INT ) )
            	    // InternalRunstar.g:2970:1: (lv_depth_27_0= RULE_INT )
            	    {
            	    // InternalRunstar.g:2970:1: (lv_depth_27_0= RULE_INT )
            	    // InternalRunstar.g:2971:3: lv_depth_27_0= RULE_INT
            	    {
            	    lv_depth_27_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_43); 

            	    			newLeafNode(lv_depth_27_0, grammarAccess.getStackAccess().getDepthINTTerminalRuleCall_3_5_3_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getStackRule());
            	    	        }
            	           		setWithLastConsumed(
            	           			current, 
            	           			"depth",
            	            		lv_depth_27_0, 
            	            		"org.eclipse.xtext.common.Terminals.INT");
            	    	    

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getStackAccess().getUnorderedGroup_3());
            	    	 				

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);


            }


            }

             
            	  getUnorderedGroupHelper().leave(grammarAccess.getStackAccess().getUnorderedGroup_3());
            	

            }

            otherlv_28=(Token)match(input,19,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_28, grammarAccess.getStackAccess().getRightParenthesisKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStack"


    // $ANTLR start "entryRuleRColor"
    // InternalRunstar.g:3013:1: entryRuleRColor returns [EObject current=null] : iv_ruleRColor= ruleRColor EOF ;
    public final EObject entryRuleRColor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRColor = null;


        try {
            // InternalRunstar.g:3014:2: (iv_ruleRColor= ruleRColor EOF )
            // InternalRunstar.g:3015:2: iv_ruleRColor= ruleRColor EOF
            {
             newCompositeNode(grammarAccess.getRColorRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRColor=ruleRColor();

            state._fsp--;

             current =iv_ruleRColor; 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRColor"


    // $ANTLR start "ruleRColor"
    // InternalRunstar.g:3022:1: ruleRColor returns [EObject current=null] : (otherlv_0= '(' ( (lv_red_1_0= RULE_INT ) ) otherlv_2= ',' ( (lv_green_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_blue_5_0= RULE_INT ) ) otherlv_6= ')' ) ;
    public final EObject ruleRColor() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_red_1_0=null;
        Token otherlv_2=null;
        Token lv_green_3_0=null;
        Token otherlv_4=null;
        Token lv_blue_5_0=null;
        Token otherlv_6=null;

         enterRule(); 
            
        try {
            // InternalRunstar.g:3025:28: ( (otherlv_0= '(' ( (lv_red_1_0= RULE_INT ) ) otherlv_2= ',' ( (lv_green_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_blue_5_0= RULE_INT ) ) otherlv_6= ')' ) )
            // InternalRunstar.g:3026:1: (otherlv_0= '(' ( (lv_red_1_0= RULE_INT ) ) otherlv_2= ',' ( (lv_green_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_blue_5_0= RULE_INT ) ) otherlv_6= ')' )
            {
            // InternalRunstar.g:3026:1: (otherlv_0= '(' ( (lv_red_1_0= RULE_INT ) ) otherlv_2= ',' ( (lv_green_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_blue_5_0= RULE_INT ) ) otherlv_6= ')' )
            // InternalRunstar.g:3026:3: otherlv_0= '(' ( (lv_red_1_0= RULE_INT ) ) otherlv_2= ',' ( (lv_green_3_0= RULE_INT ) ) otherlv_4= ',' ( (lv_blue_5_0= RULE_INT ) ) otherlv_6= ')'
            {
            otherlv_0=(Token)match(input,18,FollowSets000.FOLLOW_26); 

                	newLeafNode(otherlv_0, grammarAccess.getRColorAccess().getLeftParenthesisKeyword_0());
                
            // InternalRunstar.g:3030:1: ( (lv_red_1_0= RULE_INT ) )
            // InternalRunstar.g:3031:1: (lv_red_1_0= RULE_INT )
            {
            // InternalRunstar.g:3031:1: (lv_red_1_0= RULE_INT )
            // InternalRunstar.g:3032:3: lv_red_1_0= RULE_INT
            {
            lv_red_1_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_19); 

            			newLeafNode(lv_red_1_0, grammarAccess.getRColorAccess().getRedINTTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRColorRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"red",
                    		lv_red_1_0, 
                    		"org.eclipse.xtext.common.Terminals.INT");
            	    

            }


            }

            otherlv_2=(Token)match(input,22,FollowSets000.FOLLOW_26); 

                	newLeafNode(otherlv_2, grammarAccess.getRColorAccess().getCommaKeyword_2());
                
            // InternalRunstar.g:3052:1: ( (lv_green_3_0= RULE_INT ) )
            // InternalRunstar.g:3053:1: (lv_green_3_0= RULE_INT )
            {
            // InternalRunstar.g:3053:1: (lv_green_3_0= RULE_INT )
            // InternalRunstar.g:3054:3: lv_green_3_0= RULE_INT
            {
            lv_green_3_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_19); 

            			newLeafNode(lv_green_3_0, grammarAccess.getRColorAccess().getGreenINTTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRColorRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"green",
                    		lv_green_3_0, 
                    		"org.eclipse.xtext.common.Terminals.INT");
            	    

            }


            }

            otherlv_4=(Token)match(input,22,FollowSets000.FOLLOW_26); 

                	newLeafNode(otherlv_4, grammarAccess.getRColorAccess().getCommaKeyword_4());
                
            // InternalRunstar.g:3074:1: ( (lv_blue_5_0= RULE_INT ) )
            // InternalRunstar.g:3075:1: (lv_blue_5_0= RULE_INT )
            {
            // InternalRunstar.g:3075:1: (lv_blue_5_0= RULE_INT )
            // InternalRunstar.g:3076:3: lv_blue_5_0= RULE_INT
            {
            lv_blue_5_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_13); 

            			newLeafNode(lv_blue_5_0, grammarAccess.getRColorAccess().getBlueINTTerminalRuleCall_5_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRColorRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"blue",
                    		lv_blue_5_0, 
                    		"org.eclipse.xtext.common.Terminals.INT");
            	    

            }


            }

            otherlv_6=(Token)match(input,19,FollowSets000.FOLLOW_2); 

                	newLeafNode(otherlv_6, grammarAccess.getRColorAccess().getRightParenthesisKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRColor"


    // $ANTLR start "entryRuleEString"
    // InternalRunstar.g:3104:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalRunstar.g:3105:2: (iv_ruleEString= ruleEString EOF )
            // InternalRunstar.g:3106:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalRunstar.g:3113:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;

         enterRule(); 
            
        try {
            // InternalRunstar.g:3116:28: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalRunstar.g:3117:1: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalRunstar.g:3117:1: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==RULE_STRING) ) {
                alt27=1;
            }
            else if ( (LA27_0==RULE_ID) ) {
                alt27=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }
            switch (alt27) {
                case 1 :
                    // InternalRunstar.g:3117:6: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); 

                    		current.merge(this_STRING_0);
                        
                     
                        newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                        

                    }
                    break;
                case 2 :
                    // InternalRunstar.g:3125:10: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); 

                    		current.merge(this_ID_1);
                        
                     
                        newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "ruleFontStyle"
    // InternalRunstar.g:3140:1: ruleFontStyle returns [Enumerator current=null] : ( (enumLiteral_0= 'NONE' ) | (enumLiteral_1= 'BOLD' ) | (enumLiteral_2= 'ITALIC' ) ) ;
    public final Enumerator ruleFontStyle() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;

         enterRule(); 
        try {
            // InternalRunstar.g:3142:28: ( ( (enumLiteral_0= 'NONE' ) | (enumLiteral_1= 'BOLD' ) | (enumLiteral_2= 'ITALIC' ) ) )
            // InternalRunstar.g:3143:1: ( (enumLiteral_0= 'NONE' ) | (enumLiteral_1= 'BOLD' ) | (enumLiteral_2= 'ITALIC' ) )
            {
            // InternalRunstar.g:3143:1: ( (enumLiteral_0= 'NONE' ) | (enumLiteral_1= 'BOLD' ) | (enumLiteral_2= 'ITALIC' ) )
            int alt28=3;
            switch ( input.LA(1) ) {
            case 78:
                {
                alt28=1;
                }
                break;
            case 79:
                {
                alt28=2;
                }
                break;
            case 80:
                {
                alt28=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }

            switch (alt28) {
                case 1 :
                    // InternalRunstar.g:3143:2: (enumLiteral_0= 'NONE' )
                    {
                    // InternalRunstar.g:3143:2: (enumLiteral_0= 'NONE' )
                    // InternalRunstar.g:3143:4: enumLiteral_0= 'NONE'
                    {
                    enumLiteral_0=(Token)match(input,78,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getFontStyleAccess().getNONEEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getFontStyleAccess().getNONEEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // InternalRunstar.g:3149:6: (enumLiteral_1= 'BOLD' )
                    {
                    // InternalRunstar.g:3149:6: (enumLiteral_1= 'BOLD' )
                    // InternalRunstar.g:3149:8: enumLiteral_1= 'BOLD'
                    {
                    enumLiteral_1=(Token)match(input,79,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getFontStyleAccess().getBOLDEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getFontStyleAccess().getBOLDEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // InternalRunstar.g:3155:6: (enumLiteral_2= 'ITALIC' )
                    {
                    // InternalRunstar.g:3155:6: (enumLiteral_2= 'ITALIC' )
                    // InternalRunstar.g:3155:8: enumLiteral_2= 'ITALIC'
                    {
                    enumLiteral_2=(Token)match(input,80,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getFontStyleAccess().getITALICEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getFontStyleAccess().getITALICEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFontStyle"


    // $ANTLR start "rulePaintType"
    // InternalRunstar.g:3165:1: rulePaintType returns [Enumerator current=null] : ( (enumLiteral_0= 'NONE' ) | (enumLiteral_1= 'BACKGROUND' ) | (enumLiteral_2= 'FOREGROUND' ) ) ;
    public final Enumerator rulePaintType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;

         enterRule(); 
        try {
            // InternalRunstar.g:3167:28: ( ( (enumLiteral_0= 'NONE' ) | (enumLiteral_1= 'BACKGROUND' ) | (enumLiteral_2= 'FOREGROUND' ) ) )
            // InternalRunstar.g:3168:1: ( (enumLiteral_0= 'NONE' ) | (enumLiteral_1= 'BACKGROUND' ) | (enumLiteral_2= 'FOREGROUND' ) )
            {
            // InternalRunstar.g:3168:1: ( (enumLiteral_0= 'NONE' ) | (enumLiteral_1= 'BACKGROUND' ) | (enumLiteral_2= 'FOREGROUND' ) )
            int alt29=3;
            switch ( input.LA(1) ) {
            case 78:
                {
                alt29=1;
                }
                break;
            case 81:
                {
                alt29=2;
                }
                break;
            case 82:
                {
                alt29=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }

            switch (alt29) {
                case 1 :
                    // InternalRunstar.g:3168:2: (enumLiteral_0= 'NONE' )
                    {
                    // InternalRunstar.g:3168:2: (enumLiteral_0= 'NONE' )
                    // InternalRunstar.g:3168:4: enumLiteral_0= 'NONE'
                    {
                    enumLiteral_0=(Token)match(input,78,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getPaintTypeAccess().getNONEEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getPaintTypeAccess().getNONEEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // InternalRunstar.g:3174:6: (enumLiteral_1= 'BACKGROUND' )
                    {
                    // InternalRunstar.g:3174:6: (enumLiteral_1= 'BACKGROUND' )
                    // InternalRunstar.g:3174:8: enumLiteral_1= 'BACKGROUND'
                    {
                    enumLiteral_1=(Token)match(input,81,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getPaintTypeAccess().getBACKGROUNDEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getPaintTypeAccess().getBACKGROUNDEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // InternalRunstar.g:3180:6: (enumLiteral_2= 'FOREGROUND' )
                    {
                    // InternalRunstar.g:3180:6: (enumLiteral_2= 'FOREGROUND' )
                    // InternalRunstar.g:3180:8: enumLiteral_2= 'FOREGROUND'
                    {
                    enumLiteral_2=(Token)match(input,82,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getPaintTypeAccess().getFOREGROUNDEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getPaintTypeAccess().getFOREGROUNDEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePaintType"


    // $ANTLR start "ruleMarkerType"
    // InternalRunstar.g:3190:1: ruleMarkerType returns [Enumerator current=null] : ( (enumLiteral_0= 'INFORMATION' ) | (enumLiteral_1= 'WARNING' ) | (enumLiteral_2= 'ERROR' ) ) ;
    public final Enumerator ruleMarkerType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;

         enterRule(); 
        try {
            // InternalRunstar.g:3192:28: ( ( (enumLiteral_0= 'INFORMATION' ) | (enumLiteral_1= 'WARNING' ) | (enumLiteral_2= 'ERROR' ) ) )
            // InternalRunstar.g:3193:1: ( (enumLiteral_0= 'INFORMATION' ) | (enumLiteral_1= 'WARNING' ) | (enumLiteral_2= 'ERROR' ) )
            {
            // InternalRunstar.g:3193:1: ( (enumLiteral_0= 'INFORMATION' ) | (enumLiteral_1= 'WARNING' ) | (enumLiteral_2= 'ERROR' ) )
            int alt30=3;
            switch ( input.LA(1) ) {
            case 83:
                {
                alt30=1;
                }
                break;
            case 84:
                {
                alt30=2;
                }
                break;
            case 85:
                {
                alt30=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }

            switch (alt30) {
                case 1 :
                    // InternalRunstar.g:3193:2: (enumLiteral_0= 'INFORMATION' )
                    {
                    // InternalRunstar.g:3193:2: (enumLiteral_0= 'INFORMATION' )
                    // InternalRunstar.g:3193:4: enumLiteral_0= 'INFORMATION'
                    {
                    enumLiteral_0=(Token)match(input,83,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getMarkerTypeAccess().getINFORMATIONEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getMarkerTypeAccess().getINFORMATIONEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // InternalRunstar.g:3199:6: (enumLiteral_1= 'WARNING' )
                    {
                    // InternalRunstar.g:3199:6: (enumLiteral_1= 'WARNING' )
                    // InternalRunstar.g:3199:8: enumLiteral_1= 'WARNING'
                    {
                    enumLiteral_1=(Token)match(input,84,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getMarkerTypeAccess().getWARNINGEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getMarkerTypeAccess().getWARNINGEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // InternalRunstar.g:3205:6: (enumLiteral_2= 'ERROR' )
                    {
                    // InternalRunstar.g:3205:6: (enumLiteral_2= 'ERROR' )
                    // InternalRunstar.g:3205:8: enumLiteral_2= 'ERROR'
                    {
                    enumLiteral_2=(Token)match(input,85,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getMarkerTypeAccess().getERROREnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getMarkerTypeAccess().getERROREnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMarkerType"


    // $ANTLR start "ruleStepExecution"
    // InternalRunstar.g:3215:1: ruleStepExecution returns [Enumerator current=null] : ( (enumLiteral_0= 'AFTER' ) | (enumLiteral_1= 'BEFORE' ) ) ;
    public final Enumerator ruleStepExecution() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // InternalRunstar.g:3217:28: ( ( (enumLiteral_0= 'AFTER' ) | (enumLiteral_1= 'BEFORE' ) ) )
            // InternalRunstar.g:3218:1: ( (enumLiteral_0= 'AFTER' ) | (enumLiteral_1= 'BEFORE' ) )
            {
            // InternalRunstar.g:3218:1: ( (enumLiteral_0= 'AFTER' ) | (enumLiteral_1= 'BEFORE' ) )
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==86) ) {
                alt31=1;
            }
            else if ( (LA31_0==87) ) {
                alt31=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }
            switch (alt31) {
                case 1 :
                    // InternalRunstar.g:3218:2: (enumLiteral_0= 'AFTER' )
                    {
                    // InternalRunstar.g:3218:2: (enumLiteral_0= 'AFTER' )
                    // InternalRunstar.g:3218:4: enumLiteral_0= 'AFTER'
                    {
                    enumLiteral_0=(Token)match(input,86,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getStepExecutionAccess().getAFTEREnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getStepExecutionAccess().getAFTEREnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // InternalRunstar.g:3224:6: (enumLiteral_1= 'BEFORE' )
                    {
                    // InternalRunstar.g:3224:6: (enumLiteral_1= 'BEFORE' )
                    // InternalRunstar.g:3224:8: enumLiteral_1= 'BEFORE'
                    {
                    enumLiteral_1=(Token)match(input,87,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getStepExecutionAccess().getBEFOREEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getStepExecutionAccess().getBEFOREEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStepExecution"


    // $ANTLR start "ruleStackShape"
    // InternalRunstar.g:3234:1: ruleStackShape returns [Enumerator current=null] : ( (enumLiteral_0= 'RECTANGLE' ) | (enumLiteral_1= 'ROUNDED_RECTANGLE' ) | (enumLiteral_2= 'ELLIPSE' ) ) ;
    public final Enumerator ruleStackShape() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;

         enterRule(); 
        try {
            // InternalRunstar.g:3236:28: ( ( (enumLiteral_0= 'RECTANGLE' ) | (enumLiteral_1= 'ROUNDED_RECTANGLE' ) | (enumLiteral_2= 'ELLIPSE' ) ) )
            // InternalRunstar.g:3237:1: ( (enumLiteral_0= 'RECTANGLE' ) | (enumLiteral_1= 'ROUNDED_RECTANGLE' ) | (enumLiteral_2= 'ELLIPSE' ) )
            {
            // InternalRunstar.g:3237:1: ( (enumLiteral_0= 'RECTANGLE' ) | (enumLiteral_1= 'ROUNDED_RECTANGLE' ) | (enumLiteral_2= 'ELLIPSE' ) )
            int alt32=3;
            switch ( input.LA(1) ) {
            case 88:
                {
                alt32=1;
                }
                break;
            case 89:
                {
                alt32=2;
                }
                break;
            case 90:
                {
                alt32=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;
            }

            switch (alt32) {
                case 1 :
                    // InternalRunstar.g:3237:2: (enumLiteral_0= 'RECTANGLE' )
                    {
                    // InternalRunstar.g:3237:2: (enumLiteral_0= 'RECTANGLE' )
                    // InternalRunstar.g:3237:4: enumLiteral_0= 'RECTANGLE'
                    {
                    enumLiteral_0=(Token)match(input,88,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getStackShapeAccess().getRECTANGLEEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getStackShapeAccess().getRECTANGLEEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // InternalRunstar.g:3243:6: (enumLiteral_1= 'ROUNDED_RECTANGLE' )
                    {
                    // InternalRunstar.g:3243:6: (enumLiteral_1= 'ROUNDED_RECTANGLE' )
                    // InternalRunstar.g:3243:8: enumLiteral_1= 'ROUNDED_RECTANGLE'
                    {
                    enumLiteral_1=(Token)match(input,89,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getStackShapeAccess().getROUNDED_RECTANGLEEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getStackShapeAccess().getROUNDED_RECTANGLEEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // InternalRunstar.g:3249:6: (enumLiteral_2= 'ELLIPSE' )
                    {
                    // InternalRunstar.g:3249:6: (enumLiteral_2= 'ELLIPSE' )
                    // InternalRunstar.g:3249:8: enumLiteral_2= 'ELLIPSE'
                    {
                    enumLiteral_2=(Token)match(input,90,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getStackShapeAccess().getELLIPSEEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getStackShapeAccess().getELLIPSEEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStackShape"


    // $ANTLR start "ruleStackStrategy"
    // InternalRunstar.g:3259:1: ruleStackStrategy returns [Enumerator current=null] : ( (enumLiteral_0= 'LASTS' ) | (enumLiteral_1= 'FIRSTS' ) ) ;
    public final Enumerator ruleStackStrategy() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // InternalRunstar.g:3261:28: ( ( (enumLiteral_0= 'LASTS' ) | (enumLiteral_1= 'FIRSTS' ) ) )
            // InternalRunstar.g:3262:1: ( (enumLiteral_0= 'LASTS' ) | (enumLiteral_1= 'FIRSTS' ) )
            {
            // InternalRunstar.g:3262:1: ( (enumLiteral_0= 'LASTS' ) | (enumLiteral_1= 'FIRSTS' ) )
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==91) ) {
                alt33=1;
            }
            else if ( (LA33_0==92) ) {
                alt33=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }
            switch (alt33) {
                case 1 :
                    // InternalRunstar.g:3262:2: (enumLiteral_0= 'LASTS' )
                    {
                    // InternalRunstar.g:3262:2: (enumLiteral_0= 'LASTS' )
                    // InternalRunstar.g:3262:4: enumLiteral_0= 'LASTS'
                    {
                    enumLiteral_0=(Token)match(input,91,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getStackStrategyAccess().getLASTSEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getStackStrategyAccess().getLASTSEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // InternalRunstar.g:3268:6: (enumLiteral_1= 'FIRSTS' )
                    {
                    // InternalRunstar.g:3268:6: (enumLiteral_1= 'FIRSTS' )
                    // InternalRunstar.g:3268:8: enumLiteral_1= 'FIRSTS'
                    {
                    enumLiteral_1=(Token)match(input,92,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getStackStrategyAccess().getFIRSTSEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getStackStrategyAccess().getFIRSTSEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStackStrategy"


    // $ANTLR start "ruleLineStyle"
    // InternalRunstar.g:3278:1: ruleLineStyle returns [Enumerator current=null] : ( (enumLiteral_0= 'DIGITAL' ) | (enumLiteral_1= 'ANALOG' ) ) ;
    public final Enumerator ruleLineStyle() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // InternalRunstar.g:3280:28: ( ( (enumLiteral_0= 'DIGITAL' ) | (enumLiteral_1= 'ANALOG' ) ) )
            // InternalRunstar.g:3281:1: ( (enumLiteral_0= 'DIGITAL' ) | (enumLiteral_1= 'ANALOG' ) )
            {
            // InternalRunstar.g:3281:1: ( (enumLiteral_0= 'DIGITAL' ) | (enumLiteral_1= 'ANALOG' ) )
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==93) ) {
                alt34=1;
            }
            else if ( (LA34_0==94) ) {
                alt34=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 34, 0, input);

                throw nvae;
            }
            switch (alt34) {
                case 1 :
                    // InternalRunstar.g:3281:2: (enumLiteral_0= 'DIGITAL' )
                    {
                    // InternalRunstar.g:3281:2: (enumLiteral_0= 'DIGITAL' )
                    // InternalRunstar.g:3281:4: enumLiteral_0= 'DIGITAL'
                    {
                    enumLiteral_0=(Token)match(input,93,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getLineStyleAccess().getDIGITALEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getLineStyleAccess().getDIGITALEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // InternalRunstar.g:3287:6: (enumLiteral_1= 'ANALOG' )
                    {
                    // InternalRunstar.g:3287:6: (enumLiteral_1= 'ANALOG' )
                    // InternalRunstar.g:3287:8: enumLiteral_1= 'ANALOG'
                    {
                    enumLiteral_1=(Token)match(input,94,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getLineStyleAccess().getANALOGEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getLineStyleAccess().getANALOGEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLineStyle"


    // $ANTLR start "ruleRBoolean"
    // InternalRunstar.g:3297:1: ruleRBoolean returns [Enumerator current=null] : ( (enumLiteral_0= 'FALSE' ) | (enumLiteral_1= 'TRUE' ) ) ;
    public final Enumerator ruleRBoolean() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // InternalRunstar.g:3299:28: ( ( (enumLiteral_0= 'FALSE' ) | (enumLiteral_1= 'TRUE' ) ) )
            // InternalRunstar.g:3300:1: ( (enumLiteral_0= 'FALSE' ) | (enumLiteral_1= 'TRUE' ) )
            {
            // InternalRunstar.g:3300:1: ( (enumLiteral_0= 'FALSE' ) | (enumLiteral_1= 'TRUE' ) )
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==95) ) {
                alt35=1;
            }
            else if ( (LA35_0==96) ) {
                alt35=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }
            switch (alt35) {
                case 1 :
                    // InternalRunstar.g:3300:2: (enumLiteral_0= 'FALSE' )
                    {
                    // InternalRunstar.g:3300:2: (enumLiteral_0= 'FALSE' )
                    // InternalRunstar.g:3300:4: enumLiteral_0= 'FALSE'
                    {
                    enumLiteral_0=(Token)match(input,95,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getRBooleanAccess().getFALSEEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getRBooleanAccess().getFALSEEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // InternalRunstar.g:3306:6: (enumLiteral_1= 'TRUE' )
                    {
                    // InternalRunstar.g:3306:6: (enumLiteral_1= 'TRUE' )
                    // InternalRunstar.g:3306:8: enumLiteral_1= 'TRUE'
                    {
                    enumLiteral_1=(Token)match(input,96,FollowSets000.FOLLOW_2); 

                            current = grammarAccess.getRBooleanAccess().getTRUEEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getRBooleanAccess().getTRUEEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRBoolean"

    // Delegated rules


    protected DFA12 dfa12 = new DFA12(this);
    static final String dfa_1s = "\12\uffff";
    static final String dfa_2s = "\1\45\11\uffff";
    static final String dfa_3s = "\1\56\11\uffff";
    static final String dfa_4s = "\1\uffff\1\11\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10";
    static final String dfa_5s = "\1\0\11\uffff}>";
    static final String[] dfa_6s = {
            "\1\1\1\uffff\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA12 extends DFA {

        public DFA12(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 12;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "()* loopback of 1124:3: ( ({...}? => ( ({...}? => ( (otherlv_7= 'prefix' otherlv_8= '=' ( (lv_prefix_9_0= RULE_STRING ) ) ) otherlv_10= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'suffix' otherlv_12= '=' ( (lv_suffix_13_0= RULE_STRING ) ) ) otherlv_14= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_15= 'font' otherlv_16= '=' ( (lv_font_17_0= RULE_STRING ) ) ) otherlv_18= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_19= 'fontSize' otherlv_20= '=' ( (lv_fontSize_21_0= RULE_INT ) ) ) otherlv_22= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_23= 'fontStyle' otherlv_24= '=' ( (lv_fontStyle_25_0= ruleFontStyle ) ) ) otherlv_26= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_27= 'color' otherlv_28= '=' ( (lv_color_29_0= ruleRColor ) ) ) otherlv_30= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_31= 'errorText' otherlv_32= '=' ( (lv_errorText_33_0= RULE_STRING ) ) ) otherlv_34= ';' ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_35= 'errorColor' otherlv_36= '=' ( (lv_errorColor_37_0= ruleRColor ) ) ) otherlv_38= ';' ) ) ) ) )*";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA12_0 = input.LA(1);

                         
                        int index12_0 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (LA12_0==37) ) {s = 1;}

                        else if ( LA12_0 == 39 && getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 0) ) {s = 2;}

                        else if ( LA12_0 == 40 && getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 1) ) {s = 3;}

                        else if ( LA12_0 == 41 && getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 2) ) {s = 4;}

                        else if ( LA12_0 == 42 && getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 3) ) {s = 5;}

                        else if ( LA12_0 == 43 && getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 4) ) {s = 6;}

                        else if ( LA12_0 == 44 && getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 5) ) {s = 7;}

                        else if ( LA12_0 == 45 && getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 6) ) {s = 8;}

                        else if ( LA12_0 == 46 && getUnorderedGroupHelper().canSelect(grammarAccess.getDecoratedTextVarAccess().getUnorderedGroup_6(), 7) ) {s = 9;}

                         
                        input.seek(index12_0);
                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 12, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x04D0000000008002L});
        public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0400000000008002L});
        public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000004001B20000L});
        public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000800000000000L});
        public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000080000L});
        public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000100000L});
        public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000800000400000L});
        public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000004000000L});
        public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000008000000L});
        public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000010400000L});
        public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000020000000L});
        public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000040000000L});
        public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000080000000L});
        public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000003F00000000L});
        public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000000000L,0x0000000060000000L});
        public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x00007FA000000000L});
        public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000000000L,0x000000000001C000L});
        public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0004000000000002L,0x0000000000C00000L});
        public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0001000000000000L});
        public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0002000000000000L});
        public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0008000000000000L});
        public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0020000000010000L});
        public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0200000000000000L});
        public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0100000000000002L});
        public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0800000000000000L});
        public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x1000000000020000L,0x0000000000000233L});
        public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x2000000000040000L});
        public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x4000000000000000L});
        public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000000000000000L,0x0000000000064000L});
        public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000000000480000L});
        public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x8000000000000000L});
        public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000000000000000L,0x0000000180000000L});
        public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0004800000000000L,0x0000000000C00000L});
        public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0004000000000000L,0x0000000000C00000L});
        public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000000000000000L,0x0000000000380000L});
        public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000000000000020L,0x0000000000000004L});
        public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
        public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
        public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000040L});
        public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000080L});
        public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
        public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
        public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000800L});
        public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0000000000000000L,0x0000000000001000L});
        public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x0000000000000000L,0x0000000007000000L});
        public static final BitSet FOLLOW_59 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
        public static final BitSet FOLLOW_60 = new BitSet(new long[]{0x0000000000000000L,0x0000000018000000L});
        public static final BitSet FOLLOW_61 = new BitSet(new long[]{0x0000000100000000L});
    }


}