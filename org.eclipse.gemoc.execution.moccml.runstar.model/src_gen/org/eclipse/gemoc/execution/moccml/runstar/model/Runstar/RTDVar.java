/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RTD Var</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RTDVar#getAttributes <em>Attributes</em>}</li>
 * </ul>
 *
 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getRTDVar()
 * @model
 * @generated
 */
public interface RTDVar extends TextVar {
	/**
	 * Returns the value of the '<em><b>Attributes</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes</em>' attribute list.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getRTDVar_Attributes()
	 * @model required="true"
	 * @generated
	 */
	EList<String> getAttributes();

} // RTDVar
