/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Comment;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ConstTextVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Constant;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.EObjectRef;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Hover;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Image;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImageVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImportJavaStatement;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.IntegerLiteral;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Label;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Mark;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RColor;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RStrategy;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RTDVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RUpdateStrategy;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Representation;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringDecoration;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringLiteral;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.TextVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Variable;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.VariableRef;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Variable_old;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage
 * @generated
 */
public class RunstarSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static RunstarPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RunstarSwitch() {
		if (modelPackage == null) {
			modelPackage = RunstarPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case RunstarPackage.OBJECT_VARIABLE: {
				ObjectVariable objectVariable = (ObjectVariable)theEObject;
				T result = caseObjectVariable(objectVariable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION: {
				RuntimeStateRepresentation runtimeStateRepresentation = (RuntimeStateRepresentation)theEObject;
				T result = caseRuntimeStateRepresentation(runtimeStateRepresentation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.IMPORT_JAVA_STATEMENT: {
				ImportJavaStatement importJavaStatement = (ImportJavaStatement)theEObject;
				T result = caseImportJavaStatement(importJavaStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.VARIABLE_OLD: {
				Variable_old variable_old = (Variable_old)theEObject;
				T result = caseVariable_old(variable_old);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.CONSTANT: {
				Constant constant = (Constant)theEObject;
				T result = caseConstant(constant);
				if (result == null) result = caseVariable_old(constant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.INTEGER_LITERAL: {
				IntegerLiteral integerLiteral = (IntegerLiteral)theEObject;
				T result = caseIntegerLiteral(integerLiteral);
				if (result == null) result = caseConstant(integerLiteral);
				if (result == null) result = caseVariable_old(integerLiteral);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.VARIABLE_REF: {
				VariableRef variableRef = (VariableRef)theEObject;
				T result = caseVariableRef(variableRef);
				if (result == null) result = caseVariable_old(variableRef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.STRING_LITERAL: {
				StringLiteral stringLiteral = (StringLiteral)theEObject;
				T result = caseStringLiteral(stringLiteral);
				if (result == null) result = caseConstant(stringLiteral);
				if (result == null) result = caseVariable_old(stringLiteral);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.EOBJECT_REF: {
				EObjectRef eObjectRef = (EObjectRef)theEObject;
				T result = caseEObjectRef(eObjectRef);
				if (result == null) result = caseVariable_old(eObjectRef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.STRING_DECORATION: {
				StringDecoration stringDecoration = (StringDecoration)theEObject;
				T result = caseStringDecoration(stringDecoration);
				if (result == null) result = caseConstant(stringDecoration);
				if (result == null) result = caseVariable_old(stringDecoration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.REPRESENTATION: {
				Representation representation = (Representation)theEObject;
				T result = caseRepresentation(representation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.PAINT: {
				Paint paint = (Paint)theEObject;
				T result = casePaint(paint);
				if (result == null) result = caseRepresentation(paint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.RCOLOR: {
				RColor rColor = (RColor)theEObject;
				T result = caseRColor(rColor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.HOVER: {
				Hover hover = (Hover)theEObject;
				T result = caseHover(hover);
				if (result == null) result = caseRepresentation(hover);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.MARK: {
				Mark mark = (Mark)theEObject;
				T result = caseMark(mark);
				if (result == null) result = caseRepresentation(mark);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.LABEL: {
				Label label = (Label)theEObject;
				T result = caseLabel(label);
				if (result == null) result = caseRepresentation(label);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.IMAGE_VAR: {
				ImageVar imageVar = (ImageVar)theEObject;
				T result = caseImageVar(imageVar);
				if (result == null) result = caseVariable(imageVar);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.RTD_VAR: {
				RTDVar rtdVar = (RTDVar)theEObject;
				T result = caseRTDVar(rtdVar);
				if (result == null) result = caseTextVar(rtdVar);
				if (result == null) result = caseVariable(rtdVar);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.CONST_TEXT_VAR: {
				ConstTextVar constTextVar = (ConstTextVar)theEObject;
				T result = caseConstTextVar(constTextVar);
				if (result == null) result = caseTextVar(constTextVar);
				if (result == null) result = caseVariable(constTextVar);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.CHART_VAR: {
				ChartVar chartVar = (ChartVar)theEObject;
				T result = caseChartVar(chartVar);
				if (result == null) result = caseImageVar(chartVar);
				if (result == null) result = caseVariable(chartVar);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.RUPDATE_STRATEGY: {
				RUpdateStrategy rUpdateStrategy = (RUpdateStrategy)theEObject;
				T result = caseRUpdateStrategy(rUpdateStrategy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.DECORATED_TEXT_VAR: {
				DecoratedTextVar decoratedTextVar = (DecoratedTextVar)theEObject;
				T result = caseDecoratedTextVar(decoratedTextVar);
				if (result == null) result = caseTextVar(decoratedTextVar);
				if (result == null) result = caseVariable(decoratedTextVar);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.VARIABLE: {
				Variable variable = (Variable)theEObject;
				T result = caseVariable(variable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.TEXT_VAR: {
				TextVar textVar = (TextVar)theEObject;
				T result = caseTextVar(textVar);
				if (result == null) result = caseVariable(textVar);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.IMAGE: {
				Image image = (Image)theEObject;
				T result = caseImage(image);
				if (result == null) result = caseRepresentation(image);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.STACK: {
				Stack stack = (Stack)theEObject;
				T result = caseStack(stack);
				if (result == null) result = caseRepresentation(stack);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.RSTRATEGY: {
				RStrategy rStrategy = (RStrategy)theEObject;
				T result = caseRStrategy(rStrategy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RunstarPackage.COMMENT: {
				Comment comment = (Comment)theEObject;
				T result = caseComment(comment);
				if (result == null) result = caseRepresentation(comment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObjectVariable(ObjectVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Runtime State Representation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Runtime State Representation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuntimeStateRepresentation(RuntimeStateRepresentation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Import Java Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Import Java Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImportJavaStatement(ImportJavaStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable old</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable old</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariable_old(Variable_old object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstant(Constant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Integer Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntegerLiteral(IntegerLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Ref</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariableRef(VariableRef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringLiteral(StringLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject Ref</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEObjectRef(EObjectRef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String Decoration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String Decoration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringDecoration(StringDecoration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Representation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Representation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRepresentation(Representation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Paint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Paint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePaint(Paint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RColor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RColor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRColor(RColor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hover</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hover</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHover(Hover object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mark</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mark</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMark(Mark object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Label</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Label</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLabel(Label object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Image Var</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Image Var</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImageVar(ImageVar object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RTD Var</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RTD Var</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRTDVar(RTDVar object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Const Text Var</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Const Text Var</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstTextVar(ConstTextVar object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Chart Var</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Chart Var</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChartVar(ChartVar object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RUpdate Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RUpdate Strategy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRUpdateStrategy(RUpdateStrategy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Decorated Text Var</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Decorated Text Var</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDecoratedTextVar(DecoratedTextVar object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariable(Variable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Text Var</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Text Var</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTextVar(TextVar object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Image</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Image</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImage(Image object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Stack</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Stack</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStack(Stack object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RStrategy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RStrategy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRStrategy(RStrategy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Comment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Comment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComment(Comment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //RunstarSwitch
