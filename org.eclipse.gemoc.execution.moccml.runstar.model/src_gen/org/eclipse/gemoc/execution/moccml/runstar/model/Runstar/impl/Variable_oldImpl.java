/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Variable_old;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable old</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class Variable_oldImpl extends EObjectImpl implements Variable_old {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Variable_oldImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RunstarPackage.Literals.VARIABLE_OLD;
	}

} //Variable_oldImpl
