/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.LineStyle;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RTDVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Chart Var</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ChartVarImpl#getNbValuesDisplayed <em>Nb Values Displayed</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ChartVarImpl#getXlabel <em>Xlabel</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ChartVarImpl#getYlabel <em>Ylabel</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ChartVarImpl#getTitle <em>Title</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ChartVarImpl#getXrtd <em>Xrtd</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ChartVarImpl#getYrtds <em>Yrtds</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ChartVarImpl#getLineStyle <em>Line Style</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ChartVarImpl extends ImageVarImpl implements ChartVar {
	/**
	 * The default value of the '{@link #getNbValuesDisplayed() <em>Nb Values Displayed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNbValuesDisplayed()
	 * @generated
	 * @ordered
	 */
	protected static final int NB_VALUES_DISPLAYED_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNbValuesDisplayed() <em>Nb Values Displayed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNbValuesDisplayed()
	 * @generated
	 * @ordered
	 */
	protected int nbValuesDisplayed = NB_VALUES_DISPLAYED_EDEFAULT;

	/**
	 * The default value of the '{@link #getXlabel() <em>Xlabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXlabel()
	 * @generated
	 * @ordered
	 */
	protected static final String XLABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getXlabel() <em>Xlabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXlabel()
	 * @generated
	 * @ordered
	 */
	protected String xlabel = XLABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getYlabel() <em>Ylabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYlabel()
	 * @generated
	 * @ordered
	 */
	protected static final String YLABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getYlabel() <em>Ylabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYlabel()
	 * @generated
	 * @ordered
	 */
	protected String ylabel = YLABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected static final String TITLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected String title = TITLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getXrtd() <em>Xrtd</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXrtd()
	 * @generated
	 * @ordered
	 */
	protected RTDVar xrtd;

	/**
	 * The cached value of the '{@link #getYrtds() <em>Yrtds</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYrtds()
	 * @generated
	 * @ordered
	 */
	protected EList<RTDVar> yrtds;

	/**
	 * The default value of the '{@link #getLineStyle() <em>Line Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLineStyle()
	 * @generated
	 * @ordered
	 */
	protected static final LineStyle LINE_STYLE_EDEFAULT = LineStyle.DIGITAL;

	/**
	 * The cached value of the '{@link #getLineStyle() <em>Line Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLineStyle()
	 * @generated
	 * @ordered
	 */
	protected LineStyle lineStyle = LINE_STYLE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChartVarImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RunstarPackage.Literals.CHART_VAR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNbValuesDisplayed() {
		return nbValuesDisplayed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNbValuesDisplayed(int newNbValuesDisplayed) {
		int oldNbValuesDisplayed = nbValuesDisplayed;
		nbValuesDisplayed = newNbValuesDisplayed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.CHART_VAR__NB_VALUES_DISPLAYED, oldNbValuesDisplayed, nbValuesDisplayed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getXlabel() {
		return xlabel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setXlabel(String newXlabel) {
		String oldXlabel = xlabel;
		xlabel = newXlabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.CHART_VAR__XLABEL, oldXlabel, xlabel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getYlabel() {
		return ylabel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setYlabel(String newYlabel) {
		String oldYlabel = ylabel;
		ylabel = newYlabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.CHART_VAR__YLABEL, oldYlabel, ylabel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTitle() {
		return title;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTitle(String newTitle) {
		String oldTitle = title;
		title = newTitle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.CHART_VAR__TITLE, oldTitle, title));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTDVar getXrtd() {
		if (xrtd != null && xrtd.eIsProxy()) {
			InternalEObject oldXrtd = (InternalEObject)xrtd;
			xrtd = (RTDVar)eResolveProxy(oldXrtd);
			if (xrtd != oldXrtd) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RunstarPackage.CHART_VAR__XRTD, oldXrtd, xrtd));
			}
		}
		return xrtd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RTDVar basicGetXrtd() {
		return xrtd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setXrtd(RTDVar newXrtd) {
		RTDVar oldXrtd = xrtd;
		xrtd = newXrtd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.CHART_VAR__XRTD, oldXrtd, xrtd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<RTDVar> getYrtds() {
		if (yrtds == null) {
			yrtds = new EObjectResolvingEList<RTDVar>(RTDVar.class, this, RunstarPackage.CHART_VAR__YRTDS);
		}
		return yrtds;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LineStyle getLineStyle() {
		return lineStyle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLineStyle(LineStyle newLineStyle) {
		LineStyle oldLineStyle = lineStyle;
		lineStyle = newLineStyle == null ? LINE_STYLE_EDEFAULT : newLineStyle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.CHART_VAR__LINE_STYLE, oldLineStyle, lineStyle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RunstarPackage.CHART_VAR__NB_VALUES_DISPLAYED:
				return getNbValuesDisplayed();
			case RunstarPackage.CHART_VAR__XLABEL:
				return getXlabel();
			case RunstarPackage.CHART_VAR__YLABEL:
				return getYlabel();
			case RunstarPackage.CHART_VAR__TITLE:
				return getTitle();
			case RunstarPackage.CHART_VAR__XRTD:
				if (resolve) return getXrtd();
				return basicGetXrtd();
			case RunstarPackage.CHART_VAR__YRTDS:
				return getYrtds();
			case RunstarPackage.CHART_VAR__LINE_STYLE:
				return getLineStyle();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RunstarPackage.CHART_VAR__NB_VALUES_DISPLAYED:
				setNbValuesDisplayed((Integer)newValue);
				return;
			case RunstarPackage.CHART_VAR__XLABEL:
				setXlabel((String)newValue);
				return;
			case RunstarPackage.CHART_VAR__YLABEL:
				setYlabel((String)newValue);
				return;
			case RunstarPackage.CHART_VAR__TITLE:
				setTitle((String)newValue);
				return;
			case RunstarPackage.CHART_VAR__XRTD:
				setXrtd((RTDVar)newValue);
				return;
			case RunstarPackage.CHART_VAR__YRTDS:
				getYrtds().clear();
				getYrtds().addAll((Collection<? extends RTDVar>)newValue);
				return;
			case RunstarPackage.CHART_VAR__LINE_STYLE:
				setLineStyle((LineStyle)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RunstarPackage.CHART_VAR__NB_VALUES_DISPLAYED:
				setNbValuesDisplayed(NB_VALUES_DISPLAYED_EDEFAULT);
				return;
			case RunstarPackage.CHART_VAR__XLABEL:
				setXlabel(XLABEL_EDEFAULT);
				return;
			case RunstarPackage.CHART_VAR__YLABEL:
				setYlabel(YLABEL_EDEFAULT);
				return;
			case RunstarPackage.CHART_VAR__TITLE:
				setTitle(TITLE_EDEFAULT);
				return;
			case RunstarPackage.CHART_VAR__XRTD:
				setXrtd((RTDVar)null);
				return;
			case RunstarPackage.CHART_VAR__YRTDS:
				getYrtds().clear();
				return;
			case RunstarPackage.CHART_VAR__LINE_STYLE:
				setLineStyle(LINE_STYLE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RunstarPackage.CHART_VAR__NB_VALUES_DISPLAYED:
				return nbValuesDisplayed != NB_VALUES_DISPLAYED_EDEFAULT;
			case RunstarPackage.CHART_VAR__XLABEL:
				return XLABEL_EDEFAULT == null ? xlabel != null : !XLABEL_EDEFAULT.equals(xlabel);
			case RunstarPackage.CHART_VAR__YLABEL:
				return YLABEL_EDEFAULT == null ? ylabel != null : !YLABEL_EDEFAULT.equals(ylabel);
			case RunstarPackage.CHART_VAR__TITLE:
				return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
			case RunstarPackage.CHART_VAR__XRTD:
				return xrtd != null;
			case RunstarPackage.CHART_VAR__YRTDS:
				return yrtds != null && !yrtds.isEmpty();
			case RunstarPackage.CHART_VAR__LINE_STYLE:
				return lineStyle != LINE_STYLE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (nbValuesDisplayed: ");
		result.append(nbValuesDisplayed);
		result.append(", xlabel: ");
		result.append(xlabel);
		result.append(", ylabel: ");
		result.append(ylabel);
		result.append(", title: ");
		result.append(title);
		result.append(", lineStyle: ");
		result.append(lineStyle);
		result.append(')');
		return result.toString();
	}

} //ChartVarImpl
