/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.PaintType;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RColor;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RUpdateStrategy;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Paint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.PaintImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.PaintImpl#getColor <em>Color</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.PaintImpl#getOnConcept <em>On Concept</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.PaintImpl#getOwnedUpdateStrategy <em>Owned Update Strategy</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.PaintImpl#getPersistent <em>Persistent</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PaintImpl extends RepresentationImpl implements Paint {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final PaintType TYPE_EDEFAULT = PaintType.NONE;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected PaintType type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected RColor color;

	/**
	 * The cached value of the '{@link #getOnConcept() <em>On Concept</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOnConcept()
	 * @generated
	 * @ordered
	 */
	protected EObject onConcept;

	/**
	 * The cached value of the '{@link #getOwnedUpdateStrategy() <em>Owned Update Strategy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedUpdateStrategy()
	 * @generated
	 * @ordered
	 */
	protected RUpdateStrategy ownedUpdateStrategy;

	/**
	 * The default value of the '{@link #getPersistent() <em>Persistent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersistent()
	 * @generated
	 * @ordered
	 */
	protected static final RBoolean PERSISTENT_EDEFAULT = RBoolean.FALSE;

	/**
	 * The cached value of the '{@link #getPersistent() <em>Persistent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersistent()
	 * @generated
	 * @ordered
	 */
	protected RBoolean persistent = PERSISTENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PaintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RunstarPackage.Literals.PAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PaintType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(PaintType newType) {
		PaintType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.PAINT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RColor getColor() {
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetColor(RColor newColor, NotificationChain msgs) {
		RColor oldColor = color;
		color = newColor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RunstarPackage.PAINT__COLOR, oldColor, newColor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setColor(RColor newColor) {
		if (newColor != color) {
			NotificationChain msgs = null;
			if (color != null)
				msgs = ((InternalEObject)color).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RunstarPackage.PAINT__COLOR, null, msgs);
			if (newColor != null)
				msgs = ((InternalEObject)newColor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RunstarPackage.PAINT__COLOR, null, msgs);
			msgs = basicSetColor(newColor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.PAINT__COLOR, newColor, newColor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject getOnConcept() {
		if (onConcept != null && onConcept.eIsProxy()) {
			InternalEObject oldOnConcept = (InternalEObject)onConcept;
			onConcept = eResolveProxy(oldOnConcept);
			if (onConcept != oldOnConcept) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RunstarPackage.PAINT__ON_CONCEPT, oldOnConcept, onConcept));
			}
		}
		return onConcept;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetOnConcept() {
		return onConcept;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOnConcept(EObject newOnConcept) {
		EObject oldOnConcept = onConcept;
		onConcept = newOnConcept;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.PAINT__ON_CONCEPT, oldOnConcept, onConcept));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RUpdateStrategy getOwnedUpdateStrategy() {
		return ownedUpdateStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOwnedUpdateStrategy(RUpdateStrategy newOwnedUpdateStrategy, NotificationChain msgs) {
		RUpdateStrategy oldOwnedUpdateStrategy = ownedUpdateStrategy;
		ownedUpdateStrategy = newOwnedUpdateStrategy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RunstarPackage.PAINT__OWNED_UPDATE_STRATEGY, oldOwnedUpdateStrategy, newOwnedUpdateStrategy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOwnedUpdateStrategy(RUpdateStrategy newOwnedUpdateStrategy) {
		if (newOwnedUpdateStrategy != ownedUpdateStrategy) {
			NotificationChain msgs = null;
			if (ownedUpdateStrategy != null)
				msgs = ((InternalEObject)ownedUpdateStrategy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RunstarPackage.PAINT__OWNED_UPDATE_STRATEGY, null, msgs);
			if (newOwnedUpdateStrategy != null)
				msgs = ((InternalEObject)newOwnedUpdateStrategy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RunstarPackage.PAINT__OWNED_UPDATE_STRATEGY, null, msgs);
			msgs = basicSetOwnedUpdateStrategy(newOwnedUpdateStrategy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.PAINT__OWNED_UPDATE_STRATEGY, newOwnedUpdateStrategy, newOwnedUpdateStrategy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RBoolean getPersistent() {
		return persistent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPersistent(RBoolean newPersistent) {
		RBoolean oldPersistent = persistent;
		persistent = newPersistent == null ? PERSISTENT_EDEFAULT : newPersistent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.PAINT__PERSISTENT, oldPersistent, persistent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RunstarPackage.PAINT__COLOR:
				return basicSetColor(null, msgs);
			case RunstarPackage.PAINT__OWNED_UPDATE_STRATEGY:
				return basicSetOwnedUpdateStrategy(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RunstarPackage.PAINT__TYPE:
				return getType();
			case RunstarPackage.PAINT__COLOR:
				return getColor();
			case RunstarPackage.PAINT__ON_CONCEPT:
				if (resolve) return getOnConcept();
				return basicGetOnConcept();
			case RunstarPackage.PAINT__OWNED_UPDATE_STRATEGY:
				return getOwnedUpdateStrategy();
			case RunstarPackage.PAINT__PERSISTENT:
				return getPersistent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RunstarPackage.PAINT__TYPE:
				setType((PaintType)newValue);
				return;
			case RunstarPackage.PAINT__COLOR:
				setColor((RColor)newValue);
				return;
			case RunstarPackage.PAINT__ON_CONCEPT:
				setOnConcept((EObject)newValue);
				return;
			case RunstarPackage.PAINT__OWNED_UPDATE_STRATEGY:
				setOwnedUpdateStrategy((RUpdateStrategy)newValue);
				return;
			case RunstarPackage.PAINT__PERSISTENT:
				setPersistent((RBoolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RunstarPackage.PAINT__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case RunstarPackage.PAINT__COLOR:
				setColor((RColor)null);
				return;
			case RunstarPackage.PAINT__ON_CONCEPT:
				setOnConcept((EObject)null);
				return;
			case RunstarPackage.PAINT__OWNED_UPDATE_STRATEGY:
				setOwnedUpdateStrategy((RUpdateStrategy)null);
				return;
			case RunstarPackage.PAINT__PERSISTENT:
				setPersistent(PERSISTENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RunstarPackage.PAINT__TYPE:
				return type != TYPE_EDEFAULT;
			case RunstarPackage.PAINT__COLOR:
				return color != null;
			case RunstarPackage.PAINT__ON_CONCEPT:
				return onConcept != null;
			case RunstarPackage.PAINT__OWNED_UPDATE_STRATEGY:
				return ownedUpdateStrategy != null;
			case RunstarPackage.PAINT__PERSISTENT:
				return persistent != PERSISTENT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(", persistent: ");
		result.append(persistent);
		result.append(')');
		return result.toString();
	}

} //PaintImpl
