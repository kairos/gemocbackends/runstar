/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImportJavaStatement;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Representation;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Variable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Runtime State Representation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RuntimeStateRepresentationImpl#getModelImports <em>Model Imports</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RuntimeStateRepresentationImpl#getClassImports <em>Class Imports</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RuntimeStateRepresentationImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RuntimeStateRepresentationImpl#getVariables <em>Variables</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RuntimeStateRepresentationImpl#getRepresentations <em>Representations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RuntimeStateRepresentationImpl extends EObjectImpl implements RuntimeStateRepresentation {
	/**
	 * The cached value of the '{@link #getModelImports() <em>Model Imports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelImports()
	 * @generated
	 * @ordered
	 */
	protected EList<ImportStatement> modelImports;

	/**
	 * The cached value of the '{@link #getClassImports() <em>Class Imports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassImports()
	 * @generated
	 * @ordered
	 */
	protected EList<ImportJavaStatement> classImports;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getVariables() <em>Variables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariables()
	 * @generated
	 * @ordered
	 */
	protected EList<Variable> variables;

	/**
	 * The cached value of the '{@link #getRepresentations() <em>Representations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepresentations()
	 * @generated
	 * @ordered
	 */
	protected EList<Representation> representations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RuntimeStateRepresentationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RunstarPackage.Literals.RUNTIME_STATE_REPRESENTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ImportStatement> getModelImports() {
		if (modelImports == null) {
			modelImports = new EObjectContainmentEList<ImportStatement>(ImportStatement.class, this, RunstarPackage.RUNTIME_STATE_REPRESENTATION__MODEL_IMPORTS);
		}
		return modelImports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ImportJavaStatement> getClassImports() {
		if (classImports == null) {
			classImports = new EObjectContainmentEList<ImportJavaStatement>(ImportJavaStatement.class, this, RunstarPackage.RUNTIME_STATE_REPRESENTATION__CLASS_IMPORTS);
		}
		return classImports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.RUNTIME_STATE_REPRESENTATION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Variable> getVariables() {
		if (variables == null) {
			variables = new EObjectContainmentEList<Variable>(Variable.class, this, RunstarPackage.RUNTIME_STATE_REPRESENTATION__VARIABLES);
		}
		return variables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Representation> getRepresentations() {
		if (representations == null) {
			representations = new EObjectContainmentEList<Representation>(Representation.class, this, RunstarPackage.RUNTIME_STATE_REPRESENTATION__REPRESENTATIONS);
		}
		return representations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__MODEL_IMPORTS:
				return ((InternalEList<?>)getModelImports()).basicRemove(otherEnd, msgs);
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__CLASS_IMPORTS:
				return ((InternalEList<?>)getClassImports()).basicRemove(otherEnd, msgs);
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__VARIABLES:
				return ((InternalEList<?>)getVariables()).basicRemove(otherEnd, msgs);
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__REPRESENTATIONS:
				return ((InternalEList<?>)getRepresentations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__MODEL_IMPORTS:
				return getModelImports();
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__CLASS_IMPORTS:
				return getClassImports();
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__NAME:
				return getName();
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__VARIABLES:
				return getVariables();
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__REPRESENTATIONS:
				return getRepresentations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__MODEL_IMPORTS:
				getModelImports().clear();
				getModelImports().addAll((Collection<? extends ImportStatement>)newValue);
				return;
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__CLASS_IMPORTS:
				getClassImports().clear();
				getClassImports().addAll((Collection<? extends ImportJavaStatement>)newValue);
				return;
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__NAME:
				setName((String)newValue);
				return;
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__VARIABLES:
				getVariables().clear();
				getVariables().addAll((Collection<? extends Variable>)newValue);
				return;
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__REPRESENTATIONS:
				getRepresentations().clear();
				getRepresentations().addAll((Collection<? extends Representation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__MODEL_IMPORTS:
				getModelImports().clear();
				return;
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__CLASS_IMPORTS:
				getClassImports().clear();
				return;
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__VARIABLES:
				getVariables().clear();
				return;
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__REPRESENTATIONS:
				getRepresentations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__MODEL_IMPORTS:
				return modelImports != null && !modelImports.isEmpty();
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__CLASS_IMPORTS:
				return classImports != null && !classImports.isEmpty();
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__VARIABLES:
				return variables != null && !variables.isEmpty();
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION__REPRESENTATIONS:
				return representations != null && !representations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //RuntimeStateRepresentationImpl
