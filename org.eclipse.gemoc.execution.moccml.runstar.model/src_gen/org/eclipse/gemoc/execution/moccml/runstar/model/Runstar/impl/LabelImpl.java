/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Label;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Label</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LabelImpl extends RepresentationImpl implements Label {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LabelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RunstarPackage.Literals.LABEL;
	}

} //LabelImpl
