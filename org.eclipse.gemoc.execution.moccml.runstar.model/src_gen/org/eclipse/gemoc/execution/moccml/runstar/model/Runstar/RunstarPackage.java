/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarFactory
 * @model kind="package"
 * @generated
 */
public interface RunstarPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Runstar";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://gemoc.org/moccml/runstar";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Runstar";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RunstarPackage eINSTANCE = org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ObjectVariableImpl <em>Object Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ObjectVariableImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getObjectVariable()
	 * @generated
	 */
	int OBJECT_VARIABLE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_VARIABLE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_VARIABLE__TYPE = 1;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_VARIABLE__INITIAL_VALUE = 2;

	/**
	 * The number of structural features of the '<em>Object Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_VARIABLE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RuntimeStateRepresentationImpl <em>Runtime State Representation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RuntimeStateRepresentationImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getRuntimeStateRepresentation()
	 * @generated
	 */
	int RUNTIME_STATE_REPRESENTATION = 1;

	/**
	 * The feature id for the '<em><b>Model Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_REPRESENTATION__MODEL_IMPORTS = 0;

	/**
	 * The feature id for the '<em><b>Class Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_REPRESENTATION__CLASS_IMPORTS = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_REPRESENTATION__NAME = 2;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_REPRESENTATION__VARIABLES = 3;

	/**
	 * The feature id for the '<em><b>Representations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_REPRESENTATION__REPRESENTATIONS = 4;

	/**
	 * The number of structural features of the '<em>Runtime State Representation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_REPRESENTATION_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ImportJavaStatementImpl <em>Import Java Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ImportJavaStatementImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getImportJavaStatement()
	 * @generated
	 */
	int IMPORT_JAVA_STATEMENT = 2;

	/**
	 * The feature id for the '<em><b>Imported Namespace</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_JAVA_STATEMENT__IMPORTED_NAMESPACE = 0;

	/**
	 * The number of structural features of the '<em>Import Java Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_JAVA_STATEMENT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.Variable_oldImpl <em>Variable old</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.Variable_oldImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getVariable_old()
	 * @generated
	 */
	int VARIABLE_OLD = 3;

	/**
	 * The number of structural features of the '<em>Variable old</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_OLD_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ConstantImpl <em>Constant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ConstantImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getConstant()
	 * @generated
	 */
	int CONSTANT = 4;

	/**
	 * The number of structural features of the '<em>Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_FEATURE_COUNT = VARIABLE_OLD_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.IntegerLiteralImpl <em>Integer Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.IntegerLiteralImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getIntegerLiteral()
	 * @generated
	 */
	int INTEGER_LITERAL = 5;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_LITERAL__VALUE = CONSTANT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Integer Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_LITERAL_FEATURE_COUNT = CONSTANT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.VariableRefImpl <em>Variable Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.VariableRefImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getVariableRef()
	 * @generated
	 */
	int VARIABLE_REF = 6;

	/**
	 * The feature id for the '<em><b>Var</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REF__VAR = VARIABLE_OLD_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Variable Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REF_FEATURE_COUNT = VARIABLE_OLD_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StringLiteralImpl <em>String Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StringLiteralImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getStringLiteral()
	 * @generated
	 */
	int STRING_LITERAL = 7;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_LITERAL__VALUE = CONSTANT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_LITERAL_FEATURE_COUNT = CONSTANT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.EObjectRefImpl <em>EObject Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.EObjectRefImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getEObjectRef()
	 * @generated
	 */
	int EOBJECT_REF = 8;

	/**
	 * The feature id for the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_REF__OBJECT = VARIABLE_OLD_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>EObject Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_REF_FEATURE_COUNT = VARIABLE_OLD_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StringDecorationImpl <em>String Decoration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StringDecorationImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getStringDecoration()
	 * @generated
	 */
	int STRING_DECORATION = 9;

	/**
	 * The feature id for the '<em><b>Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_DECORATION__PREFIX = CONSTANT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Suffix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_DECORATION__SUFFIX = CONSTANT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>String Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_DECORATION__STRING_LITERAL = CONSTANT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>String Decoration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_DECORATION_FEATURE_COUNT = CONSTANT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RepresentationImpl <em>Representation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RepresentationImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getRepresentation()
	 * @generated
	 */
	int REPRESENTATION = 10;

	/**
	 * The feature id for the '<em><b>Image</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPRESENTATION__IMAGE = 0;

	/**
	 * The feature id for the '<em><b>Text</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPRESENTATION__TEXT = 1;

	/**
	 * The number of structural features of the '<em>Representation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPRESENTATION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.PaintImpl <em>Paint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.PaintImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getPaint()
	 * @generated
	 */
	int PAINT = 11;

	/**
	 * The feature id for the '<em><b>Image</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT__IMAGE = REPRESENTATION__IMAGE;

	/**
	 * The feature id for the '<em><b>Text</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT__TEXT = REPRESENTATION__TEXT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT__TYPE = REPRESENTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT__COLOR = REPRESENTATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>On Concept</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT__ON_CONCEPT = REPRESENTATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Owned Update Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT__OWNED_UPDATE_STRATEGY = REPRESENTATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Persistent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT__PERSISTENT = REPRESENTATION_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Paint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_FEATURE_COUNT = REPRESENTATION_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RColorImpl <em>RColor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RColorImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getRColor()
	 * @generated
	 */
	int RCOLOR = 12;

	/**
	 * The feature id for the '<em><b>Red</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RCOLOR__RED = 0;

	/**
	 * The feature id for the '<em><b>Blue</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RCOLOR__BLUE = 1;

	/**
	 * The feature id for the '<em><b>Green</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RCOLOR__GREEN = 2;

	/**
	 * The number of structural features of the '<em>RColor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RCOLOR_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.HoverImpl <em>Hover</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.HoverImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getHover()
	 * @generated
	 */
	int HOVER = 13;

	/**
	 * The feature id for the '<em><b>Image</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOVER__IMAGE = REPRESENTATION__IMAGE;

	/**
	 * The feature id for the '<em><b>Text</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOVER__TEXT = REPRESENTATION__TEXT;

	/**
	 * The feature id for the '<em><b>From Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOVER__FROM_START = REPRESENTATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Hover</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOVER_FEATURE_COUNT = REPRESENTATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.MarkImpl <em>Mark</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.MarkImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getMark()
	 * @generated
	 */
	int MARK = 14;

	/**
	 * The feature id for the '<em><b>Image</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MARK__IMAGE = REPRESENTATION__IMAGE;

	/**
	 * The feature id for the '<em><b>Text</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MARK__TEXT = REPRESENTATION__TEXT;

	/**
	 * The feature id for the '<em><b>Marker Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MARK__MARKER_TYPE = REPRESENTATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Mark</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MARK_FEATURE_COUNT = REPRESENTATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.LabelImpl <em>Label</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.LabelImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getLabel()
	 * @generated
	 */
	int LABEL = 15;

	/**
	 * The feature id for the '<em><b>Image</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__IMAGE = REPRESENTATION__IMAGE;

	/**
	 * The feature id for the '<em><b>Text</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__TEXT = REPRESENTATION__TEXT;

	/**
	 * The number of structural features of the '<em>Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL_FEATURE_COUNT = REPRESENTATION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.VariableImpl <em>Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.VariableImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getVariable()
	 * @generated
	 */
	int VARIABLE = 22;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__STRATEGY = 1;

	/**
	 * The number of structural features of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ImageVarImpl <em>Image Var</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ImageVarImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getImageVar()
	 * @generated
	 */
	int IMAGE_VAR = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_VAR__NAME = VARIABLE__NAME;

	/**
	 * The feature id for the '<em><b>Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_VAR__STRATEGY = VARIABLE__STRATEGY;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_VAR__PATH = VARIABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Image Var</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_VAR_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.TextVarImpl <em>Text Var</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.TextVarImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getTextVar()
	 * @generated
	 */
	int TEXT_VAR = 23;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT_VAR__NAME = VARIABLE__NAME;

	/**
	 * The feature id for the '<em><b>Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT_VAR__STRATEGY = VARIABLE__STRATEGY;

	/**
	 * The number of structural features of the '<em>Text Var</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT_VAR_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RTDVarImpl <em>RTD Var</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RTDVarImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getRTDVar()
	 * @generated
	 */
	int RTD_VAR = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTD_VAR__NAME = TEXT_VAR__NAME;

	/**
	 * The feature id for the '<em><b>Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTD_VAR__STRATEGY = TEXT_VAR__STRATEGY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTD_VAR__ATTRIBUTES = TEXT_VAR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>RTD Var</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTD_VAR_FEATURE_COUNT = TEXT_VAR_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ConstTextVarImpl <em>Const Text Var</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ConstTextVarImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getConstTextVar()
	 * @generated
	 */
	int CONST_TEXT_VAR = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONST_TEXT_VAR__NAME = TEXT_VAR__NAME;

	/**
	 * The feature id for the '<em><b>Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONST_TEXT_VAR__STRATEGY = TEXT_VAR__STRATEGY;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONST_TEXT_VAR__VALUE = TEXT_VAR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Const Text Var</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONST_TEXT_VAR_FEATURE_COUNT = TEXT_VAR_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ChartVarImpl <em>Chart Var</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ChartVarImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getChartVar()
	 * @generated
	 */
	int CHART_VAR = 19;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VAR__NAME = IMAGE_VAR__NAME;

	/**
	 * The feature id for the '<em><b>Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VAR__STRATEGY = IMAGE_VAR__STRATEGY;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VAR__PATH = IMAGE_VAR__PATH;

	/**
	 * The feature id for the '<em><b>Nb Values Displayed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VAR__NB_VALUES_DISPLAYED = IMAGE_VAR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Xlabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VAR__XLABEL = IMAGE_VAR_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ylabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VAR__YLABEL = IMAGE_VAR_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VAR__TITLE = IMAGE_VAR_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Xrtd</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VAR__XRTD = IMAGE_VAR_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Yrtds</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VAR__YRTDS = IMAGE_VAR_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Line Style</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VAR__LINE_STYLE = IMAGE_VAR_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Chart Var</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHART_VAR_FEATURE_COUNT = IMAGE_VAR_FEATURE_COUNT + 7;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RUpdateStrategyImpl <em>RUpdate Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RUpdateStrategyImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getRUpdateStrategy()
	 * @generated
	 */
	int RUPDATE_STRATEGY = 20;

	/**
	 * The feature id for the '<em><b>Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUPDATE_STRATEGY__EVENTS = 0;

	/**
	 * The feature id for the '<em><b>Step Execution</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUPDATE_STRATEGY__STEP_EXECUTION = 1;

	/**
	 * The number of structural features of the '<em>RUpdate Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUPDATE_STRATEGY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.DecoratedTextVarImpl <em>Decorated Text Var</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.DecoratedTextVarImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getDecoratedTextVar()
	 * @generated
	 */
	int DECORATED_TEXT_VAR = 21;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATED_TEXT_VAR__NAME = TEXT_VAR__NAME;

	/**
	 * The feature id for the '<em><b>Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATED_TEXT_VAR__STRATEGY = TEXT_VAR__STRATEGY;

	/**
	 * The feature id for the '<em><b>Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATED_TEXT_VAR__PREFIX = TEXT_VAR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Suffix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATED_TEXT_VAR__SUFFIX = TEXT_VAR_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATED_TEXT_VAR__COLOR = TEXT_VAR_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Font</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATED_TEXT_VAR__FONT = TEXT_VAR_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Font Style</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATED_TEXT_VAR__FONT_STYLE = TEXT_VAR_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Font Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATED_TEXT_VAR__FONT_SIZE = TEXT_VAR_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Decorated</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATED_TEXT_VAR__DECORATED = TEXT_VAR_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Error Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATED_TEXT_VAR__ERROR_COLOR = TEXT_VAR_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Error Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATED_TEXT_VAR__ERROR_TEXT = TEXT_VAR_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Decorated Text Var</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATED_TEXT_VAR_FEATURE_COUNT = TEXT_VAR_FEATURE_COUNT + 9;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ImageImpl <em>Image</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ImageImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getImage()
	 * @generated
	 */
	int IMAGE = 24;

	/**
	 * The feature id for the '<em><b>Image</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE__IMAGE = REPRESENTATION__IMAGE;

	/**
	 * The feature id for the '<em><b>Text</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE__TEXT = REPRESENTATION__TEXT;

	/**
	 * The feature id for the '<em><b>Initial Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE__INITIAL_SIZE = REPRESENTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE__DEPTH = REPRESENTATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Image</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_FEATURE_COUNT = REPRESENTATION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StackImpl <em>Stack</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StackImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getStack()
	 * @generated
	 */
	int STACK = 25;

	/**
	 * The feature id for the '<em><b>Image</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STACK__IMAGE = REPRESENTATION__IMAGE;

	/**
	 * The feature id for the '<em><b>Text</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STACK__TEXT = REPRESENTATION__TEXT;

	/**
	 * The feature id for the '<em><b>Max Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STACK__MAX_LENGTH = REPRESENTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Vertical</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STACK__IS_VERTICAL = REPRESENTATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Shape</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STACK__SHAPE = REPRESENTATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Strategy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STACK__STRATEGY = REPRESENTATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STACK__DEPTH = REPRESENTATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STACK__TITLE = REPRESENTATION_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Stack</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STACK_FEATURE_COUNT = REPRESENTATION_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RStrategyImpl <em>RStrategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RStrategyImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getRStrategy()
	 * @generated
	 */
	int RSTRATEGY = 26;

	/**
	 * The feature id for the '<em><b>On Concept</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RSTRATEGY__ON_CONCEPT = 0;

	/**
	 * The feature id for the '<em><b>Owned Update Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RSTRATEGY__OWNED_UPDATE_STRATEGY = 1;

	/**
	 * The number of structural features of the '<em>RStrategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RSTRATEGY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.CommentImpl <em>Comment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.CommentImpl
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getComment()
	 * @generated
	 */
	int COMMENT = 27;

	/**
	 * The feature id for the '<em><b>Image</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__IMAGE = REPRESENTATION__IMAGE;

	/**
	 * The feature id for the '<em><b>Text</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__TEXT = REPRESENTATION__TEXT;

	/**
	 * The number of structural features of the '<em>Comment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT_FEATURE_COUNT = REPRESENTATION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.PaintType <em>Paint Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.PaintType
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getPaintType()
	 * @generated
	 */
	int PAINT_TYPE = 28;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StepExecution <em>Step Execution</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StepExecution
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getStepExecution()
	 * @generated
	 */
	int STEP_EXECUTION = 29;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.FontStyle <em>Font Style</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.FontStyle
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getFontStyle()
	 * @generated
	 */
	int FONT_STYLE = 30;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.MarkerType <em>Marker Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.MarkerType
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getMarkerType()
	 * @generated
	 */
	int MARKER_TYPE = 31;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackShape <em>Stack Shape</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackShape
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getStackShape()
	 * @generated
	 */
	int STACK_SHAPE = 32;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackStrategy <em>Stack Strategy</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackStrategy
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getStackStrategy()
	 * @generated
	 */
	int STACK_STRATEGY = 33;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Boolean <em>Boolean</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Boolean
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getBoolean()
	 * @generated
	 */
	int BOOLEAN = 34;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean <em>RBoolean</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getRBoolean()
	 * @generated
	 */
	int RBOOLEAN = 35;

	/**
	 * The meta object id for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.LineStyle <em>Line Style</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.LineStyle
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getLineStyle()
	 * @generated
	 */
	int LINE_STYLE = 36;


	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable <em>Object Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Variable</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable
	 * @generated
	 */
	EClass getObjectVariable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable#getName()
	 * @see #getObjectVariable()
	 * @generated
	 */
	EAttribute getObjectVariable_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable#getType()
	 * @see #getObjectVariable()
	 * @generated
	 */
	EReference getObjectVariable_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable#getInitialValue <em>Initial Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Value</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable#getInitialValue()
	 * @see #getObjectVariable()
	 * @generated
	 */
	EAttribute getObjectVariable_InitialValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation <em>Runtime State Representation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Runtime State Representation</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation
	 * @generated
	 */
	EClass getRuntimeStateRepresentation();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation#getModelImports <em>Model Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Model Imports</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation#getModelImports()
	 * @see #getRuntimeStateRepresentation()
	 * @generated
	 */
	EReference getRuntimeStateRepresentation_ModelImports();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation#getClassImports <em>Class Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Class Imports</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation#getClassImports()
	 * @see #getRuntimeStateRepresentation()
	 * @generated
	 */
	EReference getRuntimeStateRepresentation_ClassImports();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation#getName()
	 * @see #getRuntimeStateRepresentation()
	 * @generated
	 */
	EAttribute getRuntimeStateRepresentation_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation#getVariables <em>Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variables</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation#getVariables()
	 * @see #getRuntimeStateRepresentation()
	 * @generated
	 */
	EReference getRuntimeStateRepresentation_Variables();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation#getRepresentations <em>Representations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Representations</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation#getRepresentations()
	 * @see #getRuntimeStateRepresentation()
	 * @generated
	 */
	EReference getRuntimeStateRepresentation_Representations();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImportJavaStatement <em>Import Java Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import Java Statement</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImportJavaStatement
	 * @generated
	 */
	EClass getImportJavaStatement();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImportJavaStatement#getImportedNamespace <em>Imported Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Imported Namespace</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImportJavaStatement#getImportedNamespace()
	 * @see #getImportJavaStatement()
	 * @generated
	 */
	EAttribute getImportJavaStatement_ImportedNamespace();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Variable_old <em>Variable old</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable old</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Variable_old
	 * @generated
	 */
	EClass getVariable_old();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Constant <em>Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Constant
	 * @generated
	 */
	EClass getConstant();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.IntegerLiteral <em>Integer Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Literal</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.IntegerLiteral
	 * @generated
	 */
	EClass getIntegerLiteral();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.IntegerLiteral#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.IntegerLiteral#getValue()
	 * @see #getIntegerLiteral()
	 * @generated
	 */
	EAttribute getIntegerLiteral_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.VariableRef <em>Variable Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Ref</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.VariableRef
	 * @generated
	 */
	EClass getVariableRef();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.VariableRef#getVar <em>Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Var</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.VariableRef#getVar()
	 * @see #getVariableRef()
	 * @generated
	 */
	EReference getVariableRef_Var();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringLiteral <em>String Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Literal</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringLiteral
	 * @generated
	 */
	EClass getStringLiteral();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringLiteral#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringLiteral#getValue()
	 * @see #getStringLiteral()
	 * @generated
	 */
	EAttribute getStringLiteral_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.EObjectRef <em>EObject Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EObject Ref</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.EObjectRef
	 * @generated
	 */
	EClass getEObjectRef();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.EObjectRef#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.EObjectRef#getObject()
	 * @see #getEObjectRef()
	 * @generated
	 */
	EReference getEObjectRef_Object();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringDecoration <em>String Decoration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Decoration</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringDecoration
	 * @generated
	 */
	EClass getStringDecoration();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringDecoration#getPrefix <em>Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Prefix</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringDecoration#getPrefix()
	 * @see #getStringDecoration()
	 * @generated
	 */
	EAttribute getStringDecoration_Prefix();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringDecoration#getSuffix <em>Suffix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Suffix</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringDecoration#getSuffix()
	 * @see #getStringDecoration()
	 * @generated
	 */
	EAttribute getStringDecoration_Suffix();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringDecoration#getStringLiteral <em>String Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>String Literal</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringDecoration#getStringLiteral()
	 * @see #getStringDecoration()
	 * @generated
	 */
	EReference getStringDecoration_StringLiteral();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Representation <em>Representation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Representation</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Representation
	 * @generated
	 */
	EClass getRepresentation();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Representation#getImage <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Image</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Representation#getImage()
	 * @see #getRepresentation()
	 * @generated
	 */
	EReference getRepresentation_Image();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Representation#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Text</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Representation#getText()
	 * @see #getRepresentation()
	 * @generated
	 */
	EReference getRepresentation_Text();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint <em>Paint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Paint</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint
	 * @generated
	 */
	EClass getPaint();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getType()
	 * @see #getPaint()
	 * @generated
	 */
	EAttribute getPaint_Type();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Color</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getColor()
	 * @see #getPaint()
	 * @generated
	 */
	EReference getPaint_Color();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getOnConcept <em>On Concept</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>On Concept</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getOnConcept()
	 * @see #getPaint()
	 * @generated
	 */
	EReference getPaint_OnConcept();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getOwnedUpdateStrategy <em>Owned Update Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Owned Update Strategy</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getOwnedUpdateStrategy()
	 * @see #getPaint()
	 * @generated
	 */
	EReference getPaint_OwnedUpdateStrategy();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getPersistent <em>Persistent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Persistent</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getPersistent()
	 * @see #getPaint()
	 * @generated
	 */
	EAttribute getPaint_Persistent();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RColor <em>RColor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>RColor</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RColor
	 * @generated
	 */
	EClass getRColor();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RColor#getRed <em>Red</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Red</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RColor#getRed()
	 * @see #getRColor()
	 * @generated
	 */
	EAttribute getRColor_Red();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RColor#getBlue <em>Blue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Blue</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RColor#getBlue()
	 * @see #getRColor()
	 * @generated
	 */
	EAttribute getRColor_Blue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RColor#getGreen <em>Green</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Green</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RColor#getGreen()
	 * @see #getRColor()
	 * @generated
	 */
	EAttribute getRColor_Green();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Hover <em>Hover</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hover</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Hover
	 * @generated
	 */
	EClass getHover();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Hover#getFromStart <em>From Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>From Start</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Hover#getFromStart()
	 * @see #getHover()
	 * @generated
	 */
	EAttribute getHover_FromStart();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Mark <em>Mark</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mark</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Mark
	 * @generated
	 */
	EClass getMark();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Mark#getMarkerType <em>Marker Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Marker Type</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Mark#getMarkerType()
	 * @see #getMark()
	 * @generated
	 */
	EAttribute getMark_MarkerType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Label <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Label</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Label
	 * @generated
	 */
	EClass getLabel();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImageVar <em>Image Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Image Var</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImageVar
	 * @generated
	 */
	EClass getImageVar();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImageVar#getPath <em>Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Path</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImageVar#getPath()
	 * @see #getImageVar()
	 * @generated
	 */
	EAttribute getImageVar_Path();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RTDVar <em>RTD Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>RTD Var</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RTDVar
	 * @generated
	 */
	EClass getRTDVar();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RTDVar#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Attributes</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RTDVar#getAttributes()
	 * @see #getRTDVar()
	 * @generated
	 */
	EAttribute getRTDVar_Attributes();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ConstTextVar <em>Const Text Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Const Text Var</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ConstTextVar
	 * @generated
	 */
	EClass getConstTextVar();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ConstTextVar#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ConstTextVar#getValue()
	 * @see #getConstTextVar()
	 * @generated
	 */
	EAttribute getConstTextVar_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar <em>Chart Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Chart Var</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar
	 * @generated
	 */
	EClass getChartVar();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getNbValuesDisplayed <em>Nb Values Displayed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nb Values Displayed</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getNbValuesDisplayed()
	 * @see #getChartVar()
	 * @generated
	 */
	EAttribute getChartVar_NbValuesDisplayed();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getXlabel <em>Xlabel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Xlabel</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getXlabel()
	 * @see #getChartVar()
	 * @generated
	 */
	EAttribute getChartVar_Xlabel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getYlabel <em>Ylabel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ylabel</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getYlabel()
	 * @see #getChartVar()
	 * @generated
	 */
	EAttribute getChartVar_Ylabel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getTitle()
	 * @see #getChartVar()
	 * @generated
	 */
	EAttribute getChartVar_Title();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getXrtd <em>Xrtd</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Xrtd</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getXrtd()
	 * @see #getChartVar()
	 * @generated
	 */
	EReference getChartVar_Xrtd();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getYrtds <em>Yrtds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Yrtds</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getYrtds()
	 * @see #getChartVar()
	 * @generated
	 */
	EReference getChartVar_Yrtds();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getLineStyle <em>Line Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Line Style</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getLineStyle()
	 * @see #getChartVar()
	 * @generated
	 */
	EAttribute getChartVar_LineStyle();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RUpdateStrategy <em>RUpdate Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>RUpdate Strategy</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RUpdateStrategy
	 * @generated
	 */
	EClass getRUpdateStrategy();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RUpdateStrategy#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Events</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RUpdateStrategy#getEvents()
	 * @see #getRUpdateStrategy()
	 * @generated
	 */
	EReference getRUpdateStrategy_Events();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RUpdateStrategy#getStepExecution <em>Step Execution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Step Execution</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RUpdateStrategy#getStepExecution()
	 * @see #getRUpdateStrategy()
	 * @generated
	 */
	EAttribute getRUpdateStrategy_StepExecution();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar <em>Decorated Text Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Decorated Text Var</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar
	 * @generated
	 */
	EClass getDecoratedTextVar();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getPrefix <em>Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Prefix</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getPrefix()
	 * @see #getDecoratedTextVar()
	 * @generated
	 */
	EAttribute getDecoratedTextVar_Prefix();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getSuffix <em>Suffix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Suffix</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getSuffix()
	 * @see #getDecoratedTextVar()
	 * @generated
	 */
	EAttribute getDecoratedTextVar_Suffix();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Color</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getColor()
	 * @see #getDecoratedTextVar()
	 * @generated
	 */
	EReference getDecoratedTextVar_Color();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getFont <em>Font</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Font</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getFont()
	 * @see #getDecoratedTextVar()
	 * @generated
	 */
	EAttribute getDecoratedTextVar_Font();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getFontStyle <em>Font Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Font Style</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getFontStyle()
	 * @see #getDecoratedTextVar()
	 * @generated
	 */
	EAttribute getDecoratedTextVar_FontStyle();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getFontSize <em>Font Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Font Size</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getFontSize()
	 * @see #getDecoratedTextVar()
	 * @generated
	 */
	EAttribute getDecoratedTextVar_FontSize();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getDecorated <em>Decorated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Decorated</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getDecorated()
	 * @see #getDecoratedTextVar()
	 * @generated
	 */
	EReference getDecoratedTextVar_Decorated();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getErrorColor <em>Error Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Error Color</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getErrorColor()
	 * @see #getDecoratedTextVar()
	 * @generated
	 */
	EReference getDecoratedTextVar_ErrorColor();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getErrorText <em>Error Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Error Text</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getErrorText()
	 * @see #getDecoratedTextVar()
	 * @generated
	 */
	EAttribute getDecoratedTextVar_ErrorText();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Variable
	 * @generated
	 */
	EClass getVariable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Variable#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Variable#getName()
	 * @see #getVariable()
	 * @generated
	 */
	EAttribute getVariable_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Variable#getStrategy <em>Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Strategy</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Variable#getStrategy()
	 * @see #getVariable()
	 * @generated
	 */
	EReference getVariable_Strategy();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.TextVar <em>Text Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Text Var</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.TextVar
	 * @generated
	 */
	EClass getTextVar();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Image <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Image</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Image
	 * @generated
	 */
	EClass getImage();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Image#getInitialSize <em>Initial Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Size</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Image#getInitialSize()
	 * @see #getImage()
	 * @generated
	 */
	EAttribute getImage_InitialSize();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Image#getDepth <em>Depth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Depth</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Image#getDepth()
	 * @see #getImage()
	 * @generated
	 */
	EAttribute getImage_Depth();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack <em>Stack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stack</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack
	 * @generated
	 */
	EClass getStack();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getMaxLength <em>Max Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Length</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getMaxLength()
	 * @see #getStack()
	 * @generated
	 */
	EAttribute getStack_MaxLength();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getIsVertical <em>Is Vertical</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Vertical</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getIsVertical()
	 * @see #getStack()
	 * @generated
	 */
	EAttribute getStack_IsVertical();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getShape <em>Shape</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Shape</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getShape()
	 * @see #getStack()
	 * @generated
	 */
	EAttribute getStack_Shape();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getStrategy <em>Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Strategy</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getStrategy()
	 * @see #getStack()
	 * @generated
	 */
	EAttribute getStack_Strategy();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getDepth <em>Depth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Depth</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getDepth()
	 * @see #getStack()
	 * @generated
	 */
	EAttribute getStack_Depth();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getTitle()
	 * @see #getStack()
	 * @generated
	 */
	EAttribute getStack_Title();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RStrategy <em>RStrategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>RStrategy</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RStrategy
	 * @generated
	 */
	EClass getRStrategy();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RStrategy#getOnConcept <em>On Concept</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>On Concept</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RStrategy#getOnConcept()
	 * @see #getRStrategy()
	 * @generated
	 */
	EReference getRStrategy_OnConcept();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RStrategy#getOwnedUpdateStrategy <em>Owned Update Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Owned Update Strategy</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RStrategy#getOwnedUpdateStrategy()
	 * @see #getRStrategy()
	 * @generated
	 */
	EReference getRStrategy_OwnedUpdateStrategy();

	/**
	 * Returns the meta object for class '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Comment <em>Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Comment</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Comment
	 * @generated
	 */
	EClass getComment();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.PaintType <em>Paint Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Paint Type</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.PaintType
	 * @generated
	 */
	EEnum getPaintType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StepExecution <em>Step Execution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Step Execution</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StepExecution
	 * @generated
	 */
	EEnum getStepExecution();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.FontStyle <em>Font Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Font Style</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.FontStyle
	 * @generated
	 */
	EEnum getFontStyle();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.MarkerType <em>Marker Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Marker Type</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.MarkerType
	 * @generated
	 */
	EEnum getMarkerType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackShape <em>Stack Shape</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Stack Shape</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackShape
	 * @generated
	 */
	EEnum getStackShape();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackStrategy <em>Stack Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Stack Strategy</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackStrategy
	 * @generated
	 */
	EEnum getStackStrategy();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Boolean <em>Boolean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Boolean</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Boolean
	 * @generated
	 */
	EEnum getBoolean();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean <em>RBoolean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>RBoolean</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean
	 * @generated
	 */
	EEnum getRBoolean();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.LineStyle <em>Line Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Line Style</em>'.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.LineStyle
	 * @generated
	 */
	EEnum getLineStyle();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RunstarFactory getRunstarFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ObjectVariableImpl <em>Object Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ObjectVariableImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getObjectVariable()
		 * @generated
		 */
		EClass OBJECT_VARIABLE = eINSTANCE.getObjectVariable();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBJECT_VARIABLE__NAME = eINSTANCE.getObjectVariable_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_VARIABLE__TYPE = eINSTANCE.getObjectVariable_Type();

		/**
		 * The meta object literal for the '<em><b>Initial Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBJECT_VARIABLE__INITIAL_VALUE = eINSTANCE.getObjectVariable_InitialValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RuntimeStateRepresentationImpl <em>Runtime State Representation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RuntimeStateRepresentationImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getRuntimeStateRepresentation()
		 * @generated
		 */
		EClass RUNTIME_STATE_REPRESENTATION = eINSTANCE.getRuntimeStateRepresentation();

		/**
		 * The meta object literal for the '<em><b>Model Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_STATE_REPRESENTATION__MODEL_IMPORTS = eINSTANCE.getRuntimeStateRepresentation_ModelImports();

		/**
		 * The meta object literal for the '<em><b>Class Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_STATE_REPRESENTATION__CLASS_IMPORTS = eINSTANCE.getRuntimeStateRepresentation_ClassImports();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RUNTIME_STATE_REPRESENTATION__NAME = eINSTANCE.getRuntimeStateRepresentation_Name();

		/**
		 * The meta object literal for the '<em><b>Variables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_STATE_REPRESENTATION__VARIABLES = eINSTANCE.getRuntimeStateRepresentation_Variables();

		/**
		 * The meta object literal for the '<em><b>Representations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_STATE_REPRESENTATION__REPRESENTATIONS = eINSTANCE.getRuntimeStateRepresentation_Representations();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ImportJavaStatementImpl <em>Import Java Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ImportJavaStatementImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getImportJavaStatement()
		 * @generated
		 */
		EClass IMPORT_JAVA_STATEMENT = eINSTANCE.getImportJavaStatement();

		/**
		 * The meta object literal for the '<em><b>Imported Namespace</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT_JAVA_STATEMENT__IMPORTED_NAMESPACE = eINSTANCE.getImportJavaStatement_ImportedNamespace();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.Variable_oldImpl <em>Variable old</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.Variable_oldImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getVariable_old()
		 * @generated
		 */
		EClass VARIABLE_OLD = eINSTANCE.getVariable_old();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ConstantImpl <em>Constant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ConstantImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getConstant()
		 * @generated
		 */
		EClass CONSTANT = eINSTANCE.getConstant();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.IntegerLiteralImpl <em>Integer Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.IntegerLiteralImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getIntegerLiteral()
		 * @generated
		 */
		EClass INTEGER_LITERAL = eINSTANCE.getIntegerLiteral();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_LITERAL__VALUE = eINSTANCE.getIntegerLiteral_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.VariableRefImpl <em>Variable Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.VariableRefImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getVariableRef()
		 * @generated
		 */
		EClass VARIABLE_REF = eINSTANCE.getVariableRef();

		/**
		 * The meta object literal for the '<em><b>Var</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_REF__VAR = eINSTANCE.getVariableRef_Var();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StringLiteralImpl <em>String Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StringLiteralImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getStringLiteral()
		 * @generated
		 */
		EClass STRING_LITERAL = eINSTANCE.getStringLiteral();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_LITERAL__VALUE = eINSTANCE.getStringLiteral_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.EObjectRefImpl <em>EObject Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.EObjectRefImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getEObjectRef()
		 * @generated
		 */
		EClass EOBJECT_REF = eINSTANCE.getEObjectRef();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EOBJECT_REF__OBJECT = eINSTANCE.getEObjectRef_Object();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StringDecorationImpl <em>String Decoration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StringDecorationImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getStringDecoration()
		 * @generated
		 */
		EClass STRING_DECORATION = eINSTANCE.getStringDecoration();

		/**
		 * The meta object literal for the '<em><b>Prefix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_DECORATION__PREFIX = eINSTANCE.getStringDecoration_Prefix();

		/**
		 * The meta object literal for the '<em><b>Suffix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_DECORATION__SUFFIX = eINSTANCE.getStringDecoration_Suffix();

		/**
		 * The meta object literal for the '<em><b>String Literal</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRING_DECORATION__STRING_LITERAL = eINSTANCE.getStringDecoration_StringLiteral();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RepresentationImpl <em>Representation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RepresentationImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getRepresentation()
		 * @generated
		 */
		EClass REPRESENTATION = eINSTANCE.getRepresentation();

		/**
		 * The meta object literal for the '<em><b>Image</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPRESENTATION__IMAGE = eINSTANCE.getRepresentation_Image();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPRESENTATION__TEXT = eINSTANCE.getRepresentation_Text();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.PaintImpl <em>Paint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.PaintImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getPaint()
		 * @generated
		 */
		EClass PAINT = eINSTANCE.getPaint();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PAINT__TYPE = eINSTANCE.getPaint_Type();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PAINT__COLOR = eINSTANCE.getPaint_Color();

		/**
		 * The meta object literal for the '<em><b>On Concept</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PAINT__ON_CONCEPT = eINSTANCE.getPaint_OnConcept();

		/**
		 * The meta object literal for the '<em><b>Owned Update Strategy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PAINT__OWNED_UPDATE_STRATEGY = eINSTANCE.getPaint_OwnedUpdateStrategy();

		/**
		 * The meta object literal for the '<em><b>Persistent</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PAINT__PERSISTENT = eINSTANCE.getPaint_Persistent();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RColorImpl <em>RColor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RColorImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getRColor()
		 * @generated
		 */
		EClass RCOLOR = eINSTANCE.getRColor();

		/**
		 * The meta object literal for the '<em><b>Red</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RCOLOR__RED = eINSTANCE.getRColor_Red();

		/**
		 * The meta object literal for the '<em><b>Blue</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RCOLOR__BLUE = eINSTANCE.getRColor_Blue();

		/**
		 * The meta object literal for the '<em><b>Green</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RCOLOR__GREEN = eINSTANCE.getRColor_Green();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.HoverImpl <em>Hover</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.HoverImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getHover()
		 * @generated
		 */
		EClass HOVER = eINSTANCE.getHover();

		/**
		 * The meta object literal for the '<em><b>From Start</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HOVER__FROM_START = eINSTANCE.getHover_FromStart();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.MarkImpl <em>Mark</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.MarkImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getMark()
		 * @generated
		 */
		EClass MARK = eINSTANCE.getMark();

		/**
		 * The meta object literal for the '<em><b>Marker Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MARK__MARKER_TYPE = eINSTANCE.getMark_MarkerType();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.LabelImpl <em>Label</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.LabelImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getLabel()
		 * @generated
		 */
		EClass LABEL = eINSTANCE.getLabel();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ImageVarImpl <em>Image Var</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ImageVarImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getImageVar()
		 * @generated
		 */
		EClass IMAGE_VAR = eINSTANCE.getImageVar();

		/**
		 * The meta object literal for the '<em><b>Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMAGE_VAR__PATH = eINSTANCE.getImageVar_Path();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RTDVarImpl <em>RTD Var</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RTDVarImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getRTDVar()
		 * @generated
		 */
		EClass RTD_VAR = eINSTANCE.getRTDVar();

		/**
		 * The meta object literal for the '<em><b>Attributes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTD_VAR__ATTRIBUTES = eINSTANCE.getRTDVar_Attributes();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ConstTextVarImpl <em>Const Text Var</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ConstTextVarImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getConstTextVar()
		 * @generated
		 */
		EClass CONST_TEXT_VAR = eINSTANCE.getConstTextVar();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONST_TEXT_VAR__VALUE = eINSTANCE.getConstTextVar_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ChartVarImpl <em>Chart Var</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ChartVarImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getChartVar()
		 * @generated
		 */
		EClass CHART_VAR = eINSTANCE.getChartVar();

		/**
		 * The meta object literal for the '<em><b>Nb Values Displayed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHART_VAR__NB_VALUES_DISPLAYED = eINSTANCE.getChartVar_NbValuesDisplayed();

		/**
		 * The meta object literal for the '<em><b>Xlabel</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHART_VAR__XLABEL = eINSTANCE.getChartVar_Xlabel();

		/**
		 * The meta object literal for the '<em><b>Ylabel</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHART_VAR__YLABEL = eINSTANCE.getChartVar_Ylabel();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHART_VAR__TITLE = eINSTANCE.getChartVar_Title();

		/**
		 * The meta object literal for the '<em><b>Xrtd</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_VAR__XRTD = eINSTANCE.getChartVar_Xrtd();

		/**
		 * The meta object literal for the '<em><b>Yrtds</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHART_VAR__YRTDS = eINSTANCE.getChartVar_Yrtds();

		/**
		 * The meta object literal for the '<em><b>Line Style</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHART_VAR__LINE_STYLE = eINSTANCE.getChartVar_LineStyle();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RUpdateStrategyImpl <em>RUpdate Strategy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RUpdateStrategyImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getRUpdateStrategy()
		 * @generated
		 */
		EClass RUPDATE_STRATEGY = eINSTANCE.getRUpdateStrategy();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUPDATE_STRATEGY__EVENTS = eINSTANCE.getRUpdateStrategy_Events();

		/**
		 * The meta object literal for the '<em><b>Step Execution</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RUPDATE_STRATEGY__STEP_EXECUTION = eINSTANCE.getRUpdateStrategy_StepExecution();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.DecoratedTextVarImpl <em>Decorated Text Var</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.DecoratedTextVarImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getDecoratedTextVar()
		 * @generated
		 */
		EClass DECORATED_TEXT_VAR = eINSTANCE.getDecoratedTextVar();

		/**
		 * The meta object literal for the '<em><b>Prefix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECORATED_TEXT_VAR__PREFIX = eINSTANCE.getDecoratedTextVar_Prefix();

		/**
		 * The meta object literal for the '<em><b>Suffix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECORATED_TEXT_VAR__SUFFIX = eINSTANCE.getDecoratedTextVar_Suffix();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATED_TEXT_VAR__COLOR = eINSTANCE.getDecoratedTextVar_Color();

		/**
		 * The meta object literal for the '<em><b>Font</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECORATED_TEXT_VAR__FONT = eINSTANCE.getDecoratedTextVar_Font();

		/**
		 * The meta object literal for the '<em><b>Font Style</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECORATED_TEXT_VAR__FONT_STYLE = eINSTANCE.getDecoratedTextVar_FontStyle();

		/**
		 * The meta object literal for the '<em><b>Font Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECORATED_TEXT_VAR__FONT_SIZE = eINSTANCE.getDecoratedTextVar_FontSize();

		/**
		 * The meta object literal for the '<em><b>Decorated</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATED_TEXT_VAR__DECORATED = eINSTANCE.getDecoratedTextVar_Decorated();

		/**
		 * The meta object literal for the '<em><b>Error Color</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATED_TEXT_VAR__ERROR_COLOR = eINSTANCE.getDecoratedTextVar_ErrorColor();

		/**
		 * The meta object literal for the '<em><b>Error Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECORATED_TEXT_VAR__ERROR_TEXT = eINSTANCE.getDecoratedTextVar_ErrorText();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.VariableImpl <em>Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.VariableImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getVariable()
		 * @generated
		 */
		EClass VARIABLE = eINSTANCE.getVariable();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE__NAME = eINSTANCE.getVariable_Name();

		/**
		 * The meta object literal for the '<em><b>Strategy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE__STRATEGY = eINSTANCE.getVariable_Strategy();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.TextVarImpl <em>Text Var</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.TextVarImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getTextVar()
		 * @generated
		 */
		EClass TEXT_VAR = eINSTANCE.getTextVar();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ImageImpl <em>Image</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.ImageImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getImage()
		 * @generated
		 */
		EClass IMAGE = eINSTANCE.getImage();

		/**
		 * The meta object literal for the '<em><b>Initial Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMAGE__INITIAL_SIZE = eINSTANCE.getImage_InitialSize();

		/**
		 * The meta object literal for the '<em><b>Depth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMAGE__DEPTH = eINSTANCE.getImage_Depth();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StackImpl <em>Stack</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StackImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getStack()
		 * @generated
		 */
		EClass STACK = eINSTANCE.getStack();

		/**
		 * The meta object literal for the '<em><b>Max Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STACK__MAX_LENGTH = eINSTANCE.getStack_MaxLength();

		/**
		 * The meta object literal for the '<em><b>Is Vertical</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STACK__IS_VERTICAL = eINSTANCE.getStack_IsVertical();

		/**
		 * The meta object literal for the '<em><b>Shape</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STACK__SHAPE = eINSTANCE.getStack_Shape();

		/**
		 * The meta object literal for the '<em><b>Strategy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STACK__STRATEGY = eINSTANCE.getStack_Strategy();

		/**
		 * The meta object literal for the '<em><b>Depth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STACK__DEPTH = eINSTANCE.getStack_Depth();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STACK__TITLE = eINSTANCE.getStack_Title();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RStrategyImpl <em>RStrategy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RStrategyImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getRStrategy()
		 * @generated
		 */
		EClass RSTRATEGY = eINSTANCE.getRStrategy();

		/**
		 * The meta object literal for the '<em><b>On Concept</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RSTRATEGY__ON_CONCEPT = eINSTANCE.getRStrategy_OnConcept();

		/**
		 * The meta object literal for the '<em><b>Owned Update Strategy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RSTRATEGY__OWNED_UPDATE_STRATEGY = eINSTANCE.getRStrategy_OwnedUpdateStrategy();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.CommentImpl <em>Comment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.CommentImpl
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getComment()
		 * @generated
		 */
		EClass COMMENT = eINSTANCE.getComment();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.PaintType <em>Paint Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.PaintType
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getPaintType()
		 * @generated
		 */
		EEnum PAINT_TYPE = eINSTANCE.getPaintType();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StepExecution <em>Step Execution</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StepExecution
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getStepExecution()
		 * @generated
		 */
		EEnum STEP_EXECUTION = eINSTANCE.getStepExecution();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.FontStyle <em>Font Style</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.FontStyle
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getFontStyle()
		 * @generated
		 */
		EEnum FONT_STYLE = eINSTANCE.getFontStyle();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.MarkerType <em>Marker Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.MarkerType
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getMarkerType()
		 * @generated
		 */
		EEnum MARKER_TYPE = eINSTANCE.getMarkerType();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackShape <em>Stack Shape</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackShape
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getStackShape()
		 * @generated
		 */
		EEnum STACK_SHAPE = eINSTANCE.getStackShape();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackStrategy <em>Stack Strategy</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackStrategy
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getStackStrategy()
		 * @generated
		 */
		EEnum STACK_STRATEGY = eINSTANCE.getStackStrategy();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Boolean <em>Boolean</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Boolean
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getBoolean()
		 * @generated
		 */
		EEnum BOOLEAN = eINSTANCE.getBoolean();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean <em>RBoolean</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getRBoolean()
		 * @generated
		 */
		EEnum RBOOLEAN = eINSTANCE.getRBoolean();

		/**
		 * The meta object literal for the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.LineStyle <em>Line Style</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.LineStyle
		 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RunstarPackageImpl#getLineStyle()
		 * @generated
		 */
		EEnum LINE_STYLE = eINSTANCE.getLineStyle();

	}

} //RunstarPackage
