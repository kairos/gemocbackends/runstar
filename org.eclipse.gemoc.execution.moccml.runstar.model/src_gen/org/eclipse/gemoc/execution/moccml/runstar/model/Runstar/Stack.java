/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stack</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getMaxLength <em>Max Length</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getIsVertical <em>Is Vertical</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getShape <em>Shape</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getStrategy <em>Strategy</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getDepth <em>Depth</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getTitle <em>Title</em>}</li>
 * </ul>
 *
 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getStack()
 * @model
 * @generated
 */
public interface Stack extends Representation {
	/**
	 * Returns the value of the '<em><b>Max Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Length</em>' attribute.
	 * @see #setMaxLength(int)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getStack_MaxLength()
	 * @model
	 * @generated
	 */
	int getMaxLength();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getMaxLength <em>Max Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Length</em>' attribute.
	 * @see #getMaxLength()
	 * @generated
	 */
	void setMaxLength(int value);

	/**
	 * Returns the value of the '<em><b>Is Vertical</b></em>' attribute.
	 * The default value is <code>"FALSE"</code>.
	 * The literals are from the enumeration {@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Vertical</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean
	 * @see #setIsVertical(RBoolean)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getStack_IsVertical()
	 * @model default="FALSE"
	 * @generated
	 */
	RBoolean getIsVertical();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getIsVertical <em>Is Vertical</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Vertical</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean
	 * @see #getIsVertical()
	 * @generated
	 */
	void setIsVertical(RBoolean value);

	/**
	 * Returns the value of the '<em><b>Shape</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackShape}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shape</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackShape
	 * @see #setShape(StackShape)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getStack_Shape()
	 * @model
	 * @generated
	 */
	StackShape getShape();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getShape <em>Shape</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shape</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackShape
	 * @see #getShape()
	 * @generated
	 */
	void setShape(StackShape value);

	/**
	 * Returns the value of the '<em><b>Strategy</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackStrategy}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strategy</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackStrategy
	 * @see #setStrategy(StackStrategy)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getStack_Strategy()
	 * @model
	 * @generated
	 */
	StackStrategy getStrategy();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getStrategy <em>Strategy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Strategy</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackStrategy
	 * @see #getStrategy()
	 * @generated
	 */
	void setStrategy(StackStrategy value);

	/**
	 * Returns the value of the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Depth</em>' attribute.
	 * @see #setDepth(int)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getStack_Depth()
	 * @model
	 * @generated
	 */
	int getDepth();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getDepth <em>Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Depth</em>' attribute.
	 * @see #getDepth()
	 * @generated
	 */
	void setDepth(int value);

	/**
	 * Returns the value of the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' attribute.
	 * @see #setTitle(String)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getStack_Title()
	 * @model
	 * @generated
	 */
	String getTitle();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack#getTitle <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' attribute.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(String value);

} // Stack
