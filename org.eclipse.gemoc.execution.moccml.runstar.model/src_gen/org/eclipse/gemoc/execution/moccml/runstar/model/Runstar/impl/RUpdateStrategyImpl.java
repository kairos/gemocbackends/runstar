/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RUpdateStrategy;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StepExecution;

import org.eclipse.ocl.xtext.completeoclcs.DefPropertyCS;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>RUpdate Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RUpdateStrategyImpl#getEvents <em>Events</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RUpdateStrategyImpl#getStepExecution <em>Step Execution</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RUpdateStrategyImpl extends EObjectImpl implements RUpdateStrategy {
	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<DefPropertyCS> events;

	/**
	 * The default value of the '{@link #getStepExecution() <em>Step Execution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStepExecution()
	 * @generated
	 * @ordered
	 */
	protected static final StepExecution STEP_EXECUTION_EDEFAULT = StepExecution.AFTER;

	/**
	 * The cached value of the '{@link #getStepExecution() <em>Step Execution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStepExecution()
	 * @generated
	 * @ordered
	 */
	protected StepExecution stepExecution = STEP_EXECUTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RUpdateStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RunstarPackage.Literals.RUPDATE_STRATEGY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DefPropertyCS> getEvents() {
		if (events == null) {
			events = new EObjectResolvingEList<DefPropertyCS>(DefPropertyCS.class, this, RunstarPackage.RUPDATE_STRATEGY__EVENTS);
		}
		return events;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StepExecution getStepExecution() {
		return stepExecution;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStepExecution(StepExecution newStepExecution) {
		StepExecution oldStepExecution = stepExecution;
		stepExecution = newStepExecution == null ? STEP_EXECUTION_EDEFAULT : newStepExecution;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.RUPDATE_STRATEGY__STEP_EXECUTION, oldStepExecution, stepExecution));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RunstarPackage.RUPDATE_STRATEGY__EVENTS:
				return getEvents();
			case RunstarPackage.RUPDATE_STRATEGY__STEP_EXECUTION:
				return getStepExecution();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RunstarPackage.RUPDATE_STRATEGY__EVENTS:
				getEvents().clear();
				getEvents().addAll((Collection<? extends DefPropertyCS>)newValue);
				return;
			case RunstarPackage.RUPDATE_STRATEGY__STEP_EXECUTION:
				setStepExecution((StepExecution)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RunstarPackage.RUPDATE_STRATEGY__EVENTS:
				getEvents().clear();
				return;
			case RunstarPackage.RUPDATE_STRATEGY__STEP_EXECUTION:
				setStepExecution(STEP_EXECUTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RunstarPackage.RUPDATE_STRATEGY__EVENTS:
				return events != null && !events.isEmpty();
			case RunstarPackage.RUPDATE_STRATEGY__STEP_EXECUTION:
				return stepExecution != STEP_EXECUTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (stepExecution: ");
		result.append(stepExecution);
		result.append(')');
		return result.toString();
	}

} //RUpdateStrategyImpl
