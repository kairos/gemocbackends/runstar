/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Comment;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ConstTextVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Constant;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.EObjectRef;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.FontStyle;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Hover;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Image;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImageVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImportJavaStatement;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.IntegerLiteral;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Label;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.LineStyle;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Mark;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.MarkerType;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.PaintType;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RColor;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RStrategy;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RTDVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RUpdateStrategy;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarFactory;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackShape;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackStrategy;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StepExecution;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringDecoration;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringLiteral;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.VariableRef;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RunstarFactoryImpl extends EFactoryImpl implements RunstarFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RunstarFactory init() {
		try {
			RunstarFactory theRunstarFactory = (RunstarFactory)EPackage.Registry.INSTANCE.getEFactory(RunstarPackage.eNS_URI);
			if (theRunstarFactory != null) {
				return theRunstarFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new RunstarFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RunstarFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case RunstarPackage.OBJECT_VARIABLE: return createObjectVariable();
			case RunstarPackage.RUNTIME_STATE_REPRESENTATION: return createRuntimeStateRepresentation();
			case RunstarPackage.IMPORT_JAVA_STATEMENT: return createImportJavaStatement();
			case RunstarPackage.CONSTANT: return createConstant();
			case RunstarPackage.INTEGER_LITERAL: return createIntegerLiteral();
			case RunstarPackage.VARIABLE_REF: return createVariableRef();
			case RunstarPackage.STRING_LITERAL: return createStringLiteral();
			case RunstarPackage.EOBJECT_REF: return createEObjectRef();
			case RunstarPackage.STRING_DECORATION: return createStringDecoration();
			case RunstarPackage.PAINT: return createPaint();
			case RunstarPackage.RCOLOR: return createRColor();
			case RunstarPackage.HOVER: return createHover();
			case RunstarPackage.MARK: return createMark();
			case RunstarPackage.LABEL: return createLabel();
			case RunstarPackage.IMAGE_VAR: return createImageVar();
			case RunstarPackage.RTD_VAR: return createRTDVar();
			case RunstarPackage.CONST_TEXT_VAR: return createConstTextVar();
			case RunstarPackage.CHART_VAR: return createChartVar();
			case RunstarPackage.RUPDATE_STRATEGY: return createRUpdateStrategy();
			case RunstarPackage.DECORATED_TEXT_VAR: return createDecoratedTextVar();
			case RunstarPackage.IMAGE: return createImage();
			case RunstarPackage.STACK: return createStack();
			case RunstarPackage.RSTRATEGY: return createRStrategy();
			case RunstarPackage.COMMENT: return createComment();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case RunstarPackage.PAINT_TYPE:
				return createPaintTypeFromString(eDataType, initialValue);
			case RunstarPackage.STEP_EXECUTION:
				return createStepExecutionFromString(eDataType, initialValue);
			case RunstarPackage.FONT_STYLE:
				return createFontStyleFromString(eDataType, initialValue);
			case RunstarPackage.MARKER_TYPE:
				return createMarkerTypeFromString(eDataType, initialValue);
			case RunstarPackage.STACK_SHAPE:
				return createStackShapeFromString(eDataType, initialValue);
			case RunstarPackage.STACK_STRATEGY:
				return createStackStrategyFromString(eDataType, initialValue);
			case RunstarPackage.BOOLEAN:
				return createBooleanFromString(eDataType, initialValue);
			case RunstarPackage.RBOOLEAN:
				return createRBooleanFromString(eDataType, initialValue);
			case RunstarPackage.LINE_STYLE:
				return createLineStyleFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case RunstarPackage.PAINT_TYPE:
				return convertPaintTypeToString(eDataType, instanceValue);
			case RunstarPackage.STEP_EXECUTION:
				return convertStepExecutionToString(eDataType, instanceValue);
			case RunstarPackage.FONT_STYLE:
				return convertFontStyleToString(eDataType, instanceValue);
			case RunstarPackage.MARKER_TYPE:
				return convertMarkerTypeToString(eDataType, instanceValue);
			case RunstarPackage.STACK_SHAPE:
				return convertStackShapeToString(eDataType, instanceValue);
			case RunstarPackage.STACK_STRATEGY:
				return convertStackStrategyToString(eDataType, instanceValue);
			case RunstarPackage.BOOLEAN:
				return convertBooleanToString(eDataType, instanceValue);
			case RunstarPackage.RBOOLEAN:
				return convertRBooleanToString(eDataType, instanceValue);
			case RunstarPackage.LINE_STYLE:
				return convertLineStyleToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ObjectVariable createObjectVariable() {
		ObjectVariableImpl objectVariable = new ObjectVariableImpl();
		return objectVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RuntimeStateRepresentation createRuntimeStateRepresentation() {
		RuntimeStateRepresentationImpl runtimeStateRepresentation = new RuntimeStateRepresentationImpl();
		return runtimeStateRepresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ImportJavaStatement createImportJavaStatement() {
		ImportJavaStatementImpl importJavaStatement = new ImportJavaStatementImpl();
		return importJavaStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Constant createConstant() {
		ConstantImpl constant = new ConstantImpl();
		return constant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IntegerLiteral createIntegerLiteral() {
		IntegerLiteralImpl integerLiteral = new IntegerLiteralImpl();
		return integerLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VariableRef createVariableRef() {
		VariableRefImpl variableRef = new VariableRefImpl();
		return variableRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StringLiteral createStringLiteral() {
		StringLiteralImpl stringLiteral = new StringLiteralImpl();
		return stringLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObjectRef createEObjectRef() {
		EObjectRefImpl eObjectRef = new EObjectRefImpl();
		return eObjectRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StringDecoration createStringDecoration() {
		StringDecorationImpl stringDecoration = new StringDecorationImpl();
		return stringDecoration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Paint createPaint() {
		PaintImpl paint = new PaintImpl();
		return paint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RColor createRColor() {
		RColorImpl rColor = new RColorImpl();
		return rColor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Hover createHover() {
		HoverImpl hover = new HoverImpl();
		return hover;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Mark createMark() {
		MarkImpl mark = new MarkImpl();
		return mark;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Label createLabel() {
		LabelImpl label = new LabelImpl();
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ImageVar createImageVar() {
		ImageVarImpl imageVar = new ImageVarImpl();
		return imageVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTDVar createRTDVar() {
		RTDVarImpl rtdVar = new RTDVarImpl();
		return rtdVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConstTextVar createConstTextVar() {
		ConstTextVarImpl constTextVar = new ConstTextVarImpl();
		return constTextVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ChartVar createChartVar() {
		ChartVarImpl chartVar = new ChartVarImpl();
		return chartVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RUpdateStrategy createRUpdateStrategy() {
		RUpdateStrategyImpl rUpdateStrategy = new RUpdateStrategyImpl();
		return rUpdateStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DecoratedTextVar createDecoratedTextVar() {
		DecoratedTextVarImpl decoratedTextVar = new DecoratedTextVarImpl();
		return decoratedTextVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Image createImage() {
		ImageImpl image = new ImageImpl();
		return image;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Stack createStack() {
		StackImpl stack = new StackImpl();
		return stack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RStrategy createRStrategy() {
		RStrategyImpl rStrategy = new RStrategyImpl();
		return rStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Comment createComment() {
		CommentImpl comment = new CommentImpl();
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PaintType createPaintTypeFromString(EDataType eDataType, String initialValue) {
		PaintType result = PaintType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPaintTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StepExecution createStepExecutionFromString(EDataType eDataType, String initialValue) {
		StepExecution result = StepExecution.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStepExecutionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FontStyle createFontStyleFromString(EDataType eDataType, String initialValue) {
		FontStyle result = FontStyle.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFontStyleToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MarkerType createMarkerTypeFromString(EDataType eDataType, String initialValue) {
		MarkerType result = MarkerType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMarkerTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StackShape createStackShapeFromString(EDataType eDataType, String initialValue) {
		StackShape result = StackShape.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStackShapeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StackStrategy createStackStrategyFromString(EDataType eDataType, String initialValue) {
		StackStrategy result = StackStrategy.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStackStrategyToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Boolean createBooleanFromString(EDataType eDataType, String initialValue) {
		org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Boolean result = org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Boolean.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBooleanToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RBoolean createRBooleanFromString(EDataType eDataType, String initialValue) {
		RBoolean result = RBoolean.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRBooleanToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LineStyle createLineStyleFromString(EDataType eDataType, String initialValue) {
		LineStyle result = LineStyle.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLineStyleToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RunstarPackage getRunstarPackage() {
		return (RunstarPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static RunstarPackage getPackage() {
		return RunstarPackage.eINSTANCE;
	}

} //RunstarFactoryImpl
