/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.xtext.common.types.JvmTypeReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable#getInitialValue <em>Initial Value</em>}</li>
 * </ul>
 *
 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getObjectVariable()
 * @model
 * @generated
 */
public interface ObjectVariable extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getObjectVariable_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' containment reference.
	 * @see #setType(JvmTypeReference)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getObjectVariable_Type()
	 * @model containment="true" required="true"
	 * @generated
	 */
	JvmTypeReference getType();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable#getType <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' containment reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(JvmTypeReference value);

	/**
	 * Returns the value of the '<em><b>Initial Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Value</em>' attribute.
	 * @see #setInitialValue(int)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getObjectVariable_InitialValue()
	 * @model
	 * @generated
	 */
	int getInitialValue();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable#getInitialValue <em>Initial Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Value</em>' attribute.
	 * @see #getInitialValue()
	 * @generated
	 */
	void setInitialValue(int value);

} // ObjectVariable
