/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackShape;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackStrategy;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Stack</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StackImpl#getMaxLength <em>Max Length</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StackImpl#getIsVertical <em>Is Vertical</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StackImpl#getShape <em>Shape</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StackImpl#getStrategy <em>Strategy</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StackImpl#getDepth <em>Depth</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.StackImpl#getTitle <em>Title</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StackImpl extends RepresentationImpl implements Stack {
	/**
	 * The default value of the '{@link #getMaxLength() <em>Max Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxLength()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_LENGTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMaxLength() <em>Max Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxLength()
	 * @generated
	 * @ordered
	 */
	protected int maxLength = MAX_LENGTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getIsVertical() <em>Is Vertical</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsVertical()
	 * @generated
	 * @ordered
	 */
	protected static final RBoolean IS_VERTICAL_EDEFAULT = RBoolean.FALSE;

	/**
	 * The cached value of the '{@link #getIsVertical() <em>Is Vertical</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsVertical()
	 * @generated
	 * @ordered
	 */
	protected RBoolean isVertical = IS_VERTICAL_EDEFAULT;

	/**
	 * The default value of the '{@link #getShape() <em>Shape</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShape()
	 * @generated
	 * @ordered
	 */
	protected static final StackShape SHAPE_EDEFAULT = StackShape.RECTANGLE;

	/**
	 * The cached value of the '{@link #getShape() <em>Shape</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShape()
	 * @generated
	 * @ordered
	 */
	protected StackShape shape = SHAPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getStrategy() <em>Strategy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStrategy()
	 * @generated
	 * @ordered
	 */
	protected static final StackStrategy STRATEGY_EDEFAULT = StackStrategy.LASTS;

	/**
	 * The cached value of the '{@link #getStrategy() <em>Strategy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStrategy()
	 * @generated
	 * @ordered
	 */
	protected StackStrategy strategy = STRATEGY_EDEFAULT;

	/**
	 * The default value of the '{@link #getDepth() <em>Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDepth()
	 * @generated
	 * @ordered
	 */
	protected static final int DEPTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDepth() <em>Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDepth()
	 * @generated
	 * @ordered
	 */
	protected int depth = DEPTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected static final String TITLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected String title = TITLE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StackImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RunstarPackage.Literals.STACK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMaxLength() {
		return maxLength;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMaxLength(int newMaxLength) {
		int oldMaxLength = maxLength;
		maxLength = newMaxLength;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.STACK__MAX_LENGTH, oldMaxLength, maxLength));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RBoolean getIsVertical() {
		return isVertical;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIsVertical(RBoolean newIsVertical) {
		RBoolean oldIsVertical = isVertical;
		isVertical = newIsVertical == null ? IS_VERTICAL_EDEFAULT : newIsVertical;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.STACK__IS_VERTICAL, oldIsVertical, isVertical));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StackShape getShape() {
		return shape;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setShape(StackShape newShape) {
		StackShape oldShape = shape;
		shape = newShape == null ? SHAPE_EDEFAULT : newShape;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.STACK__SHAPE, oldShape, shape));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StackStrategy getStrategy() {
		return strategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStrategy(StackStrategy newStrategy) {
		StackStrategy oldStrategy = strategy;
		strategy = newStrategy == null ? STRATEGY_EDEFAULT : newStrategy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.STACK__STRATEGY, oldStrategy, strategy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getDepth() {
		return depth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDepth(int newDepth) {
		int oldDepth = depth;
		depth = newDepth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.STACK__DEPTH, oldDepth, depth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTitle() {
		return title;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTitle(String newTitle) {
		String oldTitle = title;
		title = newTitle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.STACK__TITLE, oldTitle, title));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RunstarPackage.STACK__MAX_LENGTH:
				return getMaxLength();
			case RunstarPackage.STACK__IS_VERTICAL:
				return getIsVertical();
			case RunstarPackage.STACK__SHAPE:
				return getShape();
			case RunstarPackage.STACK__STRATEGY:
				return getStrategy();
			case RunstarPackage.STACK__DEPTH:
				return getDepth();
			case RunstarPackage.STACK__TITLE:
				return getTitle();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RunstarPackage.STACK__MAX_LENGTH:
				setMaxLength((Integer)newValue);
				return;
			case RunstarPackage.STACK__IS_VERTICAL:
				setIsVertical((RBoolean)newValue);
				return;
			case RunstarPackage.STACK__SHAPE:
				setShape((StackShape)newValue);
				return;
			case RunstarPackage.STACK__STRATEGY:
				setStrategy((StackStrategy)newValue);
				return;
			case RunstarPackage.STACK__DEPTH:
				setDepth((Integer)newValue);
				return;
			case RunstarPackage.STACK__TITLE:
				setTitle((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RunstarPackage.STACK__MAX_LENGTH:
				setMaxLength(MAX_LENGTH_EDEFAULT);
				return;
			case RunstarPackage.STACK__IS_VERTICAL:
				setIsVertical(IS_VERTICAL_EDEFAULT);
				return;
			case RunstarPackage.STACK__SHAPE:
				setShape(SHAPE_EDEFAULT);
				return;
			case RunstarPackage.STACK__STRATEGY:
				setStrategy(STRATEGY_EDEFAULT);
				return;
			case RunstarPackage.STACK__DEPTH:
				setDepth(DEPTH_EDEFAULT);
				return;
			case RunstarPackage.STACK__TITLE:
				setTitle(TITLE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RunstarPackage.STACK__MAX_LENGTH:
				return maxLength != MAX_LENGTH_EDEFAULT;
			case RunstarPackage.STACK__IS_VERTICAL:
				return isVertical != IS_VERTICAL_EDEFAULT;
			case RunstarPackage.STACK__SHAPE:
				return shape != SHAPE_EDEFAULT;
			case RunstarPackage.STACK__STRATEGY:
				return strategy != STRATEGY_EDEFAULT;
			case RunstarPackage.STACK__DEPTH:
				return depth != DEPTH_EDEFAULT;
			case RunstarPackage.STACK__TITLE:
				return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (maxLength: ");
		result.append(maxLength);
		result.append(", isVertical: ");
		result.append(isVertical);
		result.append(", shape: ");
		result.append(shape);
		result.append(", strategy: ");
		result.append(strategy);
		result.append(", depth: ");
		result.append(depth);
		result.append(", title: ");
		result.append(title);
		result.append(')');
		return result.toString();
	}

} //StackImpl
