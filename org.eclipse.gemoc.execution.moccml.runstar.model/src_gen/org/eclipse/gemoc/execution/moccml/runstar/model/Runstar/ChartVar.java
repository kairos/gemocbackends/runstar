/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Chart Var</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getNbValuesDisplayed <em>Nb Values Displayed</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getXlabel <em>Xlabel</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getYlabel <em>Ylabel</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getTitle <em>Title</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getXrtd <em>Xrtd</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getYrtds <em>Yrtds</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getLineStyle <em>Line Style</em>}</li>
 * </ul>
 *
 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getChartVar()
 * @model
 * @generated
 */
public interface ChartVar extends ImageVar {
	/**
	 * Returns the value of the '<em><b>Nb Values Displayed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nb Values Displayed</em>' attribute.
	 * @see #setNbValuesDisplayed(int)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getChartVar_NbValuesDisplayed()
	 * @model
	 * @generated
	 */
	int getNbValuesDisplayed();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getNbValuesDisplayed <em>Nb Values Displayed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nb Values Displayed</em>' attribute.
	 * @see #getNbValuesDisplayed()
	 * @generated
	 */
	void setNbValuesDisplayed(int value);

	/**
	 * Returns the value of the '<em><b>Xlabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Xlabel</em>' attribute.
	 * @see #setXlabel(String)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getChartVar_Xlabel()
	 * @model
	 * @generated
	 */
	String getXlabel();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getXlabel <em>Xlabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Xlabel</em>' attribute.
	 * @see #getXlabel()
	 * @generated
	 */
	void setXlabel(String value);

	/**
	 * Returns the value of the '<em><b>Ylabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ylabel</em>' attribute.
	 * @see #setYlabel(String)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getChartVar_Ylabel()
	 * @model
	 * @generated
	 */
	String getYlabel();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getYlabel <em>Ylabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ylabel</em>' attribute.
	 * @see #getYlabel()
	 * @generated
	 */
	void setYlabel(String value);

	/**
	 * Returns the value of the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' attribute.
	 * @see #setTitle(String)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getChartVar_Title()
	 * @model
	 * @generated
	 */
	String getTitle();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getTitle <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' attribute.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(String value);

	/**
	 * Returns the value of the '<em><b>Xrtd</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Xrtd</em>' reference.
	 * @see #setXrtd(RTDVar)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getChartVar_Xrtd()
	 * @model required="true"
	 * @generated
	 */
	RTDVar getXrtd();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getXrtd <em>Xrtd</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Xrtd</em>' reference.
	 * @see #getXrtd()
	 * @generated
	 */
	void setXrtd(RTDVar value);

	/**
	 * Returns the value of the '<em><b>Yrtds</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RTDVar}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Yrtds</em>' reference list.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getChartVar_Yrtds()
	 * @model required="true"
	 * @generated
	 */
	EList<RTDVar> getYrtds();

	/**
	 * Returns the value of the '<em><b>Line Style</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.LineStyle}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Line Style</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.LineStyle
	 * @see #setLineStyle(LineStyle)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getChartVar_LineStyle()
	 * @model
	 * @generated
	 */
	LineStyle getLineStyle();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar#getLineStyle <em>Line Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Line Style</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.LineStyle
	 * @see #getLineStyle()
	 * @generated
	 */
	void setLineStyle(LineStyle value);

} // ChartVar
