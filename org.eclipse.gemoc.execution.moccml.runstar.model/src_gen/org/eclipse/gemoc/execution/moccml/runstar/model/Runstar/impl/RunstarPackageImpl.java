/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.TimeModelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ChartVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Comment;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ConstTextVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Constant;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.EObjectRef;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.FontStyle;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Hover;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Image;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImageVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImportJavaStatement;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.IntegerLiteral;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Label;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.LineStyle;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Mark;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.MarkerType;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ObjectVariable;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.PaintType;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RColor;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RStrategy;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RTDVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RUpdateStrategy;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Representation;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarFactory;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Stack;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackShape;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StackStrategy;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StepExecution;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringDecoration;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StringLiteral;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.TextVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Variable;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.VariableRef;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Variable_old;

import org.eclipse.ocl.pivot.PivotPackage;

import org.eclipse.ocl.xtext.basecs.BaseCSPackage;

import org.eclipse.ocl.xtext.completeoclcs.CompleteOCLCSPackage;

import org.eclipse.ocl.xtext.essentialoclcs.EssentialOCLCSPackage;

import org.eclipse.xtext.common.types.TypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RunstarPackageImpl extends EPackageImpl implements RunstarPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass objectVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass runtimeStateRepresentationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass importJavaStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variable_oldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass integerLiteralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableRefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringLiteralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eObjectRefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringDecorationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass representationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass paintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rColorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hoverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass markEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass labelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass imageVarEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rtdVarEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constTextVarEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass chartVarEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rUpdateStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass decoratedTextVarEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass textVarEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass imageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stackEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass commentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum paintTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum stepExecutionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum fontStyleEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum markerTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum stackShapeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum stackStrategyEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum booleanEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum rBooleanEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum lineStyleEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RunstarPackageImpl() {
		super(eNS_URI, RunstarFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link RunstarPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RunstarPackage init() {
		if (isInited) return (RunstarPackage)EPackage.Registry.INSTANCE.getEPackage(RunstarPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredRunstarPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		RunstarPackageImpl theRunstarPackage = registeredRunstarPackage instanceof RunstarPackageImpl ? (RunstarPackageImpl)registeredRunstarPackage : new RunstarPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		BaseCSPackage.eINSTANCE.eClass();
		CompleteOCLCSPackage.eINSTANCE.eClass();
		EssentialOCLCSPackage.eINSTANCE.eClass();
		PivotPackage.eINSTANCE.eClass();
		TimeModelPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theRunstarPackage.createPackageContents();

		// Initialize created meta-data
		theRunstarPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRunstarPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RunstarPackage.eNS_URI, theRunstarPackage);
		return theRunstarPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getObjectVariable() {
		return objectVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getObjectVariable_Name() {
		return (EAttribute)objectVariableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getObjectVariable_Type() {
		return (EReference)objectVariableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getObjectVariable_InitialValue() {
		return (EAttribute)objectVariableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRuntimeStateRepresentation() {
		return runtimeStateRepresentationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRuntimeStateRepresentation_ModelImports() {
		return (EReference)runtimeStateRepresentationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRuntimeStateRepresentation_ClassImports() {
		return (EReference)runtimeStateRepresentationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRuntimeStateRepresentation_Name() {
		return (EAttribute)runtimeStateRepresentationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRuntimeStateRepresentation_Variables() {
		return (EReference)runtimeStateRepresentationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRuntimeStateRepresentation_Representations() {
		return (EReference)runtimeStateRepresentationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getImportJavaStatement() {
		return importJavaStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getImportJavaStatement_ImportedNamespace() {
		return (EAttribute)importJavaStatementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVariable_old() {
		return variable_oldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getConstant() {
		return constantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIntegerLiteral() {
		return integerLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIntegerLiteral_Value() {
		return (EAttribute)integerLiteralEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVariableRef() {
		return variableRefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVariableRef_Var() {
		return (EReference)variableRefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStringLiteral() {
		return stringLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStringLiteral_Value() {
		return (EAttribute)stringLiteralEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEObjectRef() {
		return eObjectRefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEObjectRef_Object() {
		return (EReference)eObjectRefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStringDecoration() {
		return stringDecorationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStringDecoration_Prefix() {
		return (EAttribute)stringDecorationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStringDecoration_Suffix() {
		return (EAttribute)stringDecorationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getStringDecoration_StringLiteral() {
		return (EReference)stringDecorationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRepresentation() {
		return representationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRepresentation_Image() {
		return (EReference)representationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRepresentation_Text() {
		return (EReference)representationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPaint() {
		return paintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPaint_Type() {
		return (EAttribute)paintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPaint_Color() {
		return (EReference)paintEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPaint_OnConcept() {
		return (EReference)paintEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPaint_OwnedUpdateStrategy() {
		return (EReference)paintEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPaint_Persistent() {
		return (EAttribute)paintEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRColor() {
		return rColorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRColor_Red() {
		return (EAttribute)rColorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRColor_Blue() {
		return (EAttribute)rColorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRColor_Green() {
		return (EAttribute)rColorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHover() {
		return hoverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHover_FromStart() {
		return (EAttribute)hoverEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMark() {
		return markEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMark_MarkerType() {
		return (EAttribute)markEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLabel() {
		return labelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getImageVar() {
		return imageVarEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getImageVar_Path() {
		return (EAttribute)imageVarEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRTDVar() {
		return rtdVarEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRTDVar_Attributes() {
		return (EAttribute)rtdVarEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getConstTextVar() {
		return constTextVarEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConstTextVar_Value() {
		return (EAttribute)constTextVarEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getChartVar() {
		return chartVarEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getChartVar_NbValuesDisplayed() {
		return (EAttribute)chartVarEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getChartVar_Xlabel() {
		return (EAttribute)chartVarEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getChartVar_Ylabel() {
		return (EAttribute)chartVarEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getChartVar_Title() {
		return (EAttribute)chartVarEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getChartVar_Xrtd() {
		return (EReference)chartVarEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getChartVar_Yrtds() {
		return (EReference)chartVarEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getChartVar_LineStyle() {
		return (EAttribute)chartVarEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRUpdateStrategy() {
		return rUpdateStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRUpdateStrategy_Events() {
		return (EReference)rUpdateStrategyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRUpdateStrategy_StepExecution() {
		return (EAttribute)rUpdateStrategyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDecoratedTextVar() {
		return decoratedTextVarEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDecoratedTextVar_Prefix() {
		return (EAttribute)decoratedTextVarEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDecoratedTextVar_Suffix() {
		return (EAttribute)decoratedTextVarEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDecoratedTextVar_Color() {
		return (EReference)decoratedTextVarEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDecoratedTextVar_Font() {
		return (EAttribute)decoratedTextVarEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDecoratedTextVar_FontStyle() {
		return (EAttribute)decoratedTextVarEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDecoratedTextVar_FontSize() {
		return (EAttribute)decoratedTextVarEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDecoratedTextVar_Decorated() {
		return (EReference)decoratedTextVarEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDecoratedTextVar_ErrorColor() {
		return (EReference)decoratedTextVarEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDecoratedTextVar_ErrorText() {
		return (EAttribute)decoratedTextVarEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVariable() {
		return variableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVariable_Name() {
		return (EAttribute)variableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVariable_Strategy() {
		return (EReference)variableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTextVar() {
		return textVarEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getImage() {
		return imageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getImage_InitialSize() {
		return (EAttribute)imageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getImage_Depth() {
		return (EAttribute)imageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStack() {
		return stackEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStack_MaxLength() {
		return (EAttribute)stackEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStack_IsVertical() {
		return (EAttribute)stackEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStack_Shape() {
		return (EAttribute)stackEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStack_Strategy() {
		return (EAttribute)stackEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStack_Depth() {
		return (EAttribute)stackEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStack_Title() {
		return (EAttribute)stackEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRStrategy() {
		return rStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRStrategy_OnConcept() {
		return (EReference)rStrategyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRStrategy_OwnedUpdateStrategy() {
		return (EReference)rStrategyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getComment() {
		return commentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getPaintType() {
		return paintTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getStepExecution() {
		return stepExecutionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getFontStyle() {
		return fontStyleEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getMarkerType() {
		return markerTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getStackShape() {
		return stackShapeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getStackStrategy() {
		return stackStrategyEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getBoolean() {
		return booleanEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getRBoolean() {
		return rBooleanEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getLineStyle() {
		return lineStyleEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RunstarFactory getRunstarFactory() {
		return (RunstarFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		objectVariableEClass = createEClass(OBJECT_VARIABLE);
		createEAttribute(objectVariableEClass, OBJECT_VARIABLE__NAME);
		createEReference(objectVariableEClass, OBJECT_VARIABLE__TYPE);
		createEAttribute(objectVariableEClass, OBJECT_VARIABLE__INITIAL_VALUE);

		runtimeStateRepresentationEClass = createEClass(RUNTIME_STATE_REPRESENTATION);
		createEReference(runtimeStateRepresentationEClass, RUNTIME_STATE_REPRESENTATION__MODEL_IMPORTS);
		createEReference(runtimeStateRepresentationEClass, RUNTIME_STATE_REPRESENTATION__CLASS_IMPORTS);
		createEAttribute(runtimeStateRepresentationEClass, RUNTIME_STATE_REPRESENTATION__NAME);
		createEReference(runtimeStateRepresentationEClass, RUNTIME_STATE_REPRESENTATION__VARIABLES);
		createEReference(runtimeStateRepresentationEClass, RUNTIME_STATE_REPRESENTATION__REPRESENTATIONS);

		importJavaStatementEClass = createEClass(IMPORT_JAVA_STATEMENT);
		createEAttribute(importJavaStatementEClass, IMPORT_JAVA_STATEMENT__IMPORTED_NAMESPACE);

		variable_oldEClass = createEClass(VARIABLE_OLD);

		constantEClass = createEClass(CONSTANT);

		integerLiteralEClass = createEClass(INTEGER_LITERAL);
		createEAttribute(integerLiteralEClass, INTEGER_LITERAL__VALUE);

		variableRefEClass = createEClass(VARIABLE_REF);
		createEReference(variableRefEClass, VARIABLE_REF__VAR);

		stringLiteralEClass = createEClass(STRING_LITERAL);
		createEAttribute(stringLiteralEClass, STRING_LITERAL__VALUE);

		eObjectRefEClass = createEClass(EOBJECT_REF);
		createEReference(eObjectRefEClass, EOBJECT_REF__OBJECT);

		stringDecorationEClass = createEClass(STRING_DECORATION);
		createEAttribute(stringDecorationEClass, STRING_DECORATION__PREFIX);
		createEAttribute(stringDecorationEClass, STRING_DECORATION__SUFFIX);
		createEReference(stringDecorationEClass, STRING_DECORATION__STRING_LITERAL);

		representationEClass = createEClass(REPRESENTATION);
		createEReference(representationEClass, REPRESENTATION__IMAGE);
		createEReference(representationEClass, REPRESENTATION__TEXT);

		paintEClass = createEClass(PAINT);
		createEAttribute(paintEClass, PAINT__TYPE);
		createEReference(paintEClass, PAINT__COLOR);
		createEReference(paintEClass, PAINT__ON_CONCEPT);
		createEReference(paintEClass, PAINT__OWNED_UPDATE_STRATEGY);
		createEAttribute(paintEClass, PAINT__PERSISTENT);

		rColorEClass = createEClass(RCOLOR);
		createEAttribute(rColorEClass, RCOLOR__RED);
		createEAttribute(rColorEClass, RCOLOR__BLUE);
		createEAttribute(rColorEClass, RCOLOR__GREEN);

		hoverEClass = createEClass(HOVER);
		createEAttribute(hoverEClass, HOVER__FROM_START);

		markEClass = createEClass(MARK);
		createEAttribute(markEClass, MARK__MARKER_TYPE);

		labelEClass = createEClass(LABEL);

		imageVarEClass = createEClass(IMAGE_VAR);
		createEAttribute(imageVarEClass, IMAGE_VAR__PATH);

		rtdVarEClass = createEClass(RTD_VAR);
		createEAttribute(rtdVarEClass, RTD_VAR__ATTRIBUTES);

		constTextVarEClass = createEClass(CONST_TEXT_VAR);
		createEAttribute(constTextVarEClass, CONST_TEXT_VAR__VALUE);

		chartVarEClass = createEClass(CHART_VAR);
		createEAttribute(chartVarEClass, CHART_VAR__NB_VALUES_DISPLAYED);
		createEAttribute(chartVarEClass, CHART_VAR__XLABEL);
		createEAttribute(chartVarEClass, CHART_VAR__YLABEL);
		createEAttribute(chartVarEClass, CHART_VAR__TITLE);
		createEReference(chartVarEClass, CHART_VAR__XRTD);
		createEReference(chartVarEClass, CHART_VAR__YRTDS);
		createEAttribute(chartVarEClass, CHART_VAR__LINE_STYLE);

		rUpdateStrategyEClass = createEClass(RUPDATE_STRATEGY);
		createEReference(rUpdateStrategyEClass, RUPDATE_STRATEGY__EVENTS);
		createEAttribute(rUpdateStrategyEClass, RUPDATE_STRATEGY__STEP_EXECUTION);

		decoratedTextVarEClass = createEClass(DECORATED_TEXT_VAR);
		createEAttribute(decoratedTextVarEClass, DECORATED_TEXT_VAR__PREFIX);
		createEAttribute(decoratedTextVarEClass, DECORATED_TEXT_VAR__SUFFIX);
		createEReference(decoratedTextVarEClass, DECORATED_TEXT_VAR__COLOR);
		createEAttribute(decoratedTextVarEClass, DECORATED_TEXT_VAR__FONT);
		createEAttribute(decoratedTextVarEClass, DECORATED_TEXT_VAR__FONT_STYLE);
		createEAttribute(decoratedTextVarEClass, DECORATED_TEXT_VAR__FONT_SIZE);
		createEReference(decoratedTextVarEClass, DECORATED_TEXT_VAR__DECORATED);
		createEReference(decoratedTextVarEClass, DECORATED_TEXT_VAR__ERROR_COLOR);
		createEAttribute(decoratedTextVarEClass, DECORATED_TEXT_VAR__ERROR_TEXT);

		variableEClass = createEClass(VARIABLE);
		createEAttribute(variableEClass, VARIABLE__NAME);
		createEReference(variableEClass, VARIABLE__STRATEGY);

		textVarEClass = createEClass(TEXT_VAR);

		imageEClass = createEClass(IMAGE);
		createEAttribute(imageEClass, IMAGE__INITIAL_SIZE);
		createEAttribute(imageEClass, IMAGE__DEPTH);

		stackEClass = createEClass(STACK);
		createEAttribute(stackEClass, STACK__MAX_LENGTH);
		createEAttribute(stackEClass, STACK__IS_VERTICAL);
		createEAttribute(stackEClass, STACK__SHAPE);
		createEAttribute(stackEClass, STACK__STRATEGY);
		createEAttribute(stackEClass, STACK__DEPTH);
		createEAttribute(stackEClass, STACK__TITLE);

		rStrategyEClass = createEClass(RSTRATEGY);
		createEReference(rStrategyEClass, RSTRATEGY__ON_CONCEPT);
		createEReference(rStrategyEClass, RSTRATEGY__OWNED_UPDATE_STRATEGY);

		commentEClass = createEClass(COMMENT);

		// Create enums
		paintTypeEEnum = createEEnum(PAINT_TYPE);
		stepExecutionEEnum = createEEnum(STEP_EXECUTION);
		fontStyleEEnum = createEEnum(FONT_STYLE);
		markerTypeEEnum = createEEnum(MARKER_TYPE);
		stackShapeEEnum = createEEnum(STACK_SHAPE);
		stackStrategyEEnum = createEEnum(STACK_STRATEGY);
		booleanEEnum = createEEnum(BOOLEAN);
		rBooleanEEnum = createEEnum(RBOOLEAN);
		lineStyleEEnum = createEEnum(LINE_STYLE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		TimeModelPackage theTimeModelPackage = (TimeModelPackage)EPackage.Registry.INSTANCE.getEPackage(TimeModelPackage.eNS_URI);
		CompleteOCLCSPackage theCompleteOCLCSPackage = (CompleteOCLCSPackage)EPackage.Registry.INSTANCE.getEPackage(CompleteOCLCSPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		constantEClass.getESuperTypes().add(this.getVariable_old());
		integerLiteralEClass.getESuperTypes().add(this.getConstant());
		variableRefEClass.getESuperTypes().add(this.getVariable_old());
		stringLiteralEClass.getESuperTypes().add(this.getConstant());
		eObjectRefEClass.getESuperTypes().add(this.getVariable_old());
		stringDecorationEClass.getESuperTypes().add(this.getConstant());
		paintEClass.getESuperTypes().add(this.getRepresentation());
		hoverEClass.getESuperTypes().add(this.getRepresentation());
		markEClass.getESuperTypes().add(this.getRepresentation());
		labelEClass.getESuperTypes().add(this.getRepresentation());
		imageVarEClass.getESuperTypes().add(this.getVariable());
		rtdVarEClass.getESuperTypes().add(this.getTextVar());
		constTextVarEClass.getESuperTypes().add(this.getTextVar());
		chartVarEClass.getESuperTypes().add(this.getImageVar());
		decoratedTextVarEClass.getESuperTypes().add(this.getTextVar());
		textVarEClass.getESuperTypes().add(this.getVariable());
		imageEClass.getESuperTypes().add(this.getRepresentation());
		stackEClass.getESuperTypes().add(this.getRepresentation());
		commentEClass.getESuperTypes().add(this.getRepresentation());

		// Initialize classes and features; add operations and parameters
		initEClass(objectVariableEClass, ObjectVariable.class, "ObjectVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getObjectVariable_Name(), ecorePackage.getEString(), "name", null, 1, 1, ObjectVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObjectVariable_Type(), theTypesPackage.getJvmTypeReference(), null, "type", null, 1, 1, ObjectVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getObjectVariable_InitialValue(), ecorePackage.getEInt(), "initialValue", null, 0, 1, ObjectVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(runtimeStateRepresentationEClass, RuntimeStateRepresentation.class, "RuntimeStateRepresentation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRuntimeStateRepresentation_ModelImports(), theTimeModelPackage.getImportStatement(), null, "modelImports", null, 0, -1, RuntimeStateRepresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeStateRepresentation_ClassImports(), this.getImportJavaStatement(), null, "classImports", null, 0, -1, RuntimeStateRepresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRuntimeStateRepresentation_Name(), ecorePackage.getEString(), "name", null, 0, 1, RuntimeStateRepresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeStateRepresentation_Variables(), this.getVariable(), null, "variables", null, 0, -1, RuntimeStateRepresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeStateRepresentation_Representations(), this.getRepresentation(), null, "representations", null, 0, -1, RuntimeStateRepresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(importJavaStatementEClass, ImportJavaStatement.class, "ImportJavaStatement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImportJavaStatement_ImportedNamespace(), ecorePackage.getEString(), "importedNamespace", null, 1, 1, ImportJavaStatement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(variable_oldEClass, Variable_old.class, "Variable_old", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(constantEClass, Constant.class, "Constant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(integerLiteralEClass, IntegerLiteral.class, "IntegerLiteral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntegerLiteral_Value(), ecorePackage.getEInt(), "value", null, 0, 1, IntegerLiteral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(variableRefEClass, VariableRef.class, "VariableRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVariableRef_Var(), this.getObjectVariable(), null, "var", null, 1, 1, VariableRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringLiteralEClass, StringLiteral.class, "StringLiteral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringLiteral_Value(), ecorePackage.getEString(), "value", null, 0, 1, StringLiteral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eObjectRefEClass, EObjectRef.class, "EObjectRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEObjectRef_Object(), ecorePackage.getEObject(), null, "object", null, 1, 1, EObjectRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringDecorationEClass, StringDecoration.class, "StringDecoration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringDecoration_Prefix(), ecorePackage.getEString(), "prefix", null, 0, 1, StringDecoration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStringDecoration_Suffix(), ecorePackage.getEString(), "suffix", null, 0, 1, StringDecoration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStringDecoration_StringLiteral(), this.getStringLiteral(), null, "stringLiteral", null, 1, 1, StringDecoration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(representationEClass, Representation.class, "Representation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRepresentation_Image(), this.getImageVar(), null, "image", null, 0, 1, Representation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRepresentation_Text(), this.getTextVar(), null, "text", null, 0, 1, Representation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(paintEClass, Paint.class, "Paint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPaint_Type(), this.getPaintType(), "type", null, 0, 1, Paint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPaint_Color(), this.getRColor(), null, "color", null, 0, 1, Paint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPaint_OnConcept(), ecorePackage.getEObject(), null, "onConcept", null, 0, 1, Paint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPaint_OwnedUpdateStrategy(), this.getRUpdateStrategy(), null, "ownedUpdateStrategy", null, 0, 1, Paint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPaint_Persistent(), this.getRBoolean(), "persistent", null, 0, 1, Paint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rColorEClass, RColor.class, "RColor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRColor_Red(), ecorePackage.getEInt(), "red", null, 0, 1, RColor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRColor_Blue(), ecorePackage.getEInt(), "blue", null, 0, 1, RColor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRColor_Green(), ecorePackage.getEInt(), "green", null, 0, 1, RColor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hoverEClass, Hover.class, "Hover", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHover_FromStart(), this.getRBoolean(), "fromStart", null, 0, 1, Hover.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(markEClass, Mark.class, "Mark", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMark_MarkerType(), this.getMarkerType(), "markerType", null, 0, 1, Mark.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(labelEClass, Label.class, "Label", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(imageVarEClass, ImageVar.class, "ImageVar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImageVar_Path(), ecorePackage.getEString(), "path", null, 0, 1, ImageVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rtdVarEClass, RTDVar.class, "RTDVar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRTDVar_Attributes(), ecorePackage.getEString(), "attributes", null, 1, -1, RTDVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constTextVarEClass, ConstTextVar.class, "ConstTextVar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConstTextVar_Value(), ecorePackage.getEString(), "value", null, 0, 1, ConstTextVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(chartVarEClass, ChartVar.class, "ChartVar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getChartVar_NbValuesDisplayed(), ecorePackage.getEInt(), "nbValuesDisplayed", null, 0, 1, ChartVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getChartVar_Xlabel(), ecorePackage.getEString(), "xlabel", null, 0, 1, ChartVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getChartVar_Ylabel(), ecorePackage.getEString(), "ylabel", null, 0, 1, ChartVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getChartVar_Title(), ecorePackage.getEString(), "title", null, 0, 1, ChartVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartVar_Xrtd(), this.getRTDVar(), null, "xrtd", null, 1, 1, ChartVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChartVar_Yrtds(), this.getRTDVar(), null, "yrtds", null, 1, -1, ChartVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getChartVar_LineStyle(), this.getLineStyle(), "lineStyle", null, 0, 1, ChartVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rUpdateStrategyEClass, RUpdateStrategy.class, "RUpdateStrategy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRUpdateStrategy_Events(), theCompleteOCLCSPackage.getDefPropertyCS(), null, "events", null, 0, 2, RUpdateStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRUpdateStrategy_StepExecution(), this.getStepExecution(), "stepExecution", null, 0, 1, RUpdateStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(decoratedTextVarEClass, DecoratedTextVar.class, "DecoratedTextVar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDecoratedTextVar_Prefix(), ecorePackage.getEString(), "prefix", null, 0, 1, DecoratedTextVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDecoratedTextVar_Suffix(), ecorePackage.getEString(), "suffix", null, 0, 1, DecoratedTextVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDecoratedTextVar_Color(), this.getRColor(), null, "color", null, 0, 1, DecoratedTextVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDecoratedTextVar_Font(), ecorePackage.getEString(), "font", null, 0, 1, DecoratedTextVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDecoratedTextVar_FontStyle(), this.getFontStyle(), "fontStyle", null, 0, 1, DecoratedTextVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDecoratedTextVar_FontSize(), ecorePackage.getEInt(), "fontSize", null, 0, 1, DecoratedTextVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDecoratedTextVar_Decorated(), this.getTextVar(), null, "decorated", null, 1, 1, DecoratedTextVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDecoratedTextVar_ErrorColor(), this.getRColor(), null, "errorColor", null, 0, 1, DecoratedTextVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDecoratedTextVar_ErrorText(), ecorePackage.getEString(), "errorText", null, 0, 1, DecoratedTextVar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(variableEClass, Variable.class, "Variable", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVariable_Name(), ecorePackage.getEString(), "name", null, 0, 1, Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVariable_Strategy(), this.getRStrategy(), null, "strategy", null, 0, 1, Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(textVarEClass, TextVar.class, "TextVar", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(imageEClass, Image.class, "Image", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImage_InitialSize(), ecorePackage.getEInt(), "initialSize", null, 0, 1, Image.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImage_Depth(), ecorePackage.getEInt(), "depth", null, 0, 1, Image.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stackEClass, Stack.class, "Stack", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStack_MaxLength(), ecorePackage.getEInt(), "maxLength", null, 0, 1, Stack.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStack_IsVertical(), this.getRBoolean(), "isVertical", "FALSE", 0, 1, Stack.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStack_Shape(), this.getStackShape(), "shape", null, 0, 1, Stack.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStack_Strategy(), this.getStackStrategy(), "strategy", null, 0, 1, Stack.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStack_Depth(), ecorePackage.getEInt(), "depth", null, 0, 1, Stack.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStack_Title(), ecorePackage.getEString(), "title", null, 0, 1, Stack.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rStrategyEClass, RStrategy.class, "RStrategy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRStrategy_OnConcept(), ecorePackage.getEObject(), null, "onConcept", null, 1, 1, RStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRStrategy_OwnedUpdateStrategy(), this.getRUpdateStrategy(), null, "ownedUpdateStrategy", null, 0, 1, RStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(commentEClass, Comment.class, "Comment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(paintTypeEEnum, PaintType.class, "PaintType");
		addEEnumLiteral(paintTypeEEnum, PaintType.NONE);
		addEEnumLiteral(paintTypeEEnum, PaintType.BACKGROUND);
		addEEnumLiteral(paintTypeEEnum, PaintType.FOREGROUND);

		initEEnum(stepExecutionEEnum, StepExecution.class, "StepExecution");
		addEEnumLiteral(stepExecutionEEnum, StepExecution.AFTER);
		addEEnumLiteral(stepExecutionEEnum, StepExecution.BEFORE);

		initEEnum(fontStyleEEnum, FontStyle.class, "FontStyle");
		addEEnumLiteral(fontStyleEEnum, FontStyle.NONE);
		addEEnumLiteral(fontStyleEEnum, FontStyle.BOLD);
		addEEnumLiteral(fontStyleEEnum, FontStyle.ITALIC);

		initEEnum(markerTypeEEnum, MarkerType.class, "MarkerType");
		addEEnumLiteral(markerTypeEEnum, MarkerType.INFORMATION);
		addEEnumLiteral(markerTypeEEnum, MarkerType.WARNING);
		addEEnumLiteral(markerTypeEEnum, MarkerType.ERROR);

		initEEnum(stackShapeEEnum, StackShape.class, "StackShape");
		addEEnumLiteral(stackShapeEEnum, StackShape.RECTANGLE);
		addEEnumLiteral(stackShapeEEnum, StackShape.ROUNDED_RECTANGLE);
		addEEnumLiteral(stackShapeEEnum, StackShape.ELLIPSE);

		initEEnum(stackStrategyEEnum, StackStrategy.class, "StackStrategy");
		addEEnumLiteral(stackStrategyEEnum, StackStrategy.LASTS);
		addEEnumLiteral(stackStrategyEEnum, StackStrategy.FIRSTS);

		initEEnum(booleanEEnum, org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Boolean.class, "Boolean");
		addEEnumLiteral(booleanEEnum, org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Boolean.FALSE);
		addEEnumLiteral(booleanEEnum, org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Boolean.TRUE);

		initEEnum(rBooleanEEnum, RBoolean.class, "RBoolean");
		addEEnumLiteral(rBooleanEEnum, RBoolean.FALSE);
		addEEnumLiteral(rBooleanEEnum, RBoolean.TRUE);

		initEEnum(lineStyleEEnum, LineStyle.class, "LineStyle");
		addEEnumLiteral(lineStyleEEnum, LineStyle.DIGITAL);
		addEEnumLiteral(lineStyleEEnum, LineStyle.ANALOG);

		// Create resource
		createResource(eNS_URI);
	}

} //RunstarPackageImpl
