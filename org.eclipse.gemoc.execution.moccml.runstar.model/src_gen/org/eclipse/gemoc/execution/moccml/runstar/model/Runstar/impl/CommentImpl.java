/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Comment;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Comment</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CommentImpl extends RepresentationImpl implements Comment {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RunstarPackage.Literals.COMMENT;
	}

} //CommentImpl
