/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RStrategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RStrategy#getOnConcept <em>On Concept</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RStrategy#getOwnedUpdateStrategy <em>Owned Update Strategy</em>}</li>
 * </ul>
 *
 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getRStrategy()
 * @model
 * @generated
 */
public interface RStrategy extends EObject {
	/**
	 * Returns the value of the '<em><b>On Concept</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Concept</em>' reference.
	 * @see #setOnConcept(EObject)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getRStrategy_OnConcept()
	 * @model required="true"
	 * @generated
	 */
	EObject getOnConcept();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RStrategy#getOnConcept <em>On Concept</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Concept</em>' reference.
	 * @see #getOnConcept()
	 * @generated
	 */
	void setOnConcept(EObject value);

	/**
	 * Returns the value of the '<em><b>Owned Update Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Update Strategy</em>' containment reference.
	 * @see #setOwnedUpdateStrategy(RUpdateStrategy)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getRStrategy_OwnedUpdateStrategy()
	 * @model containment="true"
	 * @generated
	 */
	RUpdateStrategy getOwnedUpdateStrategy();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RStrategy#getOwnedUpdateStrategy <em>Owned Update Strategy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owned Update Strategy</em>' containment reference.
	 * @see #getOwnedUpdateStrategy()
	 * @generated
	 */
	void setOwnedUpdateStrategy(RUpdateStrategy value);

} // RStrategy
