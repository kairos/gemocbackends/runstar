/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.TextVar;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Text Var</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class TextVarImpl extends VariableImpl implements TextVar {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TextVarImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RunstarPackage.Literals.TEXT_VAR;
	}

} //TextVarImpl
