/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.ocl.xtext.completeoclcs.DefPropertyCS;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RUpdate Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RUpdateStrategy#getEvents <em>Events</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RUpdateStrategy#getStepExecution <em>Step Execution</em>}</li>
 * </ul>
 *
 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getRUpdateStrategy()
 * @model
 * @generated
 */
public interface RUpdateStrategy extends EObject {
	/**
	 * Returns the value of the '<em><b>Events</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.ocl.xtext.completeoclcs.DefPropertyCS}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' reference list.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getRUpdateStrategy_Events()
	 * @model upper="2"
	 * @generated
	 */
	EList<DefPropertyCS> getEvents();

	/**
	 * Returns the value of the '<em><b>Step Execution</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StepExecution}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Step Execution</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StepExecution
	 * @see #setStepExecution(StepExecution)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getRUpdateStrategy_StepExecution()
	 * @model
	 * @generated
	 */
	StepExecution getStepExecution();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RUpdateStrategy#getStepExecution <em>Step Execution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Step Execution</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.StepExecution
	 * @see #getStepExecution()
	 * @generated
	 */
	void setStepExecution(StepExecution value);

} // RUpdateStrategy
