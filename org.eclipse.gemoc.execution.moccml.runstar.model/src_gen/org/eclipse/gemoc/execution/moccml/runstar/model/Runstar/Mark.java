/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mark</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Mark#getMarkerType <em>Marker Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getMark()
 * @model
 * @generated
 */
public interface Mark extends Representation {
	/**
	 * Returns the value of the '<em><b>Marker Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.MarkerType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Marker Type</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.MarkerType
	 * @see #setMarkerType(MarkerType)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getMark_MarkerType()
	 * @model
	 * @generated
	 */
	MarkerType getMarkerType();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Mark#getMarkerType <em>Marker Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Marker Type</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.MarkerType
	 * @see #getMarkerType()
	 * @generated
	 */
	void setMarkerType(MarkerType value);

} // Mark
