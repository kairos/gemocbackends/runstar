/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Text Var</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getTextVar()
 * @model abstract="true"
 * @generated
 */
public interface TextVar extends Variable {
} // TextVar
