/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hover</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Hover#getFromStart <em>From Start</em>}</li>
 * </ul>
 *
 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getHover()
 * @model
 * @generated
 */
public interface Hover extends Representation {
	/**
	 * Returns the value of the '<em><b>From Start</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Start</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean
	 * @see #setFromStart(RBoolean)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getHover_FromStart()
	 * @model
	 * @generated
	 */
	RBoolean getFromStart();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Hover#getFromStart <em>From Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From Start</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean
	 * @see #getFromStart()
	 * @generated
	 */
	void setFromStart(RBoolean value);

} // Hover
