/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Decorated Text Var</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getSuffix <em>Suffix</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getColor <em>Color</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getFont <em>Font</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getFontStyle <em>Font Style</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getFontSize <em>Font Size</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getDecorated <em>Decorated</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getErrorColor <em>Error Color</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getErrorText <em>Error Text</em>}</li>
 * </ul>
 *
 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getDecoratedTextVar()
 * @model
 * @generated
 */
public interface DecoratedTextVar extends TextVar {
	/**
	 * Returns the value of the '<em><b>Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prefix</em>' attribute.
	 * @see #setPrefix(String)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getDecoratedTextVar_Prefix()
	 * @model
	 * @generated
	 */
	String getPrefix();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getPrefix <em>Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prefix</em>' attribute.
	 * @see #getPrefix()
	 * @generated
	 */
	void setPrefix(String value);

	/**
	 * Returns the value of the '<em><b>Suffix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suffix</em>' attribute.
	 * @see #setSuffix(String)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getDecoratedTextVar_Suffix()
	 * @model
	 * @generated
	 */
	String getSuffix();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getSuffix <em>Suffix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Suffix</em>' attribute.
	 * @see #getSuffix()
	 * @generated
	 */
	void setSuffix(String value);

	/**
	 * Returns the value of the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' containment reference.
	 * @see #setColor(RColor)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getDecoratedTextVar_Color()
	 * @model containment="true"
	 * @generated
	 */
	RColor getColor();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getColor <em>Color</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' containment reference.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(RColor value);

	/**
	 * Returns the value of the '<em><b>Font</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Font</em>' attribute.
	 * @see #setFont(String)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getDecoratedTextVar_Font()
	 * @model
	 * @generated
	 */
	String getFont();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getFont <em>Font</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Font</em>' attribute.
	 * @see #getFont()
	 * @generated
	 */
	void setFont(String value);

	/**
	 * Returns the value of the '<em><b>Font Style</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.FontStyle}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Font Style</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.FontStyle
	 * @see #setFontStyle(FontStyle)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getDecoratedTextVar_FontStyle()
	 * @model
	 * @generated
	 */
	FontStyle getFontStyle();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getFontStyle <em>Font Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Font Style</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.FontStyle
	 * @see #getFontStyle()
	 * @generated
	 */
	void setFontStyle(FontStyle value);

	/**
	 * Returns the value of the '<em><b>Font Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Font Size</em>' attribute.
	 * @see #setFontSize(int)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getDecoratedTextVar_FontSize()
	 * @model
	 * @generated
	 */
	int getFontSize();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getFontSize <em>Font Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Font Size</em>' attribute.
	 * @see #getFontSize()
	 * @generated
	 */
	void setFontSize(int value);

	/**
	 * Returns the value of the '<em><b>Decorated</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decorated</em>' reference.
	 * @see #setDecorated(TextVar)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getDecoratedTextVar_Decorated()
	 * @model required="true"
	 * @generated
	 */
	TextVar getDecorated();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getDecorated <em>Decorated</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Decorated</em>' reference.
	 * @see #getDecorated()
	 * @generated
	 */
	void setDecorated(TextVar value);

	/**
	 * Returns the value of the '<em><b>Error Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Error Color</em>' containment reference.
	 * @see #setErrorColor(RColor)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getDecoratedTextVar_ErrorColor()
	 * @model containment="true"
	 * @generated
	 */
	RColor getErrorColor();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getErrorColor <em>Error Color</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Error Color</em>' containment reference.
	 * @see #getErrorColor()
	 * @generated
	 */
	void setErrorColor(RColor value);

	/**
	 * Returns the value of the '<em><b>Error Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Error Text</em>' attribute.
	 * @see #setErrorText(String)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getDecoratedTextVar_ErrorText()
	 * @model
	 * @generated
	 */
	String getErrorText();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar#getErrorText <em>Error Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Error Text</em>' attribute.
	 * @see #getErrorText()
	 * @generated
	 */
	void setErrorText(String value);

} // DecoratedTextVar
