/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.DecoratedTextVar;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.FontStyle;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RColor;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.TextVar;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Decorated Text Var</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.DecoratedTextVarImpl#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.DecoratedTextVarImpl#getSuffix <em>Suffix</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.DecoratedTextVarImpl#getColor <em>Color</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.DecoratedTextVarImpl#getFont <em>Font</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.DecoratedTextVarImpl#getFontStyle <em>Font Style</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.DecoratedTextVarImpl#getFontSize <em>Font Size</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.DecoratedTextVarImpl#getDecorated <em>Decorated</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.DecoratedTextVarImpl#getErrorColor <em>Error Color</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.DecoratedTextVarImpl#getErrorText <em>Error Text</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DecoratedTextVarImpl extends TextVarImpl implements DecoratedTextVar {
	/**
	 * The default value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrefix()
	 * @generated
	 * @ordered
	 */
	protected static final String PREFIX_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrefix()
	 * @generated
	 * @ordered
	 */
	protected String prefix = PREFIX_EDEFAULT;

	/**
	 * The default value of the '{@link #getSuffix() <em>Suffix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuffix()
	 * @generated
	 * @ordered
	 */
	protected static final String SUFFIX_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSuffix() <em>Suffix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuffix()
	 * @generated
	 * @ordered
	 */
	protected String suffix = SUFFIX_EDEFAULT;

	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected RColor color;

	/**
	 * The default value of the '{@link #getFont() <em>Font</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFont()
	 * @generated
	 * @ordered
	 */
	protected static final String FONT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFont() <em>Font</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFont()
	 * @generated
	 * @ordered
	 */
	protected String font = FONT_EDEFAULT;

	/**
	 * The default value of the '{@link #getFontStyle() <em>Font Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFontStyle()
	 * @generated
	 * @ordered
	 */
	protected static final FontStyle FONT_STYLE_EDEFAULT = FontStyle.NONE;

	/**
	 * The cached value of the '{@link #getFontStyle() <em>Font Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFontStyle()
	 * @generated
	 * @ordered
	 */
	protected FontStyle fontStyle = FONT_STYLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFontSize() <em>Font Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFontSize()
	 * @generated
	 * @ordered
	 */
	protected static final int FONT_SIZE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFontSize() <em>Font Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFontSize()
	 * @generated
	 * @ordered
	 */
	protected int fontSize = FONT_SIZE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDecorated() <em>Decorated</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDecorated()
	 * @generated
	 * @ordered
	 */
	protected TextVar decorated;

	/**
	 * The cached value of the '{@link #getErrorColor() <em>Error Color</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErrorColor()
	 * @generated
	 * @ordered
	 */
	protected RColor errorColor;

	/**
	 * The default value of the '{@link #getErrorText() <em>Error Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErrorText()
	 * @generated
	 * @ordered
	 */
	protected static final String ERROR_TEXT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getErrorText() <em>Error Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErrorText()
	 * @generated
	 * @ordered
	 */
	protected String errorText = ERROR_TEXT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecoratedTextVarImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RunstarPackage.Literals.DECORATED_TEXT_VAR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPrefix() {
		return prefix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPrefix(String newPrefix) {
		String oldPrefix = prefix;
		prefix = newPrefix;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.DECORATED_TEXT_VAR__PREFIX, oldPrefix, prefix));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSuffix() {
		return suffix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSuffix(String newSuffix) {
		String oldSuffix = suffix;
		suffix = newSuffix;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.DECORATED_TEXT_VAR__SUFFIX, oldSuffix, suffix));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RColor getColor() {
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetColor(RColor newColor, NotificationChain msgs) {
		RColor oldColor = color;
		color = newColor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RunstarPackage.DECORATED_TEXT_VAR__COLOR, oldColor, newColor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setColor(RColor newColor) {
		if (newColor != color) {
			NotificationChain msgs = null;
			if (color != null)
				msgs = ((InternalEObject)color).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RunstarPackage.DECORATED_TEXT_VAR__COLOR, null, msgs);
			if (newColor != null)
				msgs = ((InternalEObject)newColor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RunstarPackage.DECORATED_TEXT_VAR__COLOR, null, msgs);
			msgs = basicSetColor(newColor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.DECORATED_TEXT_VAR__COLOR, newColor, newColor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFont() {
		return font;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFont(String newFont) {
		String oldFont = font;
		font = newFont;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.DECORATED_TEXT_VAR__FONT, oldFont, font));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FontStyle getFontStyle() {
		return fontStyle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFontStyle(FontStyle newFontStyle) {
		FontStyle oldFontStyle = fontStyle;
		fontStyle = newFontStyle == null ? FONT_STYLE_EDEFAULT : newFontStyle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.DECORATED_TEXT_VAR__FONT_STYLE, oldFontStyle, fontStyle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getFontSize() {
		return fontSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFontSize(int newFontSize) {
		int oldFontSize = fontSize;
		fontSize = newFontSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.DECORATED_TEXT_VAR__FONT_SIZE, oldFontSize, fontSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TextVar getDecorated() {
		if (decorated != null && decorated.eIsProxy()) {
			InternalEObject oldDecorated = (InternalEObject)decorated;
			decorated = (TextVar)eResolveProxy(oldDecorated);
			if (decorated != oldDecorated) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RunstarPackage.DECORATED_TEXT_VAR__DECORATED, oldDecorated, decorated));
			}
		}
		return decorated;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TextVar basicGetDecorated() {
		return decorated;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDecorated(TextVar newDecorated) {
		TextVar oldDecorated = decorated;
		decorated = newDecorated;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.DECORATED_TEXT_VAR__DECORATED, oldDecorated, decorated));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RColor getErrorColor() {
		return errorColor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetErrorColor(RColor newErrorColor, NotificationChain msgs) {
		RColor oldErrorColor = errorColor;
		errorColor = newErrorColor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RunstarPackage.DECORATED_TEXT_VAR__ERROR_COLOR, oldErrorColor, newErrorColor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setErrorColor(RColor newErrorColor) {
		if (newErrorColor != errorColor) {
			NotificationChain msgs = null;
			if (errorColor != null)
				msgs = ((InternalEObject)errorColor).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RunstarPackage.DECORATED_TEXT_VAR__ERROR_COLOR, null, msgs);
			if (newErrorColor != null)
				msgs = ((InternalEObject)newErrorColor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RunstarPackage.DECORATED_TEXT_VAR__ERROR_COLOR, null, msgs);
			msgs = basicSetErrorColor(newErrorColor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.DECORATED_TEXT_VAR__ERROR_COLOR, newErrorColor, newErrorColor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getErrorText() {
		return errorText;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setErrorText(String newErrorText) {
		String oldErrorText = errorText;
		errorText = newErrorText;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.DECORATED_TEXT_VAR__ERROR_TEXT, oldErrorText, errorText));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RunstarPackage.DECORATED_TEXT_VAR__COLOR:
				return basicSetColor(null, msgs);
			case RunstarPackage.DECORATED_TEXT_VAR__ERROR_COLOR:
				return basicSetErrorColor(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RunstarPackage.DECORATED_TEXT_VAR__PREFIX:
				return getPrefix();
			case RunstarPackage.DECORATED_TEXT_VAR__SUFFIX:
				return getSuffix();
			case RunstarPackage.DECORATED_TEXT_VAR__COLOR:
				return getColor();
			case RunstarPackage.DECORATED_TEXT_VAR__FONT:
				return getFont();
			case RunstarPackage.DECORATED_TEXT_VAR__FONT_STYLE:
				return getFontStyle();
			case RunstarPackage.DECORATED_TEXT_VAR__FONT_SIZE:
				return getFontSize();
			case RunstarPackage.DECORATED_TEXT_VAR__DECORATED:
				if (resolve) return getDecorated();
				return basicGetDecorated();
			case RunstarPackage.DECORATED_TEXT_VAR__ERROR_COLOR:
				return getErrorColor();
			case RunstarPackage.DECORATED_TEXT_VAR__ERROR_TEXT:
				return getErrorText();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RunstarPackage.DECORATED_TEXT_VAR__PREFIX:
				setPrefix((String)newValue);
				return;
			case RunstarPackage.DECORATED_TEXT_VAR__SUFFIX:
				setSuffix((String)newValue);
				return;
			case RunstarPackage.DECORATED_TEXT_VAR__COLOR:
				setColor((RColor)newValue);
				return;
			case RunstarPackage.DECORATED_TEXT_VAR__FONT:
				setFont((String)newValue);
				return;
			case RunstarPackage.DECORATED_TEXT_VAR__FONT_STYLE:
				setFontStyle((FontStyle)newValue);
				return;
			case RunstarPackage.DECORATED_TEXT_VAR__FONT_SIZE:
				setFontSize((Integer)newValue);
				return;
			case RunstarPackage.DECORATED_TEXT_VAR__DECORATED:
				setDecorated((TextVar)newValue);
				return;
			case RunstarPackage.DECORATED_TEXT_VAR__ERROR_COLOR:
				setErrorColor((RColor)newValue);
				return;
			case RunstarPackage.DECORATED_TEXT_VAR__ERROR_TEXT:
				setErrorText((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RunstarPackage.DECORATED_TEXT_VAR__PREFIX:
				setPrefix(PREFIX_EDEFAULT);
				return;
			case RunstarPackage.DECORATED_TEXT_VAR__SUFFIX:
				setSuffix(SUFFIX_EDEFAULT);
				return;
			case RunstarPackage.DECORATED_TEXT_VAR__COLOR:
				setColor((RColor)null);
				return;
			case RunstarPackage.DECORATED_TEXT_VAR__FONT:
				setFont(FONT_EDEFAULT);
				return;
			case RunstarPackage.DECORATED_TEXT_VAR__FONT_STYLE:
				setFontStyle(FONT_STYLE_EDEFAULT);
				return;
			case RunstarPackage.DECORATED_TEXT_VAR__FONT_SIZE:
				setFontSize(FONT_SIZE_EDEFAULT);
				return;
			case RunstarPackage.DECORATED_TEXT_VAR__DECORATED:
				setDecorated((TextVar)null);
				return;
			case RunstarPackage.DECORATED_TEXT_VAR__ERROR_COLOR:
				setErrorColor((RColor)null);
				return;
			case RunstarPackage.DECORATED_TEXT_VAR__ERROR_TEXT:
				setErrorText(ERROR_TEXT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RunstarPackage.DECORATED_TEXT_VAR__PREFIX:
				return PREFIX_EDEFAULT == null ? prefix != null : !PREFIX_EDEFAULT.equals(prefix);
			case RunstarPackage.DECORATED_TEXT_VAR__SUFFIX:
				return SUFFIX_EDEFAULT == null ? suffix != null : !SUFFIX_EDEFAULT.equals(suffix);
			case RunstarPackage.DECORATED_TEXT_VAR__COLOR:
				return color != null;
			case RunstarPackage.DECORATED_TEXT_VAR__FONT:
				return FONT_EDEFAULT == null ? font != null : !FONT_EDEFAULT.equals(font);
			case RunstarPackage.DECORATED_TEXT_VAR__FONT_STYLE:
				return fontStyle != FONT_STYLE_EDEFAULT;
			case RunstarPackage.DECORATED_TEXT_VAR__FONT_SIZE:
				return fontSize != FONT_SIZE_EDEFAULT;
			case RunstarPackage.DECORATED_TEXT_VAR__DECORATED:
				return decorated != null;
			case RunstarPackage.DECORATED_TEXT_VAR__ERROR_COLOR:
				return errorColor != null;
			case RunstarPackage.DECORATED_TEXT_VAR__ERROR_TEXT:
				return ERROR_TEXT_EDEFAULT == null ? errorText != null : !ERROR_TEXT_EDEFAULT.equals(errorText);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (prefix: ");
		result.append(prefix);
		result.append(", suffix: ");
		result.append(suffix);
		result.append(", font: ");
		result.append(font);
		result.append(", fontStyle: ");
		result.append(fontStyle);
		result.append(", fontSize: ");
		result.append(fontSize);
		result.append(", errorText: ");
		result.append(errorText);
		result.append(')');
		return result.toString();
	}

} //DecoratedTextVarImpl
