/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Paint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getColor <em>Color</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getOnConcept <em>On Concept</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getOwnedUpdateStrategy <em>Owned Update Strategy</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getPersistent <em>Persistent</em>}</li>
 * </ul>
 *
 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getPaint()
 * @model
 * @generated
 */
public interface Paint extends Representation {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.PaintType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.PaintType
	 * @see #setType(PaintType)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getPaint_Type()
	 * @model
	 * @generated
	 */
	PaintType getType();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.PaintType
	 * @see #getType()
	 * @generated
	 */
	void setType(PaintType value);

	/**
	 * Returns the value of the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' containment reference.
	 * @see #setColor(RColor)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getPaint_Color()
	 * @model containment="true"
	 * @generated
	 */
	RColor getColor();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getColor <em>Color</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' containment reference.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(RColor value);

	/**
	 * Returns the value of the '<em><b>On Concept</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Concept</em>' reference.
	 * @see #setOnConcept(EObject)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getPaint_OnConcept()
	 * @model
	 * @generated
	 */
	EObject getOnConcept();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getOnConcept <em>On Concept</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Concept</em>' reference.
	 * @see #getOnConcept()
	 * @generated
	 */
	void setOnConcept(EObject value);

	/**
	 * Returns the value of the '<em><b>Owned Update Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Update Strategy</em>' containment reference.
	 * @see #setOwnedUpdateStrategy(RUpdateStrategy)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getPaint_OwnedUpdateStrategy()
	 * @model containment="true"
	 * @generated
	 */
	RUpdateStrategy getOwnedUpdateStrategy();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getOwnedUpdateStrategy <em>Owned Update Strategy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owned Update Strategy</em>' containment reference.
	 * @see #getOwnedUpdateStrategy()
	 * @generated
	 */
	void setOwnedUpdateStrategy(RUpdateStrategy value);

	/**
	 * Returns the value of the '<em><b>Persistent</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Persistent</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean
	 * @see #setPersistent(RBoolean)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getPaint_Persistent()
	 * @model
	 * @generated
	 */
	RBoolean getPersistent();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Paint#getPersistent <em>Persistent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Persistent</em>' attribute.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean
	 * @see #getPersistent()
	 * @generated
	 */
	void setPersistent(RBoolean value);

} // Paint
