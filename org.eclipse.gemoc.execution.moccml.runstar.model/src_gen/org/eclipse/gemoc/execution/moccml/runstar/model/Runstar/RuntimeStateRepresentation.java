/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Runtime State Representation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation#getModelImports <em>Model Imports</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation#getClassImports <em>Class Imports</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation#getVariables <em>Variables</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation#getRepresentations <em>Representations</em>}</li>
 * </ul>
 *
 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getRuntimeStateRepresentation()
 * @model
 * @generated
 */
public interface RuntimeStateRepresentation extends EObject {
	/**
	 * Returns the value of the '<em><b>Model Imports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.ImportStatement}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Imports</em>' containment reference list.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getRuntimeStateRepresentation_ModelImports()
	 * @model containment="true"
	 * @generated
	 */
	EList<ImportStatement> getModelImports();

	/**
	 * Returns the value of the '<em><b>Class Imports</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.ImportJavaStatement}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Imports</em>' containment reference list.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getRuntimeStateRepresentation_ClassImports()
	 * @model containment="true"
	 * @generated
	 */
	EList<ImportJavaStatement> getClassImports();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getRuntimeStateRepresentation_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RuntimeStateRepresentation#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Variables</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Variable}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variables</em>' containment reference list.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getRuntimeStateRepresentation_Variables()
	 * @model containment="true"
	 * @generated
	 */
	EList<Variable> getVariables();

	/**
	 * Returns the value of the '<em><b>Representations</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Representation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Representations</em>' containment reference list.
	 * @see org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage#getRuntimeStateRepresentation_Representations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Representation> getRepresentations();

} // RuntimeStateRepresentation
