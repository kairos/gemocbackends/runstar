/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RStrategy;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RUpdateStrategy;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>RStrategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RStrategyImpl#getOnConcept <em>On Concept</em>}</li>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.RStrategyImpl#getOwnedUpdateStrategy <em>Owned Update Strategy</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RStrategyImpl extends EObjectImpl implements RStrategy {
	/**
	 * The cached value of the '{@link #getOnConcept() <em>On Concept</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOnConcept()
	 * @generated
	 * @ordered
	 */
	protected EObject onConcept;

	/**
	 * The cached value of the '{@link #getOwnedUpdateStrategy() <em>Owned Update Strategy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedUpdateStrategy()
	 * @generated
	 * @ordered
	 */
	protected RUpdateStrategy ownedUpdateStrategy;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RunstarPackage.Literals.RSTRATEGY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject getOnConcept() {
		if (onConcept != null && onConcept.eIsProxy()) {
			InternalEObject oldOnConcept = (InternalEObject)onConcept;
			onConcept = eResolveProxy(oldOnConcept);
			if (onConcept != oldOnConcept) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RunstarPackage.RSTRATEGY__ON_CONCEPT, oldOnConcept, onConcept));
			}
		}
		return onConcept;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetOnConcept() {
		return onConcept;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOnConcept(EObject newOnConcept) {
		EObject oldOnConcept = onConcept;
		onConcept = newOnConcept;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.RSTRATEGY__ON_CONCEPT, oldOnConcept, onConcept));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RUpdateStrategy getOwnedUpdateStrategy() {
		return ownedUpdateStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOwnedUpdateStrategy(RUpdateStrategy newOwnedUpdateStrategy, NotificationChain msgs) {
		RUpdateStrategy oldOwnedUpdateStrategy = ownedUpdateStrategy;
		ownedUpdateStrategy = newOwnedUpdateStrategy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RunstarPackage.RSTRATEGY__OWNED_UPDATE_STRATEGY, oldOwnedUpdateStrategy, newOwnedUpdateStrategy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOwnedUpdateStrategy(RUpdateStrategy newOwnedUpdateStrategy) {
		if (newOwnedUpdateStrategy != ownedUpdateStrategy) {
			NotificationChain msgs = null;
			if (ownedUpdateStrategy != null)
				msgs = ((InternalEObject)ownedUpdateStrategy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RunstarPackage.RSTRATEGY__OWNED_UPDATE_STRATEGY, null, msgs);
			if (newOwnedUpdateStrategy != null)
				msgs = ((InternalEObject)newOwnedUpdateStrategy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RunstarPackage.RSTRATEGY__OWNED_UPDATE_STRATEGY, null, msgs);
			msgs = basicSetOwnedUpdateStrategy(newOwnedUpdateStrategy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.RSTRATEGY__OWNED_UPDATE_STRATEGY, newOwnedUpdateStrategy, newOwnedUpdateStrategy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RunstarPackage.RSTRATEGY__OWNED_UPDATE_STRATEGY:
				return basicSetOwnedUpdateStrategy(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RunstarPackage.RSTRATEGY__ON_CONCEPT:
				if (resolve) return getOnConcept();
				return basicGetOnConcept();
			case RunstarPackage.RSTRATEGY__OWNED_UPDATE_STRATEGY:
				return getOwnedUpdateStrategy();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RunstarPackage.RSTRATEGY__ON_CONCEPT:
				setOnConcept((EObject)newValue);
				return;
			case RunstarPackage.RSTRATEGY__OWNED_UPDATE_STRATEGY:
				setOwnedUpdateStrategy((RUpdateStrategy)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RunstarPackage.RSTRATEGY__ON_CONCEPT:
				setOnConcept((EObject)null);
				return;
			case RunstarPackage.RSTRATEGY__OWNED_UPDATE_STRATEGY:
				setOwnedUpdateStrategy((RUpdateStrategy)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RunstarPackage.RSTRATEGY__ON_CONCEPT:
				return onConcept != null;
			case RunstarPackage.RSTRATEGY__OWNED_UPDATE_STRATEGY:
				return ownedUpdateStrategy != null;
		}
		return super.eIsSet(featureID);
	}

} //RStrategyImpl
