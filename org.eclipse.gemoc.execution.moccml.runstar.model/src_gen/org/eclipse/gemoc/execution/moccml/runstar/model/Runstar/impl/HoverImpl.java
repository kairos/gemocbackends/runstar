/**
 */
package org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.Hover;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RBoolean;
import org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.RunstarPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hover</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.gemoc.execution.moccml.runstar.model.Runstar.impl.HoverImpl#getFromStart <em>From Start</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HoverImpl extends RepresentationImpl implements Hover {
	/**
	 * The default value of the '{@link #getFromStart() <em>From Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromStart()
	 * @generated
	 * @ordered
	 */
	protected static final RBoolean FROM_START_EDEFAULT = RBoolean.FALSE;

	/**
	 * The cached value of the '{@link #getFromStart() <em>From Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromStart()
	 * @generated
	 * @ordered
	 */
	protected RBoolean fromStart = FROM_START_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HoverImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RunstarPackage.Literals.HOVER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RBoolean getFromStart() {
		return fromStart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFromStart(RBoolean newFromStart) {
		RBoolean oldFromStart = fromStart;
		fromStart = newFromStart == null ? FROM_START_EDEFAULT : newFromStart;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RunstarPackage.HOVER__FROM_START, oldFromStart, fromStart));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RunstarPackage.HOVER__FROM_START:
				return getFromStart();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RunstarPackage.HOVER__FROM_START:
				setFromStart((RBoolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RunstarPackage.HOVER__FROM_START:
				setFromStart(FROM_START_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RunstarPackage.HOVER__FROM_START:
				return fromStart != FROM_START_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (fromStart: ");
		result.append(fromStart);
		result.append(')');
		return result.toString();
	}

} //HoverImpl
