# RunStaR
---

### [RunStaR] is a meta-language created for language designers to specify a runtime state representation they would like for the concrete syntaxes of their DSL.

## Motivation

Nowadays, writing a program is no more restricted to computer scientists and there are more and more stakeholders aiming to write specific programs in many fields and General Purpose programming Languages (GPLs) are usually inappropriate for them, being too far from their domain of expertise. Usually, such stakeholders use DSLs.

To help in the creation of DSLs, Language Workbenches (LW) are tools that make the development of new languages and their IDE affordable. Debuggers are a crucial part of the tooling and help the user understand the runtime state of its program during its execution. However, they are supported only by few LWs and consequently require extra work with few support of the LW. The proposed debugging services, when provided by LW, are often generic and not related to the concrete syntax defined by the DSL, current values of variables are shown in a distinct view without customisable support. Thereby, the debugger displays information the same way no matter the DSL and these representations are orthogonal to the concrete syntax of the DSL. When compared to dedicated debuggers of GPLs, we can often notice that several interesting features are missing, specially the ones that render debugging information directly as annotation of the concrete syntax.

Some frameworks like [Xtext] or [KLighD] offer a way to complete the concrete representation of a DSL. Therefore, to make more effective language independant debuggers, we want a meta-language that is meant to provide informations about the wanted runtime state representations.

## Applications

At the moment, only two projects use RunStaR :
- [klightanimationbackendforgemoc] for the runtime animation of KLighD concrete syntax;  
- [xtextanimationbackendforgemoc] for the runtime animation of Xtext concrete syntax.  

Each project only uses the RunStaR concepts they need.  

## Content

Currently, the RunStaR syntax is made of _imports_, **_Variables_** and **_Representations_**.

In addition to its own attributes, each **_Variable_** must have a name. It also has a strategy if it does not contain another **_Variable_**.
A strategy is composed of two elements:
- information about a related concept;  
- an update strategy which defines the moment to be used, get or refreshed.  

Then, from any **_Variable_** you can retrieve either its strategy or its contained **_Variable_**.

In addition to their own attributes, most **_Representations_** must be linked to at least one **_Variable_**.

### Imports

There are two supported _imports_ kind:

- _ImportStatement_ to import a _TimeModel_ concept
- _ImportJavaStatement_ to import a java class or package with its namespace	

### Variables

A **_Variable_** specifies the contents of a **_Representation_** and its related concept thanks to a strategy.

- **_TextVar_**:
	+ **_RTDVar_**: contains a list of at least one attribute name to get from the related concept.
	+ **_ConstTextVar_**: contains a constant text
	+ **_DecoratedTextVar_**: contains a **_TextVar_** to decorate. It can also contain a prefix and suffix to add to the owned **_TextVar_**, a font name, style and size, a color or an error color.
- **_ImageVar_**: must specify the path to retrieve the image.
	+ **_ChartVar_**: an **_ImageVar_** that must contain two **_RTDVar_** to specify the X and Y axis variables. It can also contain the maximum number of values to be displayed at the same time, the X and Y axis labels, a title and a line style (digital or analog).

### Representations 

A **_Representation_** specifies the kind of representation to use, its related **_Variables_** and how to use it.

- **_Stack_** (KLighD): must point to a **_TextVar_** to be displayed as a stack. You can specify the stack maximum length, if it should be vertical or horizontal, its shape, the overload strategy and the depth from its related concept to display it.

- **_Image_** (KLighD): must point to an **_ImageVar_**. You can specify its initial size, the depth from its related concept to display it and a **_TextVar_** to be used as a label for the image.

- **_Label_** (KLighD): must point to a **_TextVar_**.

- **_Paint_** (KLighD & Xtext): must contain a color, a type (foreground or background) and an update strategy. You can also specify a related concept and if the color should be persistent.

- **_Mark_** (Xtext): must point to a **_TextVar_** and can specify the marker type.

- **_Hover_** (Xtext): must point to a **_TextVar_**. You can specify if it should be display at the beginning and an **_ImageVar_** to display.

- **_Comment_** (Xtext): must point to a **_TextVar_**.

## How to contribute

**RunStaR** is a Xtext project so you can see the [Xtext doc] where you may find everything you need.
Go [there][runstar.xtext] for the Xtext file and [there][runstar.ecore] for the model.

### Diagram

![diagram]

## How to install and use

To install, you just have to clone the project and import it to a Gemoc workspace.

To launch it you must run a new eclipse application from it. From there, you will be able to create a **_.runstar_** file.
To import your language abstract syntax and runtime data definition, your import path in the **_.runstar_** file needs to start with `"platform:/plugin/"`. Then you can write your path. Here is an example :  
`import "platform:/plugin/org.lflang/model/generated/LF.ecore";`

To use it with [klightanimationbackendforgemoc] or [xtextanimationbackendforgemoc], you must put the **_.runstar_** file in your DSL project and specify its path in your **_.dsl_** file like: `runstar = /my.dsl.project/myFile.runstar`.

## Credits

Copyright (c) 2017 I3S laboratory, INRIA and others. All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available [here][eclipse license].

### Contributors:

- [Ryana Karaki] - developer
- [Ludovic Marti] - developer
- [Julien Deantoni] - Project manager & developer




[eclipse license]:http://www.eclipse.org/legal/epl-v10.html "Eclipse Public License"
[Julien Deantoni]:https://gitlab.inria.fr/jdeanton "Julien Deantoni"
[Ludovic Marti]:https://gitlab.inria.fr/lumarti "Ludovic Marti"
[Ryana Karaki]:https://gitlab.inria.fr/rkaraki "Ryana Karaki"
[Xtext]:https://www.eclipse.org/Xtext/ "Xtext"
[KLighD]:https://github.com/kieler/KLighD "KLighD"
[RunStaR]:https://gitlab.inria.fr/kairos/gemocbackends/runstar "RunStaR"
[klightanimationbackendforgemoc]:https://gitlab.inria.fr/kairos/gemocbackends/klightanimationbackendforgemoc "klightanimationbackendforgemoc"
[xtextanimationbackendforgemoc]:https://gitlab.inria.fr/kairos/gemocbackends/xtextanimationbackendforgemoc "xtextanimationbackendforgemoc"
[Xtext doc]:https://www.eclipse.org/Xtext/documentation/index.html "Xtext"
[runstar.xtext]:https://gitlab.inria.fr/kairos/gemocbackends/runstar/-/blob/master/org.eclipse.gemoc.execution.moccml.runstar.xtext/src/org/eclipse/gemoc/execution/moccml/runstar/xtext/Runstar.xtext "Runstar.xtext"
[runstar.ecore]:https://gitlab.inria.fr/kairos/gemocbackends/runstar/-/blob/master/org.eclipse.gemoc.execution.moccml.runstar.model/model/Runstar.ecore "Runstar.ecore"
[diagram]:./Runstar_diagram.png "Runstar diagram"
