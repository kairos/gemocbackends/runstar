package org.eclipse.gemoc.execution.moccml.runstar.xtext.ui


import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory

class TimemodelExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	override protected getBundle() {
		RunstarActivatorEx.instance.bundle
	}

	override protected getInjector() {
		RunstarActivatorEx.instance.getInjector("fr.inria.aoste.timesquare.ccslkernel.model")
	}

}